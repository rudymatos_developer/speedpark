//
//  ActiveCell.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/19/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class ActiveCell: UITableViewCell,Identifiable {
    
    @IBOutlet weak var ticketNumberLBL: UILabel!
    @IBOutlet weak var customerNameLBL: UILabel!
    @IBOutlet weak var vehicleInformationLBL: UILabel!
    @IBOutlet weak var parkingSpotLBL: UILabel!
    @IBOutlet weak var paidLBL: UILabel!
    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var moreView: UIView!
    @IBOutlet weak var waitingTimerLBL: UILabel!
    
    private var timer : Timer?
    
    var booking: SPBooking!{
        didSet{
            configureCell()
        }
    }
    
    override func prepareForReuse() {
        waitingTimerLBL.textColor = .black
        timer?.invalidate()
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    var callActionCompletion : ((SPBooking) -> Void)?
    var showMoreOptions : ((SPBooking) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func call(_ sender: UIButton) {
        callActionCompletion?(booking)
    }
    
    @IBAction func showMoreOptions(_ sender: UIButton) {
        showMoreOptions?(booking)
    }
    
    @objc private func calculateWaitingTimeDifference(){
        guard let waitingTimeDate = booking.customer.waitingTimeDate, let totalMinutes = Calendar.current.dateComponents([.minute], from: waitingTimeDate, to: Date()).minute else{
            return
        }
        let originalWaitingTime =  "\(booking.customer.waitingTime) minutes"
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.minute,.second]
        formatter.unitsStyle = .abbreviated
        waitingTimerLBL.text = "Timer: \(formatter.string(from: waitingTimeDate, to: Date()) ?? "Cannot Calculate") (\(originalWaitingTime))"
        waitingTimerLBL.textColor = totalMinutes >= booking.customer.waitingTime ? .red : .black
    }
    
}

extension ActiveCell{
    
    private func configureCell(){
        paidLBL.isHidden = true
        backgroundColor = .white
        ticketNumberLBL.text = "\(booking.ticketNumber)"
        customerNameLBL.text = booking.customer.name.uppercased()
        vehicleInformationLBL.text = booking.vehicle.description
        parkingSpotLBL.text = "Parking Spot: \(booking.parking.siteLocation) \(booking.parking.spot)"

        callView.layer.borderColor = #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        callView.layer.borderWidth = 2
        
        moreView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        moreView.layer.borderWidth = 2
        
        ticketNumberLBL.textColor = booking.status.getBTNActionColor()
        if booking.payment.paid || booking.payment.prePaid{
            paidLBL.isHidden = false
            backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.9116873741, blue: 0.9629065394, alpha: 1)
        }
        
        if booking.payment.serviceType == .spotHero{
            backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.2545483733)
        }
        
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(ActiveCell.calculateWaitingTimeDifference), userInfo: nil, repeats: true)
        timer?.fire()
        RunLoop.current.add(timer!, forMode: .common)
    }
    
}
