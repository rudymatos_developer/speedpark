//
//  CustomerCell.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 7/14/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class CustomerCell: UITableViewCell {

    @IBOutlet weak var customerPhoneLBL: UILabel!
    @IBOutlet weak var customerEmailLBL: UILabel!
    @IBOutlet weak var customerNameLBL: UILabel!
    
    var customer: SPCustomer!
    
    func configure(with: SPCustomer){
        self.customer = with
        customerPhoneLBL.text = customer.fullName
        customerEmailLBL.text = customer.email
        customerPhoneLBL.text = customer.phone
    }
    
}
