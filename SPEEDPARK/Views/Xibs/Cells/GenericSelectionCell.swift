//
//  GenericSelectionCell.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 6/21/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class GenericSelectionCell: UITableViewCell {

    @IBOutlet weak var value: UILabel!
    @IBOutlet weak var checkedIV: UIImageView!
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var additionalInfoLBL: UILabel!

    var genericSelectionData: GenericSelectionData!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureView(genericSelectionData : GenericSelectionData){
        self.genericSelectionData = genericSelectionData
        checkedIV.isHidden = !genericSelectionData.selected
        value.text = genericSelectionData.displayName
        descriptionLBL.text = genericSelectionData.info
        additionalInfoLBL.text = genericSelectionData.additionalInfo
        descriptionLBL.isHidden = !(genericSelectionData.type == .customer)
        additionalInfoLBL.isHidden = !(genericSelectionData.type == .customer)
    }
    
}
