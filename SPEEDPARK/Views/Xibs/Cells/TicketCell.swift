//
//  TicketCell.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/29/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class TicketCell: UITableViewCell, Identifiable, SPDateFormatter {
    
    @IBOutlet weak var keysCheckInView: UIView!
    @IBOutlet weak var actionBTN: UIButton!
    @IBOutlet weak var buttonActionView: UIView!
    @IBOutlet weak var auxiliarActionView: UIView!
    
    @IBOutlet weak var mainTextLBL: UILabel!
    @IBOutlet weak var secondaryMainTextLBL: UILabel!
    @IBOutlet weak var secondaryTextLBL: UILabel!
    @IBOutlet weak var detailTextLBL: UILabel!
    @IBOutlet weak var secondaryDetailTextLBL: UILabel!
    @IBOutlet weak var paidLBL: UILabel!
    
    @IBOutlet weak var swapParkingView: UIView!
    @IBOutlet weak var swapBTN: UIButton!
    
    var booking: SPBooking!{
        didSet{
            configureCell()
        }
    }

    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    var processActionCompletion : ((SPBooking) -> Void)?
    var swapingActionCompletion : ((SPBooking) -> Void)?
    var checkKeysInCompletion : ((SPBooking) -> Void)?
    var auxiliarActionCompletion  : ((SPBooking) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func keysCheckedIn(_ sender: UIButton) {
        checkKeysInCompletion?(booking)
    }
    
    @IBAction func auxiliarAction(_ sender: UIButton) {
        auxiliarActionCompletion?(booking)
    }
    
    @IBAction func swapParking(_ sender: UIButton) {
        swapingActionCompletion?(booking)
    }
    
    private func configureCell(){
        
        swapParkingView.isHidden = true
        secondaryMainTextLBL.isHidden = true
        secondaryDetailTextLBL.isHidden = true
        keysCheckInView.isHidden = true
        paidLBL.isHidden = true
        backgroundColor = .white
        
        secondaryMainTextLBL.text = ""
        mainTextLBL.text = "\(booking.identifier)"
        
        switch booking.status{
        case .booked, .noShow:
            secondaryDetailTextLBL.isHidden = false
            secondaryDetailTextLBL.text = "Enter Date: \(booking.parking.enterDate12HoursFormat)"
            secondaryDetailTextLBL.font = UIFont(name: "HelveticaNeue-Light", size: 12)
            secondaryDetailTextLBL.textColor = #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)
        case .active:
            mainTextLBL.text = "\(booking.identifier)"
            auxiliarActionView.isHidden = false
            secondaryMainTextLBL.isHidden = false
        case .new:
            mainTextLBL.text = "\(booking.identifier)"
            secondaryDetailTextLBL.isHidden = false
            secondaryDetailTextLBL.text = "Parking Spot: \(booking.parking.siteLocation) \(booking.parking.spot)"
            auxiliarActionView.isHidden = false
            auxiliarActionView.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.6, blue: 0.6156862745, alpha: 1)
            auxiliarActionView.layer.borderColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
            auxiliarActionView.layer.borderWidth = 2
        case .checkedIn, .parked, .done:
            mainTextLBL.text = "\(booking.ticketNumber)"
            secondaryMainTextLBL.isHidden = false
            secondaryDetailTextLBL.isHidden = false
            secondaryDetailTextLBL.text = "Reservation Number: \(booking.identifier)"
            if booking.isSwapable{
                
                swapParkingView.isHidden = false
                swapBTN.setImage(#imageLiteral(resourceName: "swap").withRenderingMode(.alwaysTemplate), for: .normal)
                swapBTN.tintColor = .white
                swapParkingView.layer.cornerRadius = swapParkingView.frame.height / 2
                swapParkingView.layer.borderWidth = 2
                swapParkingView.layer.borderColor = booking.status.getBorderColor().cgColor
                swapParkingView.backgroundColor = #colorLiteral(red: 0.7254902124, green: 0.4784313738, blue: 0.09803921729, alpha: 1)
                
            }
        }
        
        if booking.parking.keyStatus == .keysNotCheckedIn && booking.status == .parked{
            keysCheckInView.isHidden = false
        }
        
        if booking.payment.paid || booking.payment.prePaid{
            paidLBL.isHidden = false
            backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.9116873741, blue: 0.9629065394, alpha: 1)
        }
        
        if booking.payment.serviceType == .spotHero{
            backgroundColor = #colorLiteral(red: 0.8509803922, green: 0.9116873741, blue: 0.9629065394, alpha: 1)
        }
        
        if !booking.parking.siteLocation.isEmpty,  !booking.parking.spot.isEmpty{
                secondaryMainTextLBL.text = "\(booking.parking.siteLocation) \(booking.parking.spot)"
        }else{
            secondaryMainTextLBL.isHidden = true
        }
        secondaryTextLBL.text = booking.customer.name.uppercased()
        detailTextLBL.text = booking.vehicle.description
        buttonActionView.layer.borderWidth = 2
        buttonActionView.layer.borderColor = booking.status.getBorderColor().cgColor
        buttonActionView.backgroundColor = booking.status.getBTNActionColor()
        mainTextLBL.textColor = booking.status.getBTNActionColor()
        actionBTN.setTitle(booking.status.getBTNActionTitle(), for: .normal)
    }
    
    @IBAction func processAction(_ sender: UIButton) {
        processActionCompletion?(booking)
    }
    
}
