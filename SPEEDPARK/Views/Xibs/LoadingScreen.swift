//
//  LoadingScreen.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 3/31/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import UIKit
import Lottie

class LoadingScreen: UIView {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var animatedView: UIView!
    
    deinit {
        print("💀 im out from \(self.classForCoder.description())")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    private func configureView() {
        Bundle.main.loadNibNamed("\(type(of: self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight, UIView.AutoresizingMask.flexibleHeight]
        setupLottie()
    }
    
    private func setupLottie(){
        animatedView.layer.cornerRadius = animatedView.frame.height / 2
        animatedView.initLottieView(withName: "loading")
    }
    
}
