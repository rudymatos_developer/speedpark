//
//  SelectServiceTypeView.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/23/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

protocol SelectServiceTypeViewDelegate: class{
    func select(serviceType: SPServiceType)
    func cancelServiceTypeSelection()
}

class SelectServiceTypeView: UIView, CardCreator {

    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var serviceTypePickerView: UIPickerView!
    @IBOutlet weak var feeLBL: UILabel!
    weak var delegate: SelectServiceTypeViewDelegate?
    
    private let serviceTypes = SPServiceType.allCases
    var selectedServiceType: SPServiceType!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    private func configureView(){
        Bundle.main.loadNibNamed("\(type(of:self))", owner: self, options: nil)
        addSubview(contentView)
        contentView.frame = self.bounds
        contentView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        createCard(shadowView: shadowView, mainView: cardView)
        serviceTypePickerView.delegate = self
        serviceTypePickerView.dataSource = self
    }

    
    @IBAction func selectServiceType(_ sender: UIButton) {
        delegate?.select(serviceType: selectedServiceType)
    }
    
    @IBAction func cancelSelection(_ sender: UIButton) {
        delegate?.cancelServiceTypeSelection()
    }
    
    func configureUI(){
        if let selectedServiceType = selectedServiceType{
            self.selectedServiceType = selectedServiceType
        }else{
            self.selectedServiceType = .noSelection
        }
        configureViewWith(selectedServiceType: selectedServiceType)
    }
    
    private func configureViewWith(selectedServiceType: SPServiceType){
        feeLBL.text = selectedServiceType.getDisplayRate()
        if let index = serviceTypes.firstIndex(of: selectedServiceType){
            serviceTypePickerView.selectRow(index, inComponent: 0, animated: false)
        }
    }
    
}


extension SelectServiceTypeView: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return serviceTypes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        selectedServiceType = serviceTypes[row]
        configureViewWith(selectedServiceType: selectedServiceType)
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return serviceTypes[row].getTitle()
    }
    
}
