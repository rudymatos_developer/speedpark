//
//  CategoryCell.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/1/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryNameLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func set(category: ParkVehicleCategory){
        self.categoryNameLBL.textColor = category.selected ? UIColor(named: SpeedParkConstants.Color.main) : UIColor(named: SpeedParkConstants.Color.disabled)
        self.categoryNameLBL.text = category.name
    }

}
