//
//  ParkCarOther.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/3/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class ParkCarOther: UICollectionViewCell {

    var poso: ParkTicketPOSO!{
        didSet{
            registerBindings()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBOutlet weak var internalCommentsTV: BindiableTV!
    
    private func registerBindings(){
        internalCommentsTV.text = poso.internalComments
        internalCommentsTV.bind { [weak self] in
            self?.poso.internalComments = $0
        }
    }
    
}
