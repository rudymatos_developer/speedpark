//
//  ParkingCell.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/1/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class ParkCarOptionSelector: UICollectionViewCell, Identifiable {
    
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var descriptionLBL: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var spotSegment: UISegmentedControl!
    
    var type: PCOSType!
    var options: [PCOSOption] = []
    
    weak var viewModel: ParkVehicleVM!{
        didSet{
            configureView()
        }
    }
    
    var locationSelectorCompletion : ((Location) -> Void)?
    var selectParkingSpotSegmentCompletion : ((Location.ParkingSpot.Filter) -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        tableView.register(UINib(nibName: ParkCarOptionSelectorCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: ParkCarOptionSelectorCell.identifier)
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    @IBAction func filterSpots(_ sender: UISegmentedControl) {
        let index = sender.selectedSegmentIndex
        guard let filter  = Location.ParkingSpot.Filter(rawValue: index) else {return}
        selectParkingSpotSegmentCompletion?(filter)
    }
    
    func reloadData(){
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    private func configureView(){
        let header = viewModel.getHeader(byType: type)
        titleLBL.text = header.title
        descriptionLBL.text = header.description
        spotSegment.isHidden = viewModel.isSpotSegmentHidden(type: type)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
}


extension ParkCarOptionSelector: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let option = options[indexPath.row]
        if option.type == .spot, let spot = option.object as? Location.ParkingSpot, !spot.isAvailable{
            return
        }
        options.forEach({$0.selected = false})
        option.selected = true
        viewModel.setPoso(withOption: option)
        if option.type == .location, let location = option.object as? Location{
            locationSelectorCompletion?(location)
        }
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ParkCarOptionSelectorCell.identifier, for: indexPath) as? ParkCarOptionSelectorCell else {
            return UITableViewCell()
        }
        let option = options[indexPath.row]
        if option.type != .location{
            cell.backgroundColor = indexPath.row % 2 == 0 ? #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1) : nil
        }
        cell.booking = viewModel.booking
        cell.selectionStyle = .none
        cell.set(option: options[indexPath.row])
        return cell
    }
    
}
