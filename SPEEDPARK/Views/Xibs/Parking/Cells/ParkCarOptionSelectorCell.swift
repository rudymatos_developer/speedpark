//
//  ParkCarOptionSelectorCell.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/3/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class ParkCarOptionSelectorCell: UITableViewCell, Identifiable {

    @IBOutlet weak var parkingStatusLBL: UILabel!
    @IBOutlet weak var checkedIV: UIImageView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var iconIV: UIImageView!
    
    var booking: SPBooking!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        backgroundColor = nil
    }
    
    func set(option : PCOSOption){
        parkingStatusLBL.isHidden = true
        self.checkedIV.image = nil
        
        self.iconIV.image = UIImage(named: option.icon)
        self.titleLBL.text = option.title
        
        if option.selected{
            self.checkedIV.image = UIImage(named: "action_checked")
            checkedIV.tintColor = UIColor(named: SpeedParkConstants.Color.main)
        }
        
        if let spot = option.object as? Location.ParkingSpot{
            parkingStatusLBL.isHidden = false
            let status = spot.getStatus(byBookingReturningDate: booking.flight.arrivingDate)
            parkingStatusLBL.text = status.rawValue
            parkingStatusLBL.textColor = status.getColor()
            if !spot.isReusable && !spot.isAvailable{
                selectionStyle = .none
                checkedIV.image = UIImage(named: "action_unavailable")
                checkedIV.tintColor = status.getColor()
            }
        }
    }
    
}
