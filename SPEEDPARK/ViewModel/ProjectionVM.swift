//
//  ProjectionVM.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 2/8/20.
//  Copyright © 2020 TonyS. All rights reserved.
//

import Foundation
import Charts

class ProjectionVM: NSObject, HasDependencies{
    
    typealias Dependencies = HasDataService & HasUser & HasCarAPIService
    var dependencies: Dependencies?
    
    private let availability: [ParkingSpotAvailability]
    
    init(dependencies: Dependencies, availability: [ParkingSpotAvailability]){
        self.dependencies = dependencies
        self.availability = availability
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }

    var title: String{
        return "7 Days Projection"
    }
    
    var chartData: BarChartData {
        let data =  BarChartData(dataSet: dataSet)
        data.setValueFont(.systemFont(ofSize: 12, weight: .bold))
        data.setValueTextColor(.white)
        return data
    }
    
    
    
    private var dataSet: BarChartDataSet{
        let dataSet = BarChartDataSet(entries: entries, label: "")
        dataSet.colors = [#colorLiteral(red: 0.7800908685, green: 0.8688666821, blue: 0.6379829645, alpha: 1), #colorLiteral(red: 0.9753463864, green: 0.4815911651, blue: 0.361325264, alpha: 1)]
        dataSet.stackLabels = labels
        return dataSet
    }
    
    private var labels: [String]{
        return ["Available", "Unavailable"]
    }
    
    private var entries: [BarChartDataEntry] {
        return availability.enumerated().compactMap { day, psa -> BarChartDataEntry in
            let available = Double(psa.available.count)
            let unavailable = Double(psa.unavailable.count)
            return BarChartDataEntry(x: Double(day), yValues: [available, unavailable])
        }
    }
    
    var todaysProjection: String{
        guard let todaysAvailability = availability.first else { return " INVALID_DATA" }
        return "Available: \(todaysAvailability.available.count) - Unavailable: \(todaysAvailability.unavailable.count)"
    }
    
}
