//
//  TicketVM.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class TicketVM: NSObject, HasDependencies{
    
    typealias Dependencies = HasDataService & HasUser & HasCarAPIService
    var dependencies: Dependencies?
    
    private var mode: Mode
    var booking :SPBooking!
    
    var reparkVehicleCompletion:  (() -> Void)?
    var saveCompletion : (()->Void)?
    var genericSelectionCompletion : ((GenericSelectionVCConfigurator) -> Void)?
    
    init(dependencies: Dependencies, mode: Mode){
        self.dependencies = dependencies
        self.mode = mode
        super.init()
        initBooking()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    enum Mode: Equatable{
        static func == (lhs: TicketVM.Mode, rhs: TicketVM.Mode) -> Bool {
            switch (lhs, rhs) {
            case  (.create, .create):
                return true
            case let (.edit(b0), .edit(b1)):
                return b0 == b1
            default:
                return false
            }
        }
        
        case edit(SPBooking)
        case create
    }
    
    private func initBooking(){
        switch mode {
        case .create:
            booking = SPBooking(firebaseDocumentId: nil, data: [:])
        case .edit(let booking):
            print(booking.description)
            self.booking = booking
        }
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
}

extension TicketVM{
    func search(by genericSelectionType: GenericSelectionVCConfigurator.GenericSelectionType, completion :@escaping (Result<GenericSelectionVCConfigurator, SpeedParkError>) -> Void){
        var configurator: GenericSelectionVCConfigurator!
        switch genericSelectionType{
        case .carMake:
            let maker = GenericSelectionData(displayName: booking.vehicle.maker, info: "", additionalInfo: "", selected: false, type: .carMake)
            configurator = GenericSelectionVCConfigurator(dependencies: dependencies!, genericSelectionType: genericSelectionType, defaultValue: maker)
        case .carModel:
            guard !booking.vehicle.maker.isEmpty else{
                let error = SpeedParkError(message: "Invalid Maker: Maker cannot be empty", type: .errorGettingCarModels)
                completion(.failure(error))
                return
            }
            let maker = booking.vehicle.maker
            let model = GenericSelectionData(displayName: booking.vehicle.model, info: "", additionalInfo: "", selected: false, type: .carModel(maker))
            configurator = GenericSelectionVCConfigurator(dependencies: dependencies!, genericSelectionType: .carModel(maker), defaultValue: model)
        case .customer:
            var defaultCustomer : GenericSelectionData?
            if booking.customer.name.trimmingCharacters(in: .whitespaces) != ""{
                defaultCustomer = GenericSelectionData(displayName: booking.customer.name, info: booking.customer.phone, additionalInfo: booking.customer.email, selected: true, type: .customer)
            }
            configurator = GenericSelectionVCConfigurator(dependencies: dependencies!, genericSelectionType: .customer, defaultValue: defaultCustomer)
        }
        setDataCompletionFor(configurator: configurator, completion: completion)
        configurator.loadData()
    }
    
    private func setDataCompletionFor(configurator: GenericSelectionVCConfigurator, completion :@escaping (Result<GenericSelectionVCConfigurator, SpeedParkError>) -> Void){
        configurator.dataLoadCompletion = { [weak self] results in
            guard let self = self else {return}
            switch results{
            case .failure(let error):
                completion(.failure(error))
            case .success:
                completion(.success(configurator))
                //the following completion will handle the navigation piece in the coordinator, presenting another view after making an async request. To keep the coordinator as clean as possible I'm executing the completion in the main thread to avoid presentation in background thread.
                DispatchQueue.main.async {
                    self.genericSelectionCompletion?(configurator)
                }
            }
        }
    }
}


extension TicketVM{
    
    func isEditMode() -> Bool{
        return mode != .create
    }
    
    func setCustomerName(_ name: String){
        var components = name.trimmingCharacters(in: .whitespaces).components(separatedBy: " ")
        if components.count > 1{
            booking.customer.firstName = components.removeFirst()
            booking.customer.lastName = components.joined(separator: " ")
        }else {
            booking.customer.firstName = name
        }
    }
    
    func getTitle() -> String{
        if booking.ticketNumber.trimmingCharacters(in: .whitespaces) != "" && booking.ticketNumber != "--"{
            return "Ticket #: \(booking.ticketNumber)"
        }
        return "Reserv #: \(booking.identifier)"
    }
    
    func goToReparkVehicle(){
        reparkVehicleCompletion?()
    }
    
    func createTicketAndUpdateReservation(completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        switch mode{
        case .create:
            dependencies?.dataService.create(booking: booking, completion: completion)
        default:
            dependencies?.dataService.update(booking: booking)
            completion(.success(true))
        }
    }
}

extension TicketVM: SPDateFormatter{}

extension TicketVM{
    
    func getPhotoText(photos: [String]) -> String{
        let count = photos.count
        switch count {
        case 0:
            return "NO PHOTOS ADDED"
        case 1:
            return "THERE'S 1 PHOTO"
        default:
            return "THERE ARE \(count) PHOTOS"
        }
    }
    
    
}
