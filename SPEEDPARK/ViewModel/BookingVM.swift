//
//  MainVM.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

class BookingVM: NSObject, HasDependencies{
    
    typealias Dependencies = HasUser & HasDataService & HasMessageService & HasSoundService
    var dependencies: BookingVM.Dependencies?
    private let configurator: BookingVCConfigurator!
    
    var assignTicketNumber : ((SPBooking)-> Void)?
    var parkVehicle : ((SPBooking)-> Void)?
    var acceptBooking: ((SPBooking)-> Void)?
    var moveToActive: ((SPBooking)-> Void)?
    var bookingDetails : ((SPBooking) -> Void)?
    var newReservation : (() -> Void)?
    var payTicket : ((BookingVCConfigurator, SPBooking) -> Void)?
    
    init(dependencies: Dependencies, configurator: BookingVCConfigurator){
        self.dependencies = dependencies
        self.configurator = configurator
    }
    
    func loadTicket(completion: @escaping (()->Void)){
        configurator.dataLoaderCompletion = completion
        configurator.dataLoader()
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
}

extension BookingVM{
    
    func isPhoneNumberAvailable(booking: SPBooking) -> Bool{
        return !booking.customer.phone.isEmpty
    }
    
    func sortResultsBy(sorter: BookingVCConfigurator.BookingSorter){
        configurator.sort(with: sorter)
    }
    
    func filterResultsBy(filter: BookingVCConfigurator.BookingFilter){
        configurator.filter(with: filter)
    }
    
    func returnCarToPark(booking: SPBooking){
        dependencies?.dataService.returnVehicle(booking: booking)
    }
    
    func completeTicket(booking: SPBooking){
        dependencies?.dataService.change(booking: booking, toStatus: .done) { _ in }
    }
    
    func showWaitingTime(booking: SPBooking){
        moveToActive?(booking)
    }
    
    func goToBookingDetails(booking: SPBooking){
        bookingDetails?(booking)
    }
    
    func goToPayTicket(booking: SPBooking){
        payTicket?(configurator, booking)
    }
    
    func createNewReservation(){
        newReservation?()
    }
    
    func changeBookingToNoShow(booking: SPBooking){
        dependencies?.dataService.change(booking: booking, toStatus: .noShow) {_ in }
    }
    
    func callCustomer(booking: SPBooking){
        var phoneNumber = booking.customer.phone
        func getValidPhoneNumber() -> URL?{
            let invalidCharacters = ["-"," ","(",")","+"]
            invalidCharacters.forEach({phoneNumber = phoneNumber.replacingOccurrences(of: $0, with: "")})
            return URL(string: "tel://\(phoneNumber)")
        }
        guard let url = getValidPhoneNumber() else {
            return
        }
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
    
    func reject(booking: SPBooking){
        dependencies?.dataService.change(booking: booking, toStatus: .parked) { [weak self] _ in
            #if DEBUG
            print("Simulating Sending Reject SMS to Final User")
            #else
            self?.dependencies?.messageService.sendBookingRejectionSMS(booking: booking, completion: { _ in})
            #endif
        }
    }
    
    func swapParkingSpots(booking: SPBooking, completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        dependencies?.dataService.swapParkingSpots(booking: booking, completion: completion)
    }
    
    func checkKeysIn(booking: SPBooking){
        dependencies?.dataService.check(keysIn: true, booking: booking)
    }
    
    
}

extension BookingVM{
    func getDoneWithNoPaidOrPendingBalance(booking: SPBooking) -> String{
        if !booking.payment.paid{
            return "This ticket: \(booking.ticketNumber) hasn't been paid yet. Do you want to continue completing ticket without paying it?"
        }else{
            return "This ticket: \(booking.ticketNumber) present a pending balance. Do you want to continue completing ticket without paying it?"
        }
    }
    
    func getDoneMessage(booking: SPBooking) -> String{
        return "Do you want to complete this ticket: \(booking.ticketNumber)"
    }
    
    func getParkMessage(booking: SPBooking) -> String{
        return "Do you want to return the car for Ticket: \(booking.ticketNumber)"
    }
    
    func getNoShowMessage(booking: SPBooking) -> String{
        return "Doi you want to change the following reservation : \(booking.identifier) to No Show?"
    }
    
    func getMoveToActionMessage(booking: SPBooking) -> String{
        return "Do you want to move Ticket: \(booking.ticketNumber) to Active?"
    }
    
    func callMessage(booking: SPBooking) -> String{
        return "Do you want to call customer: \(booking.customer.name)"
    }
    
    
    func getCheckKeysInMessage(booking: SPBooking) -> String{
        return "Do you want to check the car's keys in for Ticket: \(booking.ticketNumber)"
    }
    
    func getRejectBookingMessage(booking: SPBooking) -> String{
        return "Do you want to reject this booking \(booking.identifier)"
    }
    
    func getSwapingMessage(booking: SPBooking) -> String{
        return "Do you really want to swap the vehicle in Parking Spot: \(booking.parking.spot) to \(booking.parking.spot.split(separator: "A")[0])"
    }
    
}


extension BookingVM{
    
    func shouldShowPrepaidAction(forBooking booking: SPBooking) ->Bool{
        guard let exitDate = booking.exitDate else {
            return false
        }
        return !booking.payment.prePaid && Date() < exitDate
    }
    
    func getViewName() -> String{
        return configurator.title
    }
    
    func getBookingType() -> BookingVCConfigurator.BookingType{
        return configurator.bookingType
    }
    
    func getBarTintColor() -> UIColor{
        return configurator.bookingType.getColor()
    }
    
    func getBooking(byIndex: Int) -> SPBooking{
        return configurator.filteredBookings[byIndex]
    }
    
    func getNumberOfRows() -> Int{
        return configurator.getFilteredResultsCount()
    }
    
    func getHeightBy(row: Int) -> CGFloat{
        let booking = configurator.filteredBookings[row]
        switch booking.status {
        case .active:
            return 115
        case .booked:
            return 75
        case .checkedIn, .new:
            return 60
        case .parked:
            return 75
        default:
            return 90
        }
    }
    
    func goToAssignTicketNumber(booking: SPBooking){
        assignTicketNumber?(booking)
    }
    
    func goToParkVehicle(booking: SPBooking){
        parkVehicle?(booking)
    }
    
    func filterBy(value: String){
        configurator.filterBy(value: value)
    }
    
    func getTotalResults() -> String{
        return "Total Results: \(configurator.getFilteredResultsCount())"
    }
    
    func getBookingCount() -> Int{
        return configurator.bookings.count
    }
    
    func goToAcceptBooking(booking: SPBooking){
        acceptBooking?(booking)
    }
    
}
