//
//  ParkingVehicleVM.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/3/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class ParkVehicleVM: NSObject, HasDependencies{
    
    enum Mode{
        case normal
        case repark
    }
    
    typealias Dependencies = HasDataService & HasUser
    var dependencies: ParkVehicleVM.Dependencies?
    
    @objc var poso : ParkTicketPOSO!
    var booking: SPBooking!
    var categories = ParkVehicleCategory.getCategories()
    var parkCompletion : ((SPBooking) -> Void)?
    
    private var loggedInUsers: [SPUser]?
    private var filteredParkingSpots : [Location.ParkingSpot]?
    private var currentFilter: Location.ParkingSpot.Filter = .all
    private var mode: Mode = .normal
    
    init(dependencies: Dependencies, booking: SPBooking, mode: Mode = .normal){
        self.dependencies = dependencies
        self.booking = booking
        self.poso = ParkTicketPOSO()
        self.mode = mode
        super.init()
        initPoso()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    private func initPoso(){
        guard let user = dependencies?.user else {return}
        poso.parkedBy = user
        poso.location = user.locations?.filter({$0.isDefault}).first
    }
    
    func parkVehicle(completion: @escaping (() -> Void)){

        if isBookingNoKeys(){
           poso.keyStatus = .noKeys
        }
        
        if mode == .repark{
            dependencies?.dataService.cleanParkingSpotInformationFor(booking: booking)
        }
        
        dependencies?.dataService.parkVehicle(booking: booking, withPoso: poso, completion: completion)
        parkCompletion?(booking)
    }
    
    func getConfirmationTitle() -> String{
        return "Do you really want to park this: \(booking.vehicle.maker) - \(booking.vehicle.model) from customer: \(booking.customer.name) to \(poso.location?.name ?? "INVALID_LOCATION") \(poso.spot?.name ?? "INVALID_SPOT")"
    }
    
    func isBookingNoKeys() -> Bool{
        return booking.parking.keyStatus == .noKeys
    }
    
    func getTicketNumber() -> String{
        return "\(booking.ticketNumber)"
    }
    
    func changeCategory(forItem item: Int){
        categories.first(where: {$0.selected})?.selected = false
        categories[item].selected = true
    }
    
    func loadAdditionalInformation(completion: ((Result<Bool, SpeedParkError>) -> Void)?){
        guard let user = dependencies?.user else {return}
        let company = user.company
        
        var spError : SpeedParkError?
        
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        dependencies?.dataService.getAllParkingSpots(forCompany: company, completion: { result in
            switch result{
            case .success(let parkingSpots):
                user.locations?.forEach({ (location) in
                    location.parkingSpots = []
                    location.parkingSpots?.append(contentsOf: parkingSpots.filter({$0.location == location.documentId}))
                })
            case .failure(let error):
                spError = error
            }
            dispatchGroup.leave()
        })
        
        dispatchGroup.enter()
        dependencies?.dataService.getAllLoggedInUsers{ [weak self] result in
            switch result{
            case .success(let users):
                self?.loggedInUsers = users
            case .failure(let error):
                spError = error
            }
            dispatchGroup.leave()
        }
        dispatchGroup.notify(queue: .main){
            guard spError == nil else{
                completion?(.failure(spError!))
                return
            }
            completion?(.success(true))
        }
    }
    
}

extension ParkVehicleVM{
    
    func fillUpFilteredParkingSpots(withFilter filter: Location.ParkingSpot.Filter){
        currentFilter = filter
    }
    
    func getOptions(forType type: PCOSType) -> [PCOSOption]?{
        switch type {
        case .location:
            return dependencies?.user?.locations?.sorted(by: {$0.order < $1.order}).compactMap({PCOSOption.init(title: $0.name, selected: poso.location == $0 ? true : false, type: .location, object: $0)})
        case .spot:
            return getParkingSpots()
        case .valet:
            return loggedInUsers?.compactMap({PCOSOption.init(title: $0.displayName, selected: $0.email == poso.parkedBy?.email ? true : false, type: .valet, object: $0)})
        case .other:
            return nil
        }
    }
    
    private func getParkingSpots() -> [PCOSOption]?{
        guard let fps = dependencies?.user?.locations?.first(where: {$0 == poso.location})?.parkingSpots else {return nil}
        switch currentFilter {
        case .all:
            return fps.compactMap({PCOSOption.init(title: $0.name, selected: poso.spot == $0, type: .spot, object: $0)}).sorted(by: {$0.title < $1.title})
        case .available:
            return fps.filter({$0.isAvailable || $0.isReusable}).compactMap({PCOSOption.init(title: $0.name, selected: poso.spot == $0, type: .spot, object: $0)}).sorted(by: {$0.title < $1.title})
        }
    }
    
    func isSpotSegmentHidden(type: PCOSType) -> Bool{
        return type != .spot
    }
    
    func cleanUpPosoSpot(){
        poso.spot = nil
    }
    
    func setPoso(withOption option: PCOSOption){
        switch option.type {
        case .location:
            poso.location = (option.object as? Location) ?? nil
        case .spot:
            poso.spot = (option.object as? Location.ParkingSpot) ?? nil
        case .valet:
            poso.parkedBy = (option.object as? SPUser) ?? nil
        case .other:
            print("do nothing")
        }
    }
    
    func getHeader(byType type: PCOSType) -> (title: String, description: String){
        switch type {
        case .location:
            return (title : "Select a Parking Location", description: "This is your first step. Select aparking location and go to the Spot Tab. Parking spots related to the selected location are going to be shown.")
        case .spot:
            return (title:"Select a Parking Spot", description : "Now you must select a parking spot. Parking spot are going to shown depending on their availability.")
        case .valet:
            return (title:"Select a Valet", description : "By default, you are going to be selected as the assigned Valet but you can select one of your collegues to park this car.")
        case .other:
            return (title: "", description: "")
        }
    }
    
}
