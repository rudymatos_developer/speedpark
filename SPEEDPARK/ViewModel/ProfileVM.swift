//
//  ProfileVM.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 5/29/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class ProfileVM: HasDependencies, SPDateFormatter{
    
    var logoutCompletion: (()->Void)?
    var presentProjectionCompletion: (([ParkingSpotAvailability]) -> Void)?
    
    typealias Dependencies = HasUser & HasAuthorizableService & HasDataService
    var dependencies: ProfileVM.Dependencies?
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
    }
    
    func getDisplayName() -> String{
        return dependencies?.user?.displayName ?? "Invalid Name"
    }
    
    func getEnterTime() -> String{
        guard let date = dependencies?.user?.loggedInDate else{
            return "Login Date is not Available right now"
        }
        return "Enter Time: \(convert(date: date, usingPattern: .full))"
    }
    
    func isCurrentUserAdmin() -> Bool{
        return dependencies?.user?.isAdmin ?? false
    }
    
    func logout(completion: @escaping (() -> Void)){
        dependencies?.authorizableService?.logout(completion: { [weak self] result in
            guard let self = self else {return}
            completion()
            self.logoutCompletion?()
        })
    }
}

extension ProfileVM{
    
    func goToProjection(availability: [ParkingSpotAvailability]){
        presentProjectionCompletion?(availability)
    }
    
    func generateProjectionReport(completion: @escaping (Result<[ParkingSpotAvailability], Error>) -> ())  {
        guard let user = dependencies?.user else {
            completion(.failure(NSError()))
            return
        }
        let company = user.company
        dependencies?.dataService.getAllParkingSpots(forCompany: company, completion: { result in
            switch result{
            case .success(let parkingSpots):

                var projection : [ParkingSpotAvailability] = []
                let availableSpots = parkingSpots.filter({$0.isAvailable})
                let unavailableSpots = parkingSpots.filter({!$0.isAvailable})
                
                guard let today = Calendar.current.date(bySettingHour: 23, minute: 59, second: 59, of: Date()) else {
                    let error = SpeedParkError(message: "Error creating Report", type: .errorGettingParkSpots)
                    completion(.failure(error))
                    return
                }
                
                let totalNumberOfDays = 7
                (0..<totalNumberOfDays).forEach { day in
                    if let currentDate = Calendar.current.date(byAdding: .day, value: day, to: today) {
                        var availableForDay = unavailableSpots.filter { ps -> Bool in
                            guard let returningDate = ps.returningDate else {
                                return true
                            }
                            return  returningDate >= Calendar.current.startOfDay(for: currentDate) && returningDate <= currentDate
                        }
                        availableForDay.append(contentsOf: availableSpots)
                        let unavailableForDay = unavailableSpots.filter({!availableForDay.contains($0)})
                        projection.append(ParkingSpotAvailability(date: currentDate, available: availableForDay, unavailable: unavailableForDay))
                    }
                    
                }
                completion(.success(projection))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}

