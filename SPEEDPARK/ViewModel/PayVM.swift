//
//  PayVM.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/21/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class PayVM: HasDependencies, SPDateFormatter{

    enum Mode{
        case normal
        case prepaid
    }
    
    let cannotCalculateMessage = "Cannot Calculate"
    let invalidDateMessage = "INVALID DATE"
    
    typealias Dependencies = HasDataService
    var dependencies: Dependencies?
    
    var booking: SPBooking
    var poso: PaymentPOSO
    var mode: Mode = .normal
    
    private var originalServiceType : SPServiceType
    private var numberOfDays = 0
    
    var confirmPaymentCompletion : ((ChangePOSO) -> Void)?
    var selectTipsCompletion: ((PaymentPOSO) -> Void)?
    
    init(dependencies: Dependencies, booking : SPBooking, mode: PayVM.Mode = .normal){
        print("working with \(booking.description)")
        self.dependencies = dependencies
        self.booking = booking
        self.mode = mode
        self.poso = PaymentPOSO()
        self.poso.serviceType = booking.payment.serviceType.getTitle()
        self.originalServiceType = booking.payment.serviceType
        poso.currentTotalWithNoTips = calculateChargeAmount()
    }
 
    func getPaymentMessage() -> String{
        return "Total Amount : \(getTotalPrice())"
    }
    
    func cancelPayment(){
        booking.payment.serviceType = originalServiceType
    }
    
    func creditCardPayment(){
        if mode == .normal{
            dependencies?.dataService.pay(booking: booking, withAmout: calculateTotalPrice(), totalDays: numberOfDays)
        }else{
            dependencies?.dataService.prePay(booking: booking, withAmout: calculateTotalPrice(), totalDays: numberOfDays)
        }
    }
    
    func cashPayment(){
        let totalPrice = calculateTotalPrice()
        guard totalPrice > 0 else{
            return
        }
        let changePOSO = ChangePOSO(booking: booking, total: totalPrice, totalDays: numberOfDays, mode: mode)
        confirmPaymentCompletion?(changePOSO)
    }
}

extension PayVM{
    
    func set(serviceType: SPServiceType){
        booking.payment.serviceType = serviceType
        poso.serviceType = serviceType.getTitle()
        poso.currentTotalWithNoTips = calculateChargeAmount()
    }
    
    func setTip(amount: String){
        guard let doubleAmount = Double(amount.replacingOccurrences(of: "$", with: "")) else{
            return
        }
        poso.tip = SPTip.custom(doubleAmount)
    }
    
    func set(tip: SPTip){
        poso.tip = tip
    }
    
}

//Displayable Content
extension PayVM{
    
    func getServiceType() -> String{
        return poso.serviceType
    }
    
    func getSelectedTip() -> SPTip{
        return poso.tip
    }
    
    func getSubTotal() -> String{
        return String(format: "%.02f",calculateSubTotal())
    }
    
    func getTaxRate() -> String{
        let subTotal = calculateSubTotal()
        return "\(calculateRoundedTaxes(subTotal: subTotal)).00"
    }
    
    func getChargeAmount() -> String{
        return String(format: "%.02f",calculateChargeAmount())
    }
    
    func getTipsFromAmout() -> String{
        let selectedTip = poso.tip
        
        switch selectedTip {
        case .none:
            return "$0.00"
        default:
            let tipTotal = selectedTip.getPercentage(fromValue: calculateChargeAmount())
            return String(format: "%.02f",tipTotal)
        }
    }
    
    func getTotalTips() -> String{
        return poso.tip.getDisplayName()
    }
    
    func getTotalPrice() -> String{
        return String(format: "$%.02f", calculateTotalPrice())
    }
    
    func getStarDisplayDate() -> String{
        guard let startDate = booking.getBookingPayableStartDate() else {
            return invalidDateMessage
        }
        
        let scanDateFormmatted = convert(date: startDate, usingPattern: .speedParkLongFormatDisplay)
        guard !scanDateFormmatted.isEmpty else{
            return invalidDateMessage
        }
        return scanDateFormmatted
    }
    
    func getEndDisplayDate() -> String{
        guard let endDate = booking.getBookingPayableEndDate(mode: mode) else{
            return invalidDateMessage
        }
        return convert(date: endDate, usingPattern: .speedParkLongFormatDisplay)
    }
    
    func getTotalStay() -> String{
        guard let startDate = booking.getBookingPayableStartDate(), let endDate = booking.getBookingPayableEndDate(mode: mode) else{
            return cannotCalculateMessage
        }
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.year, .month,.day, .hour, .minute]
        formatter.unitsStyle = .abbreviated
        return formatter.string(from: startDate, to: endDate) ?? cannotCalculateMessage
    }
    
}


extension PayVM{
    
    func calculateChargeAmount() -> Double{
        let subTotal = calculateSubTotal()
        let taxes = calculateRoundedTaxes(subTotal: subTotal)
        return subTotal + Double(taxes)
    }
    
    func calculateSubTotal() -> Double{
        guard let startDate = booking.getBookingPayableStartDate(),
            let endDate = booking.getBookingPayableEndDate(mode: mode),
            var  totalHours = Calendar.current.dateComponents([.hour], from: startDate, to: endDate).hour else{
                return 0
        }
        if totalHours <= 0{
            totalHours = 1
        }
        numberOfDays = calculateTotalDays(hours: totalHours)
        let total : Double = Double(numberOfDays) * booking.payment.dailyRate
        return total
    }
    
    func calculateRoundedTaxes(subTotal: Double) -> Int{
        var taxes = 0
        if booking.payment.taxRate > 0{
            taxes = Int(subTotal * booking.payment.taxRate)
        }
        return taxes
    }
    
    func calculateTotalPrice() -> Double{
        var subTotal = calculateSubTotal()
        subTotal += Double(calculateRoundedTaxes(subTotal: subTotal))
        let tip = poso.tip.getPercentage(fromValue: subTotal)
        subTotal += tip
        return subTotal
    }
    
    private func calculateTotalDays(hours: Int) -> Int{
        var total =  Int(hours / 24)
        if hours % 24 > 0{
            total += 1
        }
        return total
    }
    
}

