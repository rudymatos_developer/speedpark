//
//  ChangeVM.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/23/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

struct ChangePOSO{
    var booking: SPBooking
    var total: Double
    var totalDays: Int
    var mode: PayVM.Mode
}

class ChangeVM: HasDependencies{
    
    private var inCashAMount  = 0.0
    private var cashToReturn =  0.0
    private var poso: ChangePOSO
    
    typealias Dependencies = HasDataService
    var dependencies: Dependencies?
    var paymentCompletion : (()->Void)?
    
    init(dependencies: Dependencies, poso: ChangePOSO){
        self.dependencies = dependencies
        self.poso = poso
    }
    
    func pay(){
        if poso.mode == .normal{
            dependencies?.dataService.pay(booking: poso.booking, withAmout: poso.total, totalDays: poso.totalDays)
        }else{
            dependencies?.dataService.prePay(booking: poso.booking, withAmout: poso.total, totalDays: poso.totalDays)
        }
    }
    
    func callPaymentCompletion(){
        paymentCompletion?()
    }
    
    
    func setInCash(amount: Double){
        inCashAMount = amount
    }
    
    func calculateReturn(){
        cashToReturn = inCashAMount - poso.total
    }
    
    func getTicketNumber() -> String{
        return poso.booking.ticketNumber
    }
    
    func getTotal() -> Double{
        return poso.total
    }
    
    func getFormattedTotal() -> String{
        return "$\(String(format: "%.02f", poso.total))"
    }
    
    func getPayMessage() -> String{
        return "You will pay ticket \(poso.booking.ticketNumber). Total: \(getFormattedTotal()). Do you want to continue?"
    }
    
    func getInCashValue(amountString: String) -> Double?{
        return Double(amountString.replacingOccurrences(of: "$", with: ""))
    }
    
    func getInCashAmountDisplayString() -> String{
        return "$\(String(format: "%.02f", inCashAMount))"
    }
    
    func getCashToReturn() -> String{
        return "$\(String(format: "%.02f", cashToReturn))"
    }
    
    
}
