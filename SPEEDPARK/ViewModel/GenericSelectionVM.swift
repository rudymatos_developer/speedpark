//
//  GenericSelectionVM.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 6/20/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class GenericSelectionVM: NSObject, HasDependencies{
    
    typealias Dependencies = HasUser
    var dependencies: GenericSelectionVM.Dependencies?
    private var configurator: GenericSelectionVCConfigurator
    @objc var poso : GenericSelectionPOSO!
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    init(dependencies: Dependencies, configurator: GenericSelectionVCConfigurator){
        self.dependencies = dependencies
        self.configurator = configurator
        self.poso = GenericSelectionPOSO()
    }
    
    func getConfiguratorType() -> GenericSelectionVCConfigurator.GenericSelectionType{
        return configurator.genericSelectionType
    }
    
    func getNumberOfRows() -> Int{
        return configurator.filteredResults.count
    }
    
    func getOption(byRow: Int) -> GenericSelectionData{
        return configurator.filteredResults[byRow]
    }
    
    func selectOption(at: Int){
        let option = getOption(byRow: at)
        poso.selection = option
        configurator.select(value: option)
    }
    
    func filter(by: String){
        configurator.filter(by: by)
    }
    
    func getSelectedEntry() -> GenericSelectionData?{
        return poso.selection
    }
    
    func getHeight(byRow: Int) -> Int{
        let option = getOption(byRow: byRow)
        return option.type == .customer ? 65 : 35
    }
    
    func getEnterManualEntryTitle() -> String{
        switch configurator.genericSelectionType{
        case .carMake:
            return "Enter Value for Car Maker"
        case .customer:
            return "Enter Value for Customer Name"
        case .carModel:
            return "Enter Value for Car Model"
        }
    }
    
    func getInstructions() -> String{
        return "Please enter a manual and valid value"
    }
    
}
