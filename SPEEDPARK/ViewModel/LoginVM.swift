//
//  LoginVM.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class LoginVM: NSObject, HasDependencies{
    
    typealias Dependencies = HasAuthorizableService & HasUserDefaultService & HasUser & HasDataService
    var dependencies: Dependencies?
    
    private var email = ""
    private var password = ""
    
    var loginCompletion: ((Result<Bool, SpeedParkError>)->Void)?
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    
    func set(email: String){
        self.email = email
    }

    func set(password: String){
        self.password = password
    }
    
    func validate() -> Bool{
        guard email.trimmingCharacters(in: .whitespaces) != "" && password.trimmingCharacters(in: .whitespaces) != "" else{
            return false
        }
        return true
    }

    func login(completion: @escaping (Result<Bool, SpeedParkError>) -> Void){
         guard validate() else {
            completion(.failure(SpeedParkError(message: "Invalid Username or Password", type: .loginError)))
            return
        }
        dependencies?.authorizableService?.login(email: email, password: password) { result in
            switch result{
            case .success:
                self.loginCompletion?(.success(true))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
