//
//  ScanVM.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/31/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class ScanVM: NSObject, HasDependencies{
    
    typealias Dependencies = HasDataService & HasMessageService
    var dependencies: ScanVM.Dependencies?
    
    private var booking: SPBooking
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    init(dependencies: Dependencies, booking: SPBooking){
        self.dependencies = dependencies
        self.booking = booking
    }
    
    func getInstructionsText() -> String{
        return "Assign Ticket to Reservation \(booking.identifier)"
    }
    
    func assign(_ ticketNumber: String, completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        booking.ticketNumber = ticketNumber
        dependencies?.dataService.change(booking: booking, toStatus: .checkedIn) { _ in }
        dependencies?.dataService.assign(ticketNumber: ticketNumber, toBooking: self.booking) { _ in}
        #if DEBUG
        completion(.success(true))
        #else
        dependencies?.messageService.sendTicketAssignedSMSNotification(booking: self.booking, completion: completion)
        #endif
    }
}
