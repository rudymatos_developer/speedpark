//
//  TimeVM.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/18/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class TimeVM: NSObject, HasDependencies{
    
    private let selectOneOption = "Select one..."
    let defaultValue = "10 minutes"
    @objc dynamic var selectedMinutes = ""
    private var minutes: [String] =  []
    typealias Dependencies = HasDataService & HasMessageService
    var dependencies: Dependencies?
    
    private var booking: SPBooking!
    
    init(dependencies: Dependencies, booking: SPBooking) {
        self.dependencies = dependencies
        selectedMinutes = defaultValue
        self.booking = booking
        minutes.append(selectOneOption)
        minutes.append(contentsOf: (1..<61).compactMap({"\($0) minutes"}))
    }
    
    func getInstructionsText() -> String {
        return "Approximate Time for Ticket: \(booking.ticketNumber)"
    }
    
    func getIndex(forMinutes: String) -> Int? {
        return minutes.firstIndex(of: forMinutes)
    }
    
    func set(minutes: String) {
        selectedMinutes = minutes
    }
    
    func getNumberOfRows() -> Int {
        return minutes.count
    }
    
    func getMinutes(byIndex: Int) -> String {
        return minutes[byIndex]
    }
    
    func isValidPickUpTime() -> Bool {
        return selectedMinutes != selectOneOption
    }
    
    func setPickUpTime(completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        guard isValidPickUpTime(),let minutes = Int(selectedMinutes.split(separator: " ")[0]) else {return}
        booking.customer.waitingTime = minutes
        dependencies?.dataService.change(booking: booking, toStatus: .active) { [weak self] _ in
            guard let self = self else {return}
            #if DEBUG
            completion(.success(true))
            #else
            self.dependencies?.messageService.sendApproximateWaitSMSNotification(booking: self.booking,
                                                                                 minutes: self.selectedMinutes,
                                                                                 completion: completion)
            #endif
        }
    }
    
}
