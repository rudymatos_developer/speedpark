//
//  SoundService.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/19/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import AVFoundation

protocol HasSoundService: class{
    var soundService : SoundService? {get set}
}

class SoundService: HasDependencies{
    
    typealias Dependencies = HasDataService
    var dependencies: Dependencies?
    
    enum Sound: String, CaseIterable{
        case alarm1 = "alarm_1"
        case alarm2 = "alarm_2"
        case alarm3 = "alarm_3"
        case alarm4 = "alarm_4"
        case alarm5 = "alarm_5"
        
        func getURL() -> URL?{
            return Bundle.main.url(forResource: self.rawValue, withExtension: "mp3")
        }
    }
    
    private var player : AVAudioPlayer?
    private var timer : Timer?
    
    private var isCurrentlyPlaying = false
    private var isThereAnyAlarmToTrigger = false
    private var minutesCounter = 0
    private var playingSound: Sound = .alarm1
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
        do{
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default, options: [.mixWithOthers, .allowAirPlay])
            try AVAudioSession.sharedInstance().setActive(true)
        }catch let error{
            print(error)
        }
        listenToAlarm()
    }
    
    @objc private func reTriggerAlarm(){
        if isThereAnyAlarmToTrigger{
            minutesCounter += 1
            if minutesCounter >= 3{
                loadAndPlay(sound: playingSound, shouldOverrideSound: false)
            }
        }else{
            minutesCounter = 0
        }
    }
    
    private func listenToAlarm(){
        dependencies?.dataService.listenToAlarm(completion: { [weak self] result in
            switch result{
            case .success(let result):
                if result.customerAlarm{
                    self?.isThereAnyAlarmToTrigger = true
                    self?.loadAndPlay(sound: .alarm2, shouldOverrideSound: true)
                }else if result.keysAlarm{
                    self?.isThereAnyAlarmToTrigger = true
                    self?.loadAndPlay(sound: .alarm5, shouldOverrideSound: true)
                }else{
                    self?.isThereAnyAlarmToTrigger = false
                    self?.stopCurrentSound()
                }
            case .failure:
                self?.isThereAnyAlarmToTrigger = false
                self?.stopCurrentSound()
            }
        })
        timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(SoundService.reTriggerAlarm), userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    func loadAndPlay(sound: Sound, shouldOverrideSound: Bool = false){
        
        if shouldOverrideSound{
            player?.stop()
            isCurrentlyPlaying = false
        }
        
        if !isCurrentlyPlaying{
            guard let url = sound.getURL() else {return}
            if player == nil{
                player = try? AVAudioPlayer(contentsOf: url)
            }else{
                if player?.isPlaying ?? false{
                    stopCurrentSound()
                }
                player = try? AVAudioPlayer(contentsOf: url)
            }
            playingSound = sound
            minutesCounter = 0
            player?.setVolume(1.0, fadeDuration: 0)
            player?.numberOfLoops = -1
            player?.prepareToPlay()
            player?.play()
            DispatchQueue.main.asyncAfter(deadline: .now() + 60) {
                self.stopCurrentSound()
            }
            isCurrentlyPlaying = true
        }
    }
    
    func stopCurrentSound(){
        isCurrentlyPlaying = false
        player?.setVolume(0, fadeDuration: 3)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.player?.stop()
        }
    }
}
