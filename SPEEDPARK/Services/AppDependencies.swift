//
//  AppDependencies.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class AppDependencies: HasUser, HasUserDefaultService, HasDataService, HasAuthorizableService, HasMessageService, HasSoundService, HasCarAPIService{
    
    var user: SPUser?
    var userDefaultService: UserDefaultService
    var dataService: DataService
    var authorizableService: AuthorizableService?
    var messageService: MessageService
    var soundService: SoundService?
    var carAPIService: CarAPIService?

    init(userDefaultService: UserDefaultService, dataService : DataService, messageService: MessageService){
        self.userDefaultService = userDefaultService
        self.dataService = dataService
        self.messageService = messageService
    }
    
}
