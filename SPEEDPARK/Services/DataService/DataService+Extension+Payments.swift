//
//  DataService+Extension+Payments.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 5/25/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

extension DataService{
    
    func pay(booking: SPBooking, withAmout: Double, totalDays: Int){
        guard let bookingDocumentId = booking.firebaseDocumentId, booking.payment.serviceType != .noSelection else{
            return
        }
        var payDictionary : [String:Any] = ["paid" : true]
        payDictionary["totalCharge"] =  withAmout
        payDictionary["totalDays"] =  totalDays
        payDictionary["serviceType"] =  booking.payment.serviceType.rawValue
        payDictionary["paidDate"] =  Date()
        db.collection(SpeedParkConstants.Collections.bookings).document(bookingDocumentId).setData(payDictionary, merge: true)
    }
    
    func prePay(booking: SPBooking, withAmout: Double, totalDays: Int){
        guard let bookingDocumentId = booking.firebaseDocumentId, booking.payment.serviceType != .noSelection else{
            return
        }
        var payDictionary : [String:Any] = ["prePaid" : true]
        payDictionary["prePaidTotalCharge"] =  withAmout
        payDictionary["prePaidTotalDays"] =  totalDays
        payDictionary["prePaidTerviceType"] =  booking.payment.serviceType.rawValue
        payDictionary["prePaidDate"] =  Date()
        db.collection(SpeedParkConstants.Collections.bookings).document(bookingDocumentId).setData(payDictionary, merge: true)
    }
    
}
