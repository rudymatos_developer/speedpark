//
//  DataService.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import Firebase

protocol HasDataService{
    var dataService: DataService {get set}
}

class DataService{
    var db = Firestore.firestore()
}

extension DataService: SPDateFormatter{
    
    func assign(ticketNumber: String, toBooking booking: SPBooking, completion: @escaping ((Error?) -> Void)){
        guard let documentId = booking.firebaseDocumentId else {return}
        let data: [String:Any] = [SpeedParkConstants.Fields.Booking.ticketNumber: ticketNumber]
        if booking.payment.serviceType == .spotHero{
            prePay(booking: booking, withAmout: 0, totalDays: 0)
        }
        db.collection(SpeedParkConstants.Collections.bookings).document(documentId).setData(data, merge: true, completion: completion)
    }
    
    func check(keysIn: Bool, booking: SPBooking){
        guard let documentId = booking.firebaseDocumentId else {return}
        var data : [String: Any] = [:]
        data[SpeedParkConstants.Fields.Parking.keyStatus] = keysIn ? SPBooking.Parking.KeyStatus.keysCheckedIn.rawValue : SPBooking.Parking.KeyStatus.keysNotCheckedIn.rawValue
        db.collection(SpeedParkConstants.Collections.bookings).document(documentId).setData(data, merge: true)
    }
    
    func create(booking: SPBooking, completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        getNextBookingNumber { [weak self] result in
            guard let self = self else {return}
            switch result{
            case .failure(let error):
                completion(.failure(error))
            case .success(let bookingNo):
                booking.identifier = bookingNo
                let enterDate = self.convert(date: Date(), usingPattern: .main)
                let enterTime = self.convert(date: Date(), usingPattern: .shortTime)
                var dictionary = booking.parse()
                dictionary["bookingNo"] = "\(bookingNo)"
                dictionary["bookingStatus"] = SPBooking.Status.booked.rawValue
                dictionary["enterDate"] = enterDate
                dictionary["enterTime"] = enterTime
                dictionary["bookingSource"] = "SpeedPark Mobile App (iOS)"
                self.db.collection("bookings").document().setData(dictionary, merge: true)
                completion(.success(true))
            }
        }
    }
    
    func update(booking: SPBooking){
        guard let documentId = booking.firebaseDocumentId else {return}
        db.collection(SpeedParkConstants.Collections.bookings).document(documentId).setData(booking.parse(), merge: true)
    }
    
    func change(booking: SPBooking, toStatus: SPBooking.Status, completion: @escaping ((Error?) -> Void)){
        guard let documentId = booking.firebaseDocumentId else {return}
        var data : [String: Any] = [SpeedParkConstants.Fields.Booking.bookingStatus: toStatus.rawValue]
        switch toStatus{
        case .checkedIn:
            data[SpeedParkConstants.Fields.Booking.checkedInOrParkedStatus] = true
            data[SpeedParkConstants.Fields.Booking.scanDate] = Date()
            cleanBookingParkingInfoFor(booking: booking, data: &data)
            cleanParkingSpotInformationFor(booking: booking)
        case .parked:
            data[SpeedParkConstants.Fields.Booking.checkedInOrParkedStatus] = true
            data[SpeedParkConstants.Fields.Parking.parkedDate] = Date()
        case .active:
            data[SpeedParkConstants.Fields.Booking.checkedInOrParkedStatus] = false
            data[SpeedParkConstants.Fields.Customer.waitingTime] = booking.customer.waitingTime
            data[SpeedParkConstants.Fields.Customer.waitingTimeDate] = Date()
            data[SpeedParkConstants.Fields.Booking.activeDate] = Date()
        case .done:
            data["completionDate"] = Date()
            cleanParkingSpotInformationFor(booking: booking)
        default:
            data[SpeedParkConstants.Fields.Booking.checkedInOrParkedStatus] = false
        }
        db.collection(SpeedParkConstants.Collections.bookings).document(documentId).setData(data, merge: true, completion: completion)
    }
}

extension DataService{
    
    func addToLoggedInUsers(email: String){
        let loginDic :[String:Any] = ["logged-in":true, "login-date":Date()]
        db.collection("users").document(email).setData(loginDic, merge: true)
    }
    
    func removeFromLoggedIn(email: String){
        let loginDic :[String:Any] = ["logged-in":false, "login-date":""]
        db.collection("users").document(email).setData(loginDic, merge: true)
    }
    
    func getNextBookingNumber(completion: @escaping ((Result<Int, SpeedParkError>) -> Void)){
        db.collection("bookings").order(by: "bookingNo", descending: true).limit(to: 1).getDocuments { snapshot, error in
            guard error == nil, let data = snapshot?.documents.first?.data() , let bookingNo = data["bookingNo"] as? String , let nextNumber = Int(bookingNo) else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingBookingInfo)
                completion(.failure(spError))
                return
            }
            
            completion(.success(nextNumber + 1))
        }
    }
    
    func getUserInfo(email: String, completion:@escaping (Result<SPUser, SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.users).document(email).getDocument { [weak self] (snapshot, error) in
            guard let self = self else {return}
            guard error == nil, let data = snapshot?.data(), let user = SPUser(email: email, data: data) else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .loginError)
                completion(.failure(spError))
                return
            }
            self.getAllLocations(completion: { result in
                switch result{
                case .success(let locations):
                    user.locations = locations.sorted(by: {$0.order < $1.order})
                    completion(.success(user))
                case .failure(let error):
                    completion(.failure(error))
                }
            })
        }
    }
    
    func getAllLoggedInUsers(completion: @escaping (Result<[SPUser], SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.users).whereField(SpeedParkConstants.Fields.User.loggedIn, isEqualTo: true).getDocuments { (snapshot, error) in
            guard error == nil, let documents = snapshot?.documents else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingUsers)
                completion(.failure(spError))
                return
            }
            let users = documents.compactMap({SPUser.init(email: $0.documentID, data: $0.data())})
            guard  !users.isEmpty else{
                let spError = SpeedParkError(message: "Error getting data with Message : Users not available", type: .errorGettingUsers)
                completion(.failure(spError))
                return
            }
            completion(.success(users))
        }
    }
    
    func getAllCustomerNames(completion: @escaping (Result<[GenericSelectionData], SpeedParkError>) -> Void){
        db.collection("global").document("global_document").collection("customers").getDocuments { (snapshot, error) in
            
            if let error = error{
                let spError = SpeedParkError(message: "Error getting data with Message : \(error.localizedDescription)", type: .errorGettingUsers)
                completion(.failure(spError))
                return
            }

            guard let documents = snapshot?.documents, !documents.isEmpty else{
                let spError = SpeedParkError(message: "Error getting data with Message : Customers not found", type: .errorGettingUsers)
                completion(.failure(spError))
                return
            }
            
            let customers = documents.compactMap({ (doc) -> GenericSelectionData? in
                let data = doc.data()
                guard let email = data["email"] as? String, let firstName = data["firstName"] as? String,
                    let lastName = data["lastName"] as? String , let phone = data["phone"] as? String else{
                        return nil
                }
                return GenericSelectionData(displayName: "\(firstName) \(lastName)", info: phone, additionalInfo: email, selected: false, type: .customer)
            })
            completion(.success(customers))
        }
    }
}
