//
//  DataService+Extension+Notifications.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 5/30/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

// MARK: - Notification Management
extension DataService {
    
    func addNotificationToken(_ fcmToken: String){
        db.collection("global").document("global_document").collection("tokens").document(fcmToken).setData(["device_token":fcmToken], merge: true)
    }
    
    func removeNotificationToken(_ fcmToken: String) {
        db.collection("global").document("global_document").collection("tokens").document(fcmToken).delete()
    }
    
}
