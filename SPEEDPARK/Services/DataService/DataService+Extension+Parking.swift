//
//  DataService+Extension+Parking.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 5/25/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

extension DataService{
    
    func cleanBookingParkingInfoFor(booking: SPBooking, data: inout [String: Any]){
        data["parkedByEmail"] = ""
        data["parkingSpot"] = ""
        data["parkingSpotId"] = ""
        data["siteLocation"] = ""
        data["location"] = ""
        data["parkedBy"] = ""
        data["keyStatus"] = ""
    }
    
    func cleanParkingSpotInformationFor(booking: SPBooking){
        let parkingSpot = booking.parking.spotId
        if !parkingSpot.isEmpty{
            var parkingSpotUpdateDictionary : [String:Any] = ["isAvailable": true]
            parkingSpotUpdateDictionary["returningDate"] =  nil
            parkingSpotUpdateDictionary["bookingId"] =  nil
            db.collection(SpeedParkConstants.Collections.parkingSpot).document(parkingSpot).setData(parkingSpotUpdateDictionary, merge: true)
        }
    }
    
    private func assignValuesTo(_ booking: SPBooking, poso: ParkTicketPOSO, parkedBy: SPUser, spot: Location.ParkingSpot, location : Location){
        booking.parking.valet = parkedBy.displayName
        booking.parking.keyStatus = poso.keyStatus
        booking.parking.spot = spot.name
        booking.parking.spotId = spot.documentId
        booking.parking.siteLocation = location.displayName
        booking.parking.location = location.documentId
        booking.notes = poso.internalComments
    }
    
    private func generateBookingStatusUpdateDictionary(booking: SPBooking, poso: ParkTicketPOSO, parkedBy: SPUser, spot: Location.ParkingSpot, location : Location) -> [String:Any]{
        var bookingStatusUpdateDictionary : [String:Any] = ["bookingStatus" : SPBooking.Status.parked.rawValue]
        bookingStatusUpdateDictionary["parkedBy"] = parkedBy.displayName
        bookingStatusUpdateDictionary["parkedByEmail"] = parkedBy.email
        bookingStatusUpdateDictionary["parkingSpot"] = spot.name
        bookingStatusUpdateDictionary["parkingSpotId"] = spot.documentId
        bookingStatusUpdateDictionary["siteLocation"] = location.displayName
        bookingStatusUpdateDictionary["location"] = location.documentId
        bookingStatusUpdateDictionary["internalNotes"] = poso.internalComments
        bookingStatusUpdateDictionary["keyStatus"] = poso.keyStatus.rawValue
        return bookingStatusUpdateDictionary
    }
    
    
    func parkVehicle(booking: SPBooking, withPoso poso: ParkTicketPOSO, completion: @escaping (() -> Void)){
        guard let bookingDocumentId = booking.firebaseDocumentId , let location = poso.location, let spot = poso.spot,  let parkedBy = poso.parkedBy else{
            return
        }
        let status = spot.getStatus(byBookingReturningDate: booking.flight.arrivingDate)
        var bookingStatusUpdateDictionary = generateBookingStatusUpdateDictionary(booking: booking, poso: poso, parkedBy: parkedBy, spot: spot, location: location)
        if status == .swapable{
            bookingStatusUpdateDictionary[SpeedParkConstants.Fields.Booking.isSwapable] = true
        }
        if !spot.isReusable{
            var parkingSpotUpdateDictionary : [String:Any] = ["isAvailable": false]
            parkingSpotUpdateDictionary["returningDate"] =  booking.flight.arrivingDate
            parkingSpotUpdateDictionary["bookingId"] =  booking.firebaseDocumentId
            db.collection(SpeedParkConstants.Collections.parkingSpot).document(spot.documentId).setData(parkingSpotUpdateDictionary, merge: true)
        }
        assignValuesTo(booking, poso: poso, parkedBy: parkedBy, spot: spot, location: location)
        db.collection(SpeedParkConstants.Collections.bookings).document(bookingDocumentId).setData(bookingStatusUpdateDictionary, merge: true)
        completion()
    }
    
    func returnVehicle(booking: SPBooking){
        guard let documentId = booking.firebaseDocumentId else {return}
        var data : [String: Any] = [SpeedParkConstants.Fields.Booking.bookingStatus: SPBooking.Status.checkedIn.rawValue]
        data[SpeedParkConstants.Fields.Booking.checkedInOrParkedStatus] = true
        data[SpeedParkConstants.Fields.Booking.activeDate] = nil
        cleanBookingParkingInfoFor(booking: booking, data: &data)
        db.collection(SpeedParkConstants.Collections.bookings).document(documentId).setData(data, merge: true)
        cleanParkingSpotInformationFor(booking: booking)
    }
    
    func swapParkingSpots(booking: SPBooking, completion:@escaping ((Result<Bool, SpeedParkError>) -> Void)){
        self.db.collection(SpeedParkConstants.Collections.parkingSpot).whereField("company", isEqualTo: "speedpark_llc").whereField("location", isEqualTo: booking.parking.location).whereField("name", isEqualTo: booking.parking.spot).getDocuments { (snapshot, error) in
            guard error == nil, let document = snapshot?.documents.first, let originalParkingSpot = Location.ParkingSpot(documentId: document.documentID, data: document.data()), let parentParkingSpotDocumentId = originalParkingSpot.parent?.documentId else{
                return
            }
            self.db.runTransaction({ (transaction, errorPointer) -> Any? in
                do{
                    let parentParkingSpotDocumentRef = self.db.collection(SpeedParkConstants.Collections.parkingSpot).document(parentParkingSpotDocumentId)
                    let parentParkingSpotDocument = try transaction.getDocument(parentParkingSpotDocumentRef)
                    let tempParentParkingSpot = Location.ParkingSpot(documentId: parentParkingSpotDocument.documentID, data: parentParkingSpotDocument.data() ?? [:])
                    
                    guard let originalBookingId = originalParkingSpot.bookingId, let parentParkingSpot = tempParentParkingSpot, let parentBookingId = parentParkingSpot.bookingId else {
                        errorPointer?.pointee = NSError(domain: "speedpark", code: 0, userInfo: [NSLocalizedDescriptionKey: "Invalid Booking Id"])
                        return nil
                    }
                    
                    let originalBookingRef = self.db.collection(SpeedParkConstants.Collections.bookings).document(originalBookingId)
                    let parentBookingRef = self.db.collection(SpeedParkConstants.Collections.bookings).document(parentBookingId)
                    let originalParkingSpotRef = self.db.collection(SpeedParkConstants.Collections.parkingSpot).document(originalParkingSpot.documentId)
                    
                    let originalBookingDocument = try transaction.getDocument(originalBookingRef)
                    let parentBookingDocument = try transaction.getDocument(parentBookingRef)
                    
                    let originalBooking = SPBooking(firebaseDocumentId: originalBookingDocument.documentID, data: originalBookingDocument.data() ?? [:])
                    let parentBooking = SPBooking(firebaseDocumentId: parentBookingDocument.documentID, data: parentBookingDocument.data() ?? [:])
                    
                    transaction.setData(["parkingSpot": parentBooking.parking.spot], forDocument: originalBookingRef, merge: true)
                    transaction.setData(["parkingSpotId": parentBooking.parking.spotId], forDocument: originalBookingRef, merge: true)
                    transaction.setData(["parkingSpot": originalBooking.parking.spot], forDocument: parentBookingRef, merge: true)
                    transaction.setData(["parkingSpotId": originalBooking.parking.spotId], forDocument: parentBookingRef, merge: true)
                    transaction.setData(["bookingId": originalBooking.firebaseDocumentId ?? ""], forDocument: parentParkingSpotDocumentRef, merge: true)
                    transaction.setData(["bookingId": parentBooking.firebaseDocumentId ?? ""], forDocument: originalParkingSpotRef, merge: true)
                    transaction.setData(["returningDate": originalParkingSpot.returningDate ?? ""], forDocument: parentParkingSpotDocumentRef, merge: true)
                    transaction.setData(["returningDate": parentParkingSpot.returningDate ?? ""], forDocument: originalParkingSpotRef, merge: true)
                    transaction.setData(["isSwapable": false], forDocument: originalBookingRef, merge: true)
                }catch let error as NSError{
                    errorPointer?.pointee = error
                    return nil
                }
                return nil
            }) { (_, error) in
                guard error == nil else {
                    let spError = SpeedParkError(message: error?.localizedDescription ?? "Error Swaping Parking Spots", type: .errorGettingBookingInfo)
                    completion(.failure(spError))
                    return
                }
                completion(.success(true))
            }
        }
    }
    
    
}


extension DataService{
    
    func getAllLocations(forCompany companyName: String = "speedpark_llc", completion: @escaping (Result<[Location], SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.locations).whereField(SpeedParkConstants.Fields.Location.company, isEqualTo: companyName).getDocuments { (snapshot, error) in
            guard error == nil, let documents = snapshot?.documents else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingLocations)
                completion(.failure(spError))
                return
            }
            let locations = documents.compactMap({Location.init(documentId: $0.documentID, data: $0.data())})
            guard  !locations.isEmpty else{
                let spError = SpeedParkError(message: "Error getting data with Message : Locations not available for company \(companyName)", type: .errorGettingLocations)
                completion(.failure(spError))
                return
            }
            completion(.success(locations))
        }
    }
    
    func getAllParkingSpots(forCompany companyName: String = "speedpark_llc", completion: @escaping ((Result<[Location.ParkingSpot], SpeedParkError>) -> Void)){
        db.collection(SpeedParkConstants.Collections.parkingSpot).whereField(SpeedParkConstants.Fields.Location.ParkingSpot.company, isEqualTo: companyName).getDocuments { [weak self] (snapshot, error) in
            guard error == nil, let documents = snapshot?.documents else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingParkSpots)
                completion(.failure(spError))
                return
            }
            let parkingSpots = documents.compactMap({Location.ParkingSpot.init(documentId: $0.documentID, data: $0.data())})
            guard  !parkingSpots.isEmpty else{
                let spError = SpeedParkError(message: "Error getting data with Message : Parking Spots not available for company \(companyName)", type: .errorGettingParkSpots)
                completion(.failure(spError))
                return
            }
            self?.setParentAndChild(parkingSpots: parkingSpots)
            completion(.success(parkingSpots))
        }
    }
    
    
    private func setParentAndChild(parkingSpots: [Location.ParkingSpot]){
        for parkingSpot in parkingSpots{
            if let parentDocumentId = parkingSpot.parent?.documentId{
                parkingSpot.parent = parkingSpots.first(where: {$0.documentId == parentDocumentId})
            }
            if let childDocumentId = parkingSpot.child?.documentId{
                parkingSpot.child = parkingSpots.first(where: {$0.documentId == childDocumentId})
            }
        }
    }
    
}
