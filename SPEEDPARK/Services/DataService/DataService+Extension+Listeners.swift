//
//  DataService+.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 5/25/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import Firebase

extension DataService{
    
    func getAllBooking(byStatus: SPBooking.Status,completion : @escaping (Result<[SPBooking], SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.bookings).whereField(SpeedParkConstants.Fields.Booking.bookingStatus, isEqualTo: byStatus.rawValue).getDocuments { snapshot, error in
            guard error == nil, let documents = snapshot?.documents else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingBookingInfo)
                completion(.failure(spError))
                return
            }
            let bookings = documents.compactMap({SPBooking(firebaseDocumentId: $0.documentID, data: $0.data())})
            completion(.success(bookings))
        }
    }
    
    func listenToAlarm(completion : @escaping (Result<(customerAlarm: Bool, keysAlarm: Bool), SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.global).addSnapshotListener { (snapshot, error) in
            guard error == nil, let documents = snapshot?.documents, let globalDocument = documents.first  else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingBookingInfo)
                completion(.failure(spError))
                return
            }
            let triggerCustomerRequestedAlarm = globalDocument.data()["triggerCustomerRequestedAlarm"] as? Bool ?? false
            let triggerUncheckedKeysAlarm = globalDocument.data()["triggerUncheckedKeysAlarm"] as? Bool ?? false
            let alarmResult = (customerAlarm : triggerCustomerRequestedAlarm, keysAlarm: triggerUncheckedKeysAlarm)
            completion(.success(alarmResult))
        }
    }
    
    func listenToAllBooking(byStatus: SPBooking.Status,completion : @escaping (Result<[SPBooking], SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.bookings).whereField(SpeedParkConstants.Fields.Booking.bookingStatus, isEqualTo: byStatus.rawValue).addSnapshotListener { (snapshot, error) in
            guard error == nil, let documents = snapshot?.documents else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingBookingInfo)
                completion(.failure(spError))
                return
            }
            let bookings = documents.compactMap({SPBooking(firebaseDocumentId: $0.documentID, data: $0.data())})
            completion(.success(bookings))
        }
    }
    
    func getAllOpenTickets(completion : @escaping (Result<[SPBooking], SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.bookings)
            .whereField(SpeedParkConstants.Fields.Booking.checkedInOrParkedStatus, isEqualTo: true)
            .order(by: SpeedParkConstants.Fields.Booking.bookingStatus, descending: false)
            .getDocuments { snapshot, error in
            guard error == nil, let documents = snapshot?.documents else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingBookingInfo)
                completion(.failure(spError))
                return
            }
            let bookings = documents.compactMap({SPBooking(firebaseDocumentId: $0.documentID, data: $0.data())})
            completion(.success(bookings))
        }
    }
    
    func listenToAllOpenTickets(completion : @escaping (Result<[SPBooking], SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.bookings)
            .whereField(SpeedParkConstants.Fields.Booking.checkedInOrParkedStatus, isEqualTo: true)
            .order(by: SpeedParkConstants.Fields.Booking.bookingStatus, descending: false)
            .addSnapshotListener { (snapshot, error) in
            guard error == nil, let documents = snapshot?.documents else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingBookingInfo)
                completion(.failure(spError))
                return
            }
            let bookings = documents.compactMap({SPBooking(firebaseDocumentId: $0.documentID, data: $0.data())}).filter({$0.status == .parked || $0.status == .checkedIn})
            completion(.success(bookings))
        }
    }
    
    func getBooking(byBookingNumber: Int, completion : @escaping (Result<SPBooking, SpeedParkError>) -> Void){
        db.collection(SpeedParkConstants.Collections.bookings)
            .whereField(SpeedParkConstants.Fields.Booking.bookingNo, isEqualTo: "\(byBookingNumber)")
            .whereField(SpeedParkConstants.Fields.Booking.bookingStatus, isEqualTo: SPBooking.Status.booked.rawValue)
            .limit(to: 1)
            .getDocuments { snapshot, error in
            guard error == nil, let document = snapshot?.documents.first else {
                let spError = SpeedParkError(message: "Error getting data with Message : \(error?.localizedDescription ?? "Not Found")", type: .errorGettingBookingInfo)
                completion(.failure(spError))
                return
            }
            completion(.success(SPBooking(firebaseDocumentId: document.documentID, data: document.data())))
        }
    }
}
