//
//  ParkingMaintenance.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/8/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import Firebase

class ParkingMaintenance{
    
    private var db = Firestore.firestore()
    private var parents : [Location.ParkingSpot] = []
    
    func resetBookings(){
        db.collection("bookings").whereField("checkedInOrParked", isEqualTo: true).getDocuments {(snapshot, error) in
            guard let documents = snapshot?.documents else {return}
            for document in documents{
                let dictionary : [String:Any] = ["checkedInOrParked":true, "bookingStatus": "Check-In", "parkedBy" : "", "parkingSpot": "", "siteLocation" : "", "parkedByEmail": "", "parkingSpotId" : "", "location":"", "internalNotes":"", "isSwapable":false]
                self.db.collection("bookings").document(document.documentID).setData(dictionary, merge: true)
            }
        }
    }
    
    func updateNotifications(){
        
        db.collection("notifications").whereField("notified", isEqualTo: false).whereField("type", isEqualTo: "EXIT_DATE_TYPE").getDocuments { (snapshot, error) in
            snapshot?.documents.forEach({ (doc) in
                if let bookingId = doc.data()["firebase_id"] as? String {
                    self.db.collection("bookings").document(bookingId).getDocument(completion: { (snapshot, error) in
                        if let ticketNo = snapshot?.data()?["ticketNo"] as? String{
                            self.db.collection("notifications").document(doc.documentID).setData(["ticket":ticketNo], merge: true)
                        }
                    })
                }
            })
        }
    }
    
    func generateCustomerDocument(){
        db.collection("bookings").getDocuments {(snapshot, error) in
            var customers = [String]()
            snapshot?.documents.forEach({ doc in
                let currentBooking = SPBooking(firebaseDocumentId: doc.documentID, data: doc.data())
                let customerName = currentBooking.customer.name.trimmingCharacters(in: .whitespaces)
                guard !customerName.isEmpty, customerName != "" else{
                    print("Invalid Booking Name")
                    return
                }
                if !customers.contains(customerName.capitalized){
                    customers.append(customerName.capitalized)
                }
            })
            self.db.collection("global").document("global_document").setData(["customers" : customers.sorted()], merge: true)
        }
    }

    func updateTaxRates(){
        db.collection("bookings").getDocuments {(snapshot, error) in
            snapshot?.documents.forEach({ doc in
                guard let taxRateString = doc.data()["taxRate"] as? String, let taxRate = Double(taxRateString) else {
                    return
                }
                let dictionary : [String:Double] = ["taxRate":taxRate]
                self.db.collection("bookings").document(doc.documentID).setData(dictionary, merge:true)
            })
        }
    }
    
    func updateAllDailyRates(){
        db.collection("bookings").getDocuments {(snapshot, error) in
            snapshot?.documents.forEach({ doc in
                let currentBooking = SPBooking(firebaseDocumentId: doc.documentID, data: doc.data())
                guard currentBooking.payment.serviceType != .noSelection else{
                    print("Invalid Service Type for booking : \(currentBooking)")
                    return
                }
                if let firebaseId = currentBooking.firebaseDocumentId,  currentBooking.payment.dailyRate <= 0{
                    let dictionary : [String:Double] = ["dailyRate":currentBooking.payment.serviceType.getRate()]
                    self.db.collection("bookings").document(firebaseId).setData(dictionary, merge:true)
                }
            })
        }
    }
    
    func getReservation(){
        db.collection("bookings").whereField("carMakes", isEqualTo: "Hyundai").getDocuments { (document, error) in
            document?.documents.forEach({print("\($0.documentID) - \($0.data())")})
        }
    }
    
    func resetAllTicketNumbers(){
        db.collection("bookings").getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else {return}
            for document in documents{
                self.db.collection("bookings").document(document.documentID).setData(["ticketNo":""], merge: true)
            }
        }
    }
    
    func resetAllParkingSpots(){
        db.collection("parkingSpots").getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents else {return}
            for document in documents{
                self.db.collection("parkingSpots").document(document.documentID).setData(["isAvailable":true,"returningDate" : ""], merge: true)
            }
        }
    }
    
    
    func createParkingSpots(){
        guard let parkingSpotsJSON = Bundle.main.url(forResource: "parking_spot", withExtension: "json") else {
            return
        }
        var locations : [String:String?] = ["On Site": nil, "Off Site": nil]
        for locationName in locations.keys{
            let locationDictionary : [String:Any] = ["company" : "speedpark_llc", "displayName":"\(locationName)", "image":"","name":"\(locationName.lowercased())", "order": (locationName == "On Site" ? 1 : 2), "isDefault": (locationName == "On Site" ? true: false)]
            let locationRef = db.collection("locations").document()
            let locationDocumentId = locationRef.documentID
            locations[locationName] = locationDocumentId
            locationRef.setData(locationDictionary, merge: true)
        }
        do{
            let data = try Data(contentsOf: parkingSpotsJSON)
            guard let dictionary = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [Any] else{
                print("Error parsing json file")
                return
            }
            for parking in dictionary {
                guard let parking = parking as? [String:Any], let spot = parking["option"] as? String, let locationName = parking["location"] as? String, let locationDocumentId = locations[locationName] as? String else {continue}
                let parkingSpotRef  = db.collection("parkingSpots").document()
                let parkingSpotDocumentId = parkingSpotRef.documentID
             
                let parkingSpot = Location.ParkingSpot(documentId: parkingSpotDocumentId, name: spot, location: locationDocumentId)
                if let isReusable = parking["isReusable"] as? Bool,isReusable{
                    parkingSpot.isReusable = true
                }
                if spot.contains("A"){
                    let parentName = spot.split(separator: "A")[0]
                    guard let parentParkingSpot = parents.first(where: {$0.name == parentName}) else {continue}
                    parkingSpot.parent = parentParkingSpot
                    parkingSpotRef.setData(parkingSpot.parse(), merge: true)
                    db.collection("parkingSpots").document(parentParkingSpot.documentId).setData(["child":parkingSpotDocumentId], merge: true)
                }else{
                    parents.append(parkingSpot)
                    print("saving parking spot normal")
                    parkingSpotRef.setData(parkingSpot.parse(), merge: true)
                }
            }
        }catch(let error){
            print(error)
        }
//        parents = []
    }
    
    func getBookingBy(bookingNo: String){
        db.collection("bookings").whereField("ticketNo", isEqualTo: bookingNo).getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents,!documents.isEmpty,error == nil else{
                print(error?.localizedDescription ?? "0 Documents found")
                return
            }
            for document in documents{
                print(document.data())
            }
        }
        
    }
    
    
    func getAllParking(byLocation: String){
        db.collection("parkingSpots").whereField("location", isEqualTo: byLocation).getDocuments { (snapshot, error) in
            guard let documents = snapshot?.documents, !documents.isEmpty , error == nil else{
                print(error?.localizedDescription ?? "0 Documents found")
                return
            }
            let parkingSpots = documents.compactMap({Location.ParkingSpot.init(documentId: $0.documentID, data: $0.data())})
            print(parkingSpots.sorted(by: {$0.name < $1.name}))
        }
        
    }
    
}
