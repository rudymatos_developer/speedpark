//
//  CarApiService.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 6/20/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

protocol HasCarAPIService:class{
    var carAPIService: CarAPIService? {get set}
}

class CarAPIService{
    
    enum CarAPIType{
        case maker
        case models
    }
    
    func getAllCarMakers(completion: @escaping ((Result<[GenericSelectionData], SpeedParkError>) -> Void)){
        guard let request = generateGetAllMakersRequest() else{
            let error = SpeedParkError(message: "Invalid URL to get All Car Makers", type: .errorGettingCarMakers)
            completion(.failure(error))
            return
        }
        sendRequest(carAPIType: .maker, request: request, completion: completion)
    }
    
    func getAllModels(byMaker: String, completion: @escaping ((Result<[GenericSelectionData], SpeedParkError>) -> Void)){
        guard let request = generateGetAllModelsRequest(byMaker: byMaker) else{
            let error = SpeedParkError(message: "Invalid URL to get All Car Makers", type: .errorGettingCarMakers)
            completion(.failure(error))
            return
        }
        sendRequest(carAPIType: .models, request: request, completion: completion)
    }
    
}


extension CarAPIService{
    
    private func generateGetAllMakersRequest() -> URLRequest?{
        let urlString = "https://www.carqueryapi.com/api/0.3/?callback=?&cmd=getMakes&sold_in_us=1"
        guard let url = URL(string: urlString) else{
            return nil
        }
        var request =  URLRequest(url: url , cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        return request
    }
    
    private func generateGetAllModelsRequest(byMaker: String) -> URLRequest?{
        guard let urlString = "https://www.carqueryapi.com/api/0.3/?callback=?&cmd=getModels&make=\(byMaker)&sold_in_us=1"
            .addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
            let url = URL(string: urlString) else{
                return nil
        }
        return URLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10)
    }
    
    private func sendRequest(carAPIType: CarAPIType, request: URLRequest, completion: @escaping ((Result<[GenericSelectionData], SpeedParkError>) -> Void)){
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data, (response as? HTTPURLResponse)?.statusCode == 200, error == nil else{
                let error = SpeedParkError(message: "Error getting Car Results with message: \(String(describing: error?.localizedDescription))", type: carAPIType == .maker ? .errorGettingCarMakers : .errorGettingCarModels)
                print(error)
                completion(.failure(error))
                return
            }
            do{
                guard let jsonString = String(data: data, encoding: .utf8)?
                    .replacingOccurrences(of: "?", with: "")
                    .replacingOccurrences(of: "(", with: "")
                    .replacingOccurrences(of: ")", with: "")
                    .replacingOccurrences(of: ";", with: ""),
                    let jsonData = jsonString.data(using: .utf8) else{
                        let error = SpeedParkError(message: "Error getting Car Results with message: Error parsing JSON Data from Server.", type: carAPIType == .maker ? .errorGettingCarMakers : .errorGettingCarModels)
                        completion(.failure(error))
                        return
                }
                switch carAPIType{
                case .maker:
                    let results = try JSONDecoder().decode(CarMake.self, from: jsonData)
                    let carResults = results.makes.compactMap({GenericSelectionData.init(displayName: $0.makeDisplay, info: "", additionalInfo: "", selected: false, type: .carMake)})
                    completion(.success(carResults))
                case .models:
                    let results = try JSONDecoder().decode(CarModel.self, from: jsonData)
                    let carResults = results.models.compactMap({GenericSelectionData.init(displayName: $0.modelName, info: "", additionalInfo: "", selected: false, type: .carModel(""))})
                    completion(.success(carResults))
                }
            }catch let error{
                let spError = SpeedParkError(message: "Error getting Car Results with message : \(String(describing: error.localizedDescription))", type: carAPIType == .maker ? .errorGettingCarMakers : .errorGettingCarModels)
                print(error)
                completion(.failure(spError))
                return
            }
            }.resume()
    }
    
}


