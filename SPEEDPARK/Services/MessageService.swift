//
//  MessageService.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/17/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

protocol HasMessageService{
    var messageService: MessageService {get set}
}

class MessageService{
    
    private func getRequest(url: URL, requestParams: [String:Any?]) -> URLRequest?{
        do{
            var request = URLRequest(url: url, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: 60)
            let headers = ["Content-Type": "application/json", "Accept": "application/json"]
            request.allHTTPHeaderFields = headers
            request.httpMethod = "POST"
            request.httpBody = try JSONSerialization.data(withJSONObject: requestParams, options: .prettyPrinted)
            return request
        }catch(let error){
            print(error)
            return nil
        }
    }
    
    private func sendRequest(request: URLRequest, completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        let queue = DispatchQueue(label: "sendSMS", qos: .background)
        queue.async {
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let _ = data, (response as? HTTPURLResponse)?.statusCode == 200, error == nil else{
                    let error = SpeedParkError(message: "Error sending SMS with message : \(String(describing: error?.localizedDescription))", type: .errorSendingSMS)
                    print(error)
                    completion(.failure(error))
                    return
                }
                print("done sending message")
                completion(.success(true))
                }.resume()
        }
    }
    
    func sendTicketAssignedSMSNotification(booking: SPBooking, completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        print("entering to \(#function)")
        let ticketNumber = booking.ticketNumber
        let customerPhoneNumber = booking.customer.phone
        
        guard  !ticketNumber.isEmpty && !customerPhoneNumber.isEmpty, let url = URL(string: SpeedParkConstants.MessagingURLs.sendSMSURL) else {
            let error = SpeedParkError(message: "Error sending SMS with message : Invalid Data", type: .errorSendingSMS)
            completion(.failure(error))
            return
        }
        
        let requestParams = ["ticket_number":ticketNumber, "client_phone_number" : customerPhoneNumber]
        guard let request = getRequest(url: url, requestParams: requestParams) else{
            let error = SpeedParkError(message: "Error sending SMS with message : Invalid URL Request", type: .errorSendingSMS)
            completion(.failure(error))
            return
        }
        sendRequest(request: request, completion: completion)
    }
    
    func sendApproximateWaitSMSNotification(booking: SPBooking, minutes: String, completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        var customerPhoneNumber = booking.customer.phone
        guard  !customerPhoneNumber.isEmpty, let url = URL(string: SpeedParkConstants.MessagingURLs.approximateTime) else {
            let error = SpeedParkError(message: "Error sending SMS with message : Invalid Customer Phone Number", type: .errorSendingSMS)
            completion(.failure(error))
            return
        }
        
        if let incomingPhoneNumber = booking.customer.incomingPhoneNumber, !incomingPhoneNumber.isEmpty{
            customerPhoneNumber = incomingPhoneNumber
        }
        
        let requestParams = ["approximate_time":minutes, "client_phone_number" : customerPhoneNumber]
        guard let request = getRequest(url: url, requestParams: requestParams) else{
            let error = SpeedParkError(message: "Error sending SMS with message : Invalid URL Request", type: .errorSendingSMS)
            completion(.failure(error))
            return
        }
        sendRequest(request: request, completion: completion)
    }
    
    func sendBookingRejectionSMS(booking: SPBooking, completion: @escaping ((Result<Bool, SpeedParkError>) -> Void)){
        let ticketNumber = booking.ticketNumber
        let customerPhoneNumber = booking.customer.phone
        
        guard  !ticketNumber.isEmpty && !customerPhoneNumber.isEmpty, let url = URL(string: SpeedParkConstants.MessagingURLs.rejectBookingSMSURL) else {
            let error = SpeedParkError(message: "Error sending SMS with message : Invalid Data", type: .errorSendingSMS)
            completion(.failure(error))
            return
        }
        
        let requestParams = ["ticket_number":ticketNumber, "client_phone_number": booking.customer.phone]
        guard let request = getRequest(url: url, requestParams: requestParams) else{
            let error = SpeedParkError(message: "Error sending SMS with message : Invalid URL Request", type: .errorSendingSMS)
            completion(.failure(error))
            return
        }
        sendRequest(request: request, completion: completion)
    }
    
}
