//
//  UserDefaultService.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

protocol HasUserDefaultService{
    var userDefaultService : UserDefaultService {get set }
}

class UserDefaultService{
    private let currentLoggedInUser = "currentLoggedInUser"
    private let currentDeviceFCM = "currentDeviceFCM"
}

extension UserDefaultService{
    
    func setCurrentLoggedInUser(email: String){
        UserDefaults.standard.set(email, forKey: currentLoggedInUser)
    }
    
    func getCurrentLoggedInUser() -> String?{
        return UserDefaults.standard.string(forKey: currentLoggedInUser)
    }
    
    func removeCurrentLoggedInUser(){
        UserDefaults.standard.removeObject(forKey: currentLoggedInUser)
    }
    
}

extension UserDefaultService {
    func getFCMToken() -> String? {
        return UserDefaults.standard.object(forKey: currentDeviceFCM) as? String
    }
    func set(_ fcmToken: String) {
        UserDefaults.standard.set(fcmToken, forKey: currentDeviceFCM)
    }
    func removeFCMToken() {
        UserDefaults.standard.removeObject(forKey: currentDeviceFCM)
    }
}
