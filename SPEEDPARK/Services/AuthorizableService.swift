//
//  AuthorizableService.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import Firebase

protocol HasUser{
    var user : SPUser? {get set}
}

protocol HasDependencies{
    associatedtype Dependencies
    var dependencies: Dependencies? {get set}
}

protocol HasAuthorizableService{
    var authorizableService: AuthorizableService? {get set}
}

class AuthorizableService: HasDependencies{

    typealias Dependencies = HasDataService & HasUser & HasUserDefaultService
    var dependencies: Dependencies?
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
    }
    
    private var firebaseAuth = Auth.auth()
    
    func login(email: String, password: String, completion:@escaping (Result<Bool, SpeedParkError>) -> Void){
        firebaseAuth.signIn(withEmail: email, password: password) { [weak self] (result, error) in
            guard let self = self, error == nil else {
                completion(.failure(SpeedParkError(message: "Login error with Message: \(error?.localizedDescription ?? "User not found")", type: .loginError)))
                return
            }
            self.dependencies?.dataService.addToLoggedInUsers(email: email)
            self.dependencies?.userDefaultService.setCurrentLoggedInUser(email: email)
            self.dependencies?.dataService.getUserInfo(email: email, completion: { (result) in
                switch result{
                case .success(let user):
                    self.dependencies?.user = user
                    if let fcmToken = self.dependencies?.userDefaultService.getFCMToken(){
                        self.dependencies?.dataService.addNotificationToken(fcmToken)
                    }
                    completion(.success(true))
                case .failure(let error):
                    completion(.failure(error))
                }
            })
        }
    }
    
    func isUserLoggedIn() -> Bool{
        return firebaseAuth.currentUser != nil
    }

    func logout(completion:@escaping (Result<Bool, SpeedParkError>) -> Void){
        do{
            guard let email = dependencies?.user?.email, isUserLoggedIn() else {
                completion(.failure(SpeedParkError(message:"Error login out user. Please Contact Administrator.", type: .logoutError)))
                return
            }
            dependencies?.dataService.removeFromLoggedIn(email: email)
            if let fcmToken = dependencies?.userDefaultService.getFCMToken(){
                dependencies?.dataService.removeNotificationToken(fcmToken)
            }
            self.dependencies?.userDefaultService.removeCurrentLoggedInUser()
            try firebaseAuth.signOut()
            completion(.success(true))
        }catch(let error){
            completion(.failure(SpeedParkError(message:error.localizedDescription, type: .logoutError)))
        }
    }
}
