//
//  PayVC.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/21/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit
import Lottie
import IQKeyboardManagerSwift

class PayVC: UIViewController, StoryboardInit, CardCreator {
    
    @IBOutlet weak var completePaymentView: UIView!
    @IBOutlet weak var completePaymentBTN: UIButton!
    @IBOutlet weak var lottieView: UIView!
    @IBOutlet weak var serviceTypeTF: UITextField!
    @IBOutlet weak var periodTimeStartLBL: UILabel!
    @IBOutlet weak var periodTimeStopLBL: UILabel!
    @IBOutlet weak var totalStayLBL: UILabel!
    @IBOutlet weak var tipsPercentageBTN: UIButton!
    @IBOutlet weak var totalPriceLBL: UILabel!
    @IBOutlet weak var ticketNumberlLBL: UILabel!
    @IBOutlet weak var taxesLBL: UILabel!
    @IBOutlet weak var chargeAmountLBL: UILabel!
    @IBOutlet weak var subTotalLBL: UILabel!
    
    private var isDismissingView = false
    
    var viewModel: PayVM!
    private let selectServiceTypeView  = SelectServiceTypeView(frame: CGRect(x: 0, y: 0, width: 0, height: 250))
    private var observation = [NSKeyValueObservation]()
    private var selectTipPercentage:UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        observePoso()
        configureView()
    }
    
    @IBAction func completePayment(_ sender: UIButton) {
        
        let paymentMethod = UIAlertController(title: "Payment Method", message: "Select your Payment Method", preferredStyle: .actionSheet)
        
        let cash = UIAlertAction(title: "Cash", style: .default) { [weak self] _ in
            self?.viewModel.cashPayment()
        }
        
        let creditCard = UIAlertAction(title: "Credit Card", style: .default) { [weak self] _ in
            self?.viewModel.creditCardPayment()
            self?.navigationController?.popToRootViewController(animated: true)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        paymentMethod.addAction(cash)
        paymentMethod.addAction(creditCard)
        paymentMethod.addAction(cancel)
        present(paymentMethod, animated: true, completion: nil)
    }
    
    private func configureView(){
        title = "Payment"
        navigationItem.largeTitleDisplayMode = .never
        periodTimeStartLBL.text = viewModel.getStarDisplayDate()
        periodTimeStopLBL.text = viewModel.getEndDisplayDate()
        serviceTypeTF.text = viewModel.getServiceType()
        totalStayLBL.text = viewModel.getTotalStay()
        totalPriceLBL.text = viewModel.getTotalPrice()
        chargeAmountLBL.text = viewModel.getChargeAmount()
        subTotalLBL.text = viewModel.getSubTotal()
        taxesLBL.text = viewModel.getTaxRate()
        ticketNumberlLBL.text = viewModel.booking.ticketNumber
        shouldEnablePayConfirmationButton(isPojoValid: false)

        lottieView.initLottieView(withName: "credit_card")
        serviceTypeTF.inputView = selectServiceTypeView
        addKeyboardDismissGesture()
        
        selectServiceTypeView.delegate = self
        IQKeyboardManager.shared.enableAutoToolbar = false
    }
    
    private func addKeyboardDismissGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PayVC.dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @IBAction func selectTips(_ sender: UIButton) {
        
        guard viewModel.getServiceType() != SPServiceType.noSelection.getTitle() else{
            displaySimpleAlertMessage(title: "Additional Info Required", message: "Please Select a Service Type before adding any tips")
            return
        }
        
        guard viewModel.calculateChargeAmount() > 0 else{
            displaySimpleAlertMessage(title: "Calculation Error", message: "Seems like there's a problem with the calculation. Contact Administrator")
            return
        }
        
        viewModel.set(tip: .none)
        let tipsVC = TipsVC.getInstance(storyBoardName: "Tips")
        tipsVC.modalPresentationStyle = .overFullScreen
        tipsVC.delegate = self
        tipsVC.currentTotal = viewModel.calculateChargeAmount()
        present(tipsVC, animated: true, completion: nil)
    }
    
    @objc private func dismissKeyboard(){
        isDismissingView = true
        serviceTypeTF.resignFirstResponder()
    }
    
    private func shouldEnablePayConfirmationButton(isPojoValid: Bool){
        completePaymentBTN.isEnabled = isPojoValid
        completePaymentView.backgroundColor = isPojoValid ? UIColor(named: SpeedParkConstants.Color.main) : UIColor(named: SpeedParkConstants.Color.disabled)
    }
    
    private func observePoso(){
        self.observation = [viewModel.poso.observe(\PaymentPOSO.serviceType, options: .new) { [weak self] poso, change in
            self?.shouldEnablePayConfirmationButton(isPojoValid: poso.isValid())
            },
                            viewModel.poso.observe(\PaymentPOSO.tipPercentage, options: .new) { [weak self] poso, change in
                                self?.shouldEnablePayConfirmationButton(isPojoValid: poso.isValid())
            }]
    }
    
    
    @IBAction func dismissView(_ sender: UIButton) {
        setEditing(false, animated: true)
        viewModel.cancelPayment()
        dismiss(animated: true, completion: nil)
    }
    
}

extension PayVC: TipsSelectionDelegate{
    func select(tip: SPTip) {
        viewModel.set(tip: tip)
        tipsPercentageBTN.setTitle(viewModel.getTipsFromAmout(), for: .normal)
        tipsPercentageBTN.setTitleColor(.black, for: .normal)
        totalPriceLBL.text = viewModel.getTotalPrice()
    }
}


extension PayVC: SelectServiceTypeViewDelegate{
    
    func select(serviceType: SPServiceType){
        viewModel.set(serviceType: serviceType)
        serviceTypeTF.text = viewModel.getServiceType()
        totalPriceLBL.text = viewModel.getTotalPrice()
        chargeAmountLBL.text = viewModel.getChargeAmount()
        subTotalLBL.text = viewModel.getSubTotal()
        taxesLBL.text = viewModel.getTaxRate()
        cancelServiceTypeSelection()
    }
    
    func cancelServiceTypeSelection(){
        serviceTypeTF.resignFirstResponder()
    }
}


extension PayVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.accessibilityIdentifier == "serviceTypeTF"{
            selectServiceTypeView.selectedServiceType = viewModel.booking.payment.serviceType
            selectServiceTypeView.configureUI()
        }
    }
}


