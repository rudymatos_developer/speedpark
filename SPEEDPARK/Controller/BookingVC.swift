//
//  MainVC.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit
import Crashlytics

class BookingVC: UIViewController, StoryboardInit {
    
    @IBOutlet weak var totalResultsLBL: UILabel!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var tableView: UITableView!
    private let searchController = UISearchController(searchResultsController: nil)
    var viewModel: BookingVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        configureView()
        viewModel.loadTicket { [weak self] in
            guard let self = self else {return}
            print("refreshing \(self.viewModel.getViewName())")
            let counter = self.viewModel.getBookingCount()
            DispatchQueue.main.async {
                self.tabBarController?.tabBar.items?.filter({$0 == self.tabBarItem}).first?.badgeValue = counter > 0 ? "\(counter)" : nil
                self.searchController.isActive = false
                self.totalResultsLBL.text = self.viewModel.getTotalResults()
                self.tableView.reloadData()
                self.searchController.searchBar.text = ""
            }
        }
    }

    private func configureView(){
        titleLBL.text = viewModel.getViewName()
        configureViewForReservations()
        addSearchController()
    }
    
    
    private func configureViewForReservations(){
        if viewModel.getBookingType() == .reservations{
            navigationItem.rightBarButtonItems = navigationItem.rightBarButtonItems?.filter({$0.tag == 1})
        }else if viewModel.getBookingType() == .new{
            navigationItem.leftBarButtonItems = nil
            navigationItem.rightBarButtonItems = navigationItem.rightBarButtonItems?.filter({$0.tag == 2})
        }else{
            navigationItem.rightBarButtonItems = navigationItem.rightBarButtonItems?.filter({$0.tag == 2})
            navigationItem.leftBarButtonItems = navigationItem.leftBarButtonItems?.filter({$0.tag == 1})
        }
    }
    
    @IBAction func createNewReservation(_ sender: UIBarButtonItem) {
            viewModel.createNewReservation()
    }
    
    @IBAction func sortResults(_ sender: UIBarButtonItem) {
        let sortAlert = UIAlertController(title: "Sort Results", message: "Select an option from below to sort the results", preferredStyle: .actionSheet)
        for sorter in BookingVCConfigurator.BookingSorter.allCases{
            if sorter.shouldAppearOn(bookingType: viewModel.getBookingType()){
                let action = UIAlertAction(title: sorter.getTitle(), style: .default) { [weak self] _ in
                    self?.sortAndRefresh(with: sorter)
                }
                sortAlert.addAction(action)
            }
        }
        sortAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(sortAlert, animated: true, completion: nil)
    }
    
    @IBAction func filterResults(_ sender: UIBarButtonItem) {
        let filterAlert = UIAlertController(title: "Filter Results", message: "Select an option from below to filter the results", preferredStyle: .actionSheet)
        for filter in BookingVCConfigurator.BookingFilter.allCases{
            if filter.shouldAppearOn(bookingType: viewModel.getBookingType()){
                let action = UIAlertAction(title: filter.getTitle(), style: .default) { [weak self] _ in
                    self?.filterAndRefresh(with: filter)
                }
                filterAlert.addAction(action)
            }
        }
        filterAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(filterAlert, animated: true, completion: nil)
    }
    
    private func sortAndRefresh(with: BookingVCConfigurator.BookingSorter){
        viewModel.sortResultsBy(sorter: with)
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    private func filterAndRefresh(with: BookingVCConfigurator.BookingFilter){
        viewModel.filterResultsBy(filter: with)
        totalResultsLBL.text = viewModel.getTotalResults()
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    private func registerCell(){
        tableView.register(UINib(nibName: TicketCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: TicketCell.identifier)
        tableView.register(UINib(nibName: ActiveCell.identifier, bundle: Bundle.main), forCellReuseIdentifier: ActiveCell.identifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.tintColor = UIColor(named: "palette_main_blue_color")
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        tabBarController?.tabBar.tintColor = viewModel.getBarTintColor()
    }
    
    private func addSearchController(){
        self.definesPresentationContext = true
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false
        searchController.dimsBackgroundDuringPresentation = false
        searchController.hidesNavigationBarDuringPresentation = false
        searchController.searchBar.tintColor = UIColor(named: SpeedParkConstants.Color.main)
        searchController.searchBar.delegate = self
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
}

extension BookingVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.getHeightBy(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let booking = viewModel.getBooking(byIndex: indexPath.row)
        print(booking.description)
        viewModel.goToBookingDetails(booking: booking)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let booking = viewModel.getBooking(byIndex: indexPath.row)
        if booking.status == .active{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ActiveCell.identifier, for: indexPath) as? ActiveCell else{
                return UITableViewCell()
            }
            configure(cell: cell, atIndexPath: indexPath, withBooking: booking)
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: TicketCell.identifier, for: indexPath) as? TicketCell else{
                return UITableViewCell()
            }
            configure(cell: cell, atIndexPath: indexPath, withBooking: booking)
            return cell
        }
    }
}

extension BookingVC{
    
    private func configure(cell: ActiveCell, atIndexPath indexPath: IndexPath, withBooking booking: SPBooking){
        
        cell.showMoreOptions = { [weak self] booking in
            let moreOptions = UIAlertController(title: "More Options", message: "Ticket Number :\(booking.ticketNumber)", preferredStyle: .actionSheet)
            if (!booking.payment.paid && !booking.payment.prePaid){
                let payAction = UIAlertAction(title: "Pay", style: .default) { [weak self] _ in
                    guard let self = self else {return}
                    self.viewModel.goToPayTicket(booking: booking)
                }
                moreOptions.addAction(payAction)
            }
            
            if (booking.isThereAdditionalCharges){
                let payAdditionalCharges = UIAlertAction(title: "Pay Additional Charges", style: .default) { [weak self] _ in
                    guard let self = self else {return}
                    self.viewModel.goToPayTicket(booking: booking)
                }
                moreOptions.addAction(payAdditionalCharges)
            }
            
            let doneAction = UIAlertAction(title: "Done", style: .default) { [weak self] _ in
                guard let self = self else {return}
                if ((!booking.payment.paid && !booking.payment.prePaid) || booking.isThereAdditionalCharges){
                    let notPaidOrPendingBalanceAlert = UIAlertController(title: "Complete Ticket", message: self.viewModel.getDoneWithNoPaidOrPendingBalance(booking: booking), preferredStyle: .alert)
                    
                    let payAction = UIAlertAction(title: "Pay", style: .default, handler: { _ in
                        self.viewModel.goToPayTicket(booking: booking)
                    })
                    let completeTicket = UIAlertAction(title: "Complete without Payment", style: .destructive, handler: { _ in
                        self.viewModel.completeTicket(booking: booking)
                    })
                    
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
                    notPaidOrPendingBalanceAlert.addAction(payAction)
                    notPaidOrPendingBalanceAlert.addAction(completeTicket)
                    notPaidOrPendingBalanceAlert.addAction(cancelAction)
                    self.present(notPaidOrPendingBalanceAlert, animated: true, completion: nil)
                }else{
                    self.displayConfirmationDialog(title: "Complete Ticket", message: self.viewModel.getDoneMessage(booking: booking) , okTitle: "Yes, I'm sure!", isOKDesctructive: true, okCompletion: { [weak self] _  in
                        self?.viewModel.completeTicket(booking: booking)
                    })
                }
            }
            
            moreOptions.addAction(doneAction)
            let reparkAction = UIAlertAction(title: "Re-Park", style: .destructive) { [weak self] _ in
                guard let self = self else {return}
                self.displayConfirmationDialog(title: "Return Car", message: self.viewModel.getParkMessage(booking: booking) , okTitle: "Yes, I'm sure!", isOKDesctructive: true, okCompletion: { [weak self] _  in
                    self?.viewModel.returnCarToPark(booking: booking)
                })
            }
            
            moreOptions.addAction(reparkAction)
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
            moreOptions.addAction(cancel)
            self?.present(moreOptions, animated: true, completion: nil)
        }
        
        cell.callActionCompletion = { [weak self] booking in
            guard let self = self else {return}
            
            guard self.viewModel.isPhoneNumberAvailable(booking: booking) else{
                self.displaySimpleAlertMessage(title: "Invalid Phone Number", message: "The number was not specified on the reservation")
                return
            }
            
            self.displayConfirmationDialog(title: "Call Customer", message: self.viewModel.callMessage(booking:booking), okTitle: "Yes, I'm sure!", isOKDesctructive: true, okCompletion: { [weak self] _  in
                self?.viewModel.callCustomer(booking: booking)
            })
        }
        
        cell.booking = booking
        cell.selectionStyle = .none
    }
    
    
    private func configure(cell: TicketCell, atIndexPath indexPath: IndexPath, withBooking booking: SPBooking){
        
        cell.processActionCompletion = { [weak self]  booking in
            guard let self = self else {return}
            switch booking.status {
            case .booked:
                self.showBookedDialog(booking)
            case .checkedIn:
                self.viewModel.goToParkVehicle(booking: booking)
            case .new:
                self.viewModel.goToAcceptBooking(booking: booking)
            case .parked:
                self.showParkedDialog(booking)
            default:
                print(booking.description)
            }
        }
        
        cell.checkKeysInCompletion = { [weak self] booking in
            guard let self = self else {return}
            self.displayConfirmationDialog(title: "Check Keys In", message: self.viewModel.getCheckKeysInMessage(booking: booking) , okTitle: "Yes, I'm sure!", isOKDesctructive: true, okCompletion: { [weak self] _  in
                self?.viewModel.checkKeysIn(booking: booking)
            })
        }
        
        cell.auxiliarActionCompletion = { [weak self] booking in
            guard let self = self else {return}
            self.displayConfirmationDialog(title: "Reject", message: self.viewModel.getRejectBookingMessage(booking:booking), okTitle: "Yes, I'm sure!", isOKDesctructive: true, okCompletion: { [weak self] _  in
                self?.viewModel.reject(booking: booking)
            })
        }
        
        cell.swapingActionCompletion = { [weak self] booking in
            self?.displaySwapingConfirmationDialog(withBooking: booking)
        }
        cell.booking = booking
        cell.selectionStyle = .none
    }
    
}

extension BookingVC{
    func displaySwapingConfirmationDialog(withBooking booking:SPBooking){
        displayConfirmationDialog(title: "Do you want to Swap Vehicles?", message: viewModel.getSwapingMessage(booking: booking)) { [weak self] _ in
            self?.viewModel.swapParkingSpots(booking: booking) { result in
                switch result{
                case .failure(let error):
                    self?.displaySimpleAlertMessage(title: "Error Swapping", message: error.localizedDescription)
                case .success:
                    print("do nothing")
                }
            }
        }
    }
}

extension BookingVC{
    private func showBookedDialog(_ booking: SPBooking) {
        let sheet = UIAlertController(title: "Booking #\(booking.identifier)", message: "Available Actions", preferredStyle: .actionSheet)
        let scanTicketAction  = UIAlertAction(title: "Scan", style: .default, handler: { _ in
            self.viewModel.goToAssignTicketNumber(booking: booking)
        })
        let noShowAction = UIAlertAction(title: "Change to No Show", style: .default, handler: { _ in
            self.displayConfirmationDialog(title: "Customer didn't show", message: self.viewModel.getNoShowMessage(booking: booking), okCompletion: { _ in
                self.viewModel.changeBookingToNoShow(booking: booking)
            })
        })
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        sheet.addAction(scanTicketAction)
        sheet.addAction(noShowAction)
        sheet.addAction(cancelAction)
        self.present(sheet, animated: true, completion: nil)
    }
    
    private func showParkedDialog(_ booking: SPBooking) {
        let sheet = UIAlertController(title: "Pull #\(booking.ticketNumber)", message: "Available Actions", preferredStyle: .actionSheet)
        if self.viewModel.shouldShowPrepaidAction(forBooking: booking) {
            let payAction = UIAlertAction(title: "Prepaid", style: .default, handler:  { _ in
                self.viewModel.goToPayTicket(booking: booking)
            })
            sheet.addAction(payAction)
        }
        let moveToActiveAction = UIAlertAction(title: "Move to Active", style: .default, handler: { _ in
            self.displayConfirmationDialog(title: "Move to Active", message: self.viewModel.getMoveToActionMessage(booking: booking), okCompletion: { _ in
                self.viewModel.showWaitingTime(booking: booking)
            })
        })
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        sheet.addAction(moveToActiveAction)
        sheet.addAction(cancelAction)
        self.present(sheet, animated: true, completion: nil)
    }
    
}

extension BookingVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filterBy(value: searchText)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        filterBy(value: "")
    }
    
    private func filterBy(value: String){
        viewModel.filterBy(value: value)
        DispatchQueue.main.async {
            self.totalResultsLBL.text = self.viewModel.getTotalResults()
            self.tableView.reloadData()
        }
    }
    
}
