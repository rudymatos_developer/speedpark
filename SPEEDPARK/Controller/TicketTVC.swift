//
//  TicketTVC.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class TicketTVC: UITableViewController, StoryboardInit , ViewLoadingType{

    var viewModel: TicketVM!
    
    @IBOutlet weak var customerNameTF: BindiableTF!
    @IBOutlet weak var customerPhoneTF: BindiableTF!
    @IBOutlet weak var customerEmailTF: BindiableTF!
    @IBOutlet weak var carMakerTF: BindiableTF!
    @IBOutlet weak var carModelTF: BindiableTF!
    @IBOutlet weak var carYearTF: BindiableTF!
    @IBOutlet weak var carColorTF: BindiableTF!
    @IBOutlet weak var carPlateTF: BindiableTF!
    @IBOutlet weak var parkingSpotLBL: UILabel!
    @IBOutlet weak var parkingLocationLBL: UILabel!
    @IBOutlet weak var parkingValetTF: BindiableTF!
    @IBOutlet weak var flightAirlineTF: BindiableTF!
    @IBOutlet weak var flightNumberTF: BindiableTF!
    @IBOutlet weak var flightFromTF: BindiableTF!
    @IBOutlet weak var notesTV: BindiableTV!
    @IBOutlet weak var exitDateTF: BindiableTF!
    
    private lazy var datePicker = UIDatePicker()
    
    var loadingScreen: LoadingScreen!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerBindings()
        initValues()
        configureView()
    }

    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    @IBAction func reParkVehicle(_ sender: UIButton) {
        viewModel.goToReparkVehicle()
    }
    
    @IBAction func saveTicket(_ sender: UIButton) {
        if viewModel.booking.isValid(){
            showLoadingScreen()
            viewModel.createTicketAndUpdateReservation() { [weak self] result in
                guard let self = self else {return}
                self.removeLoadingScreen()
                switch result{
                case .failure(let error):
                    self.displaySimpleAlertMessage(title: "Error Saving Data", message: error.message)
                case .success:
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
        }else{
            displaySimpleAlertMessage(title: "Cannot Save", message: "Make sure Reservation has Customer Name, Customer Phone Number, Customer Email, Vehicle Maker and Vehicle Model")
        }
    }
    
    @IBAction func search(_ sender: UIButton) {
        guard let id = sender.accessibilityLabel,
            let genericSelectionType = GenericSelectionVCConfigurator.GenericSelectionType.getType(by: id) else{
            return
        }
        showLoadingScreen()
        viewModel.search(by: genericSelectionType) { [weak self] result in
            guard let self = self else {return}
            self.removeLoadingScreen()
            switch result{
            case .failure(let error):
                self.displaySimpleAlertMessage(title: "Error Getting Data", message: error.message)
            default:
                print("do nothing")
            }
        }
    }
}

extension TicketTVC : GenericSelectionDelegate{
    func select(genericSelectionData: GenericSelectionData){
        switch genericSelectionData.type{
        case .customer:
            customerNameTF.set(text: genericSelectionData.displayName)
            customerPhoneTF.set(text: genericSelectionData.info)
            customerEmailTF.set(text: genericSelectionData.additionalInfo)
        case .carMake:
            carMakerTF.set(text: genericSelectionData.displayName)
        case .carModel:
            carModelTF.set(text: genericSelectionData.displayName)
        }
    }
}


extension TicketTVC{
    
    private func configureView(){
        configureExitDate()
        title = viewModel.getTitle()
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.tintColor = UIColor(named: SpeedParkConstants.Color.main)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TicketTVC.dismissKeyboard))
        self.view.addGestureRecognizer(tapGesture)
    }

    private func configureExitDate(){
        exitDateTF.isEnabled = !viewModel.isEditMode()
        datePicker.datePickerMode = .dateAndTime
        datePicker.addTarget(self, action: #selector(TicketTVC.datePickerValueChanged(sender:)), for: .valueChanged)
        exitDateTF.inputView = datePicker
    }
    
    @objc private func dismissKeyboard(){
        self.view.endEditing(true)
    }
    
    @objc private func datePickerValueChanged(sender: UIDatePicker){
        viewModel.booking.parking.exitDate = viewModel.convert(date: sender.date, usingPattern: .speedParkLongFormat)
        exitDateTF.text = viewModel.booking.parking.exitDate
    }
    
    func assignParkingInformation() {
        parkingLocationLBL.text = "\(viewModel.booking.parking.siteLocation)"
        parkingSpotLBL.text = "\(viewModel.booking.parking.spot)"
        parkingValetTF.text = viewModel.booking.parking.valet
    }
    
    private func initValues(){
        customerNameTF.text = viewModel.booking.customer.name
        customerPhoneTF.text = viewModel.booking.customer.phone
        customerEmailTF.text = viewModel.booking.customer.email
        carMakerTF.text = viewModel.booking.vehicle.maker
        carModelTF.text = viewModel.booking.vehicle.model
        carColorTF.text = viewModel.booking.vehicle.color
        carYearTF.text = viewModel.booking.vehicle.year
        carPlateTF.text = viewModel.booking.vehicle.plate
        assignParkingInformation()
        flightAirlineTF.text = viewModel.booking.flight.airline
        flightNumberTF.text = viewModel.booking.flight.flightNumber
        flightFromTF.text = viewModel.booking.flight.arrivingFrom
        notesTV.text = viewModel.booking.notes
        exitDateTF.text = viewModel.booking.parking.exitDate
    }
}

extension TicketTVC{
    private func registerCustomerBindings() {
        
        customerNameTF.bind { [weak self] in
            self?.viewModel.setCustomerName($0)
        }
        
        customerPhoneTF.bind { [weak self] in
            self?.viewModel.booking.customer.phone = $0
        }
        
        customerEmailTF.bind { [weak self] in
            self?.viewModel.booking.customer.email = $0
        }
        
    }

    private func registerVehicleBindings() {
        
        carMakerTF.bind { [weak self] in
            self?.viewModel.booking.vehicle.maker = $0
        }
        
        carModelTF.bind { [weak self] in
            self?.viewModel.booking.vehicle.model = $0
        }
        
        carYearTF.bind { [weak self] in
            self?.viewModel.booking.vehicle.year = $0
        }
        
        carColorTF.bind { [weak self] in
            self?.viewModel.booking.vehicle.color = $0
        }
        
        carPlateTF.bind { [weak self] in
            self?.viewModel.booking.vehicle.plate = $0
        }
        
    }


    private func registerFlightBindings(){

        flightAirlineTF.bind { [weak self] in
            self?.viewModel.booking.flight.airline = $0
        }
        
        flightNumberTF.bind { [weak self] in
            self?.viewModel.booking.flight.flightNumber = $0
        }

        flightFromTF.bind { [weak self] in
            self?.viewModel.booking.flight.arrivingFrom = $0
        }
        
        exitDateTF.bind { [weak self] in
            print($0)
            self?.viewModel.booking.parking.exitDate = $0
        }

    }

    private func registerBindings(){
        registerCustomerBindings()
        registerVehicleBindings()
        registerFlightBindings()
        notesTV.bind { [weak self] in
            self?.viewModel.booking.notes = $0
        }
    }
    
    
}


extension TicketTVC : UITextFieldDelegate, UITextViewDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.accessibilityIdentifier == "exitDateTF" && !viewModel.isEditMode(){
            IQKeyboardManager.shared.enableAutoToolbar = true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.accessibilityIdentifier == "exitDateTF" && !viewModel.isEditMode(){
            IQKeyboardManager.shared.enableAutoToolbar = false
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.returnKeyType == .done{
            textField.resignFirstResponder()
        }
        return true
    }
    
    
}
