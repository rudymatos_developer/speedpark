//
//  ScanVC.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit
import Lottie
import IQKeyboardManagerSwift

class ScanVC: UIViewController, StoryboardInit, CardCreator, Animator, ViewLoadingType{
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var animatorView: UIView!
    @IBOutlet weak var reservationNumberTF: UITextField!
    @IBOutlet weak var instructionsLBL: UILabel!
    var loadingScreen: LoadingScreen!
    
    var viewModel : ScanVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        reservationNumberTF.becomeFirstResponder()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    private func configureView(){
        instructionsLBL.text = viewModel.getInstructionsText()
        createCard(shadowView: shadowView, mainView: cardView)
        let lottieView = AnimationView(name: "scan_animation")
        lottieView.frame = animatorView.bounds
        animatorView.addSubview(lottieView)
        lottieView.loopMode = .loop
        lottieView.play()
        IQKeyboardManager.shared.enableAutoToolbar = true
    }
    
    @IBAction func process(_ sender: UIButton) {
        guard let text = reservationNumberTF.text, !text.isEmpty else {
            shake(view: reservationNumberTF)
            return
        }
        let ticketNumber = text
        view.endEditing(true)
        showLoadingScreen()
        viewModel.assign(ticketNumber) { [weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.removeLoadingScreen()
                switch result{
                case .success:
                    IQKeyboardManager.shared.enableAutoToolbar = false
                    self.dismiss(animated: true, completion: nil)
                case .failure(let error):
                    self.displaySimpleAlertMessage(title: "Error Assigning Ticket To Booking", message: error.message)
                }
            }
        }
    }
    
    @IBAction func dismissVC(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
}
