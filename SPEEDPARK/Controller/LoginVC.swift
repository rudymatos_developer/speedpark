//
//  ViewController.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit
import Lottie

class LoginVC: UIViewController, StoryboardInit, ViewLoadingType {
    
    @IBOutlet weak var lottieView: UIView!
    @IBOutlet weak var emailTF: BindiableTF!
    @IBOutlet weak var passwordTF: BindiableTF!
    @IBOutlet weak var errorMessageLBL: UILabel!
    private var currentTextField : BindiableTF?
    var loadingScreen: LoadingScreen!
    
    var viewModel : LoginVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLottie()
        registerBinding()
    }
    
    @IBAction func login(_ sender: UIButton) {
        login()
    }
    
    private func login(){
        view.endEditing(true)
        showLoadingScreen()
        viewModel.login { [weak self] result in
            guard let self = self else {return}
            self.removeLoadingScreen()
            switch result{
            case .failure(let error):
                self.errorMessageLBL.isHidden = false
                self.errorMessageLBL.text = error.message
            default:
                self.errorMessageLBL.isHidden = true
            }
        }
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
}

extension LoginVC{
    
    private func registerBinding(){
        
        emailTF.bind{ [weak self] in
            self?.viewModel.set(email: $0)
        }

        passwordTF.bind{ [weak self] in
            self?.viewModel.set(password: $0)
        }
        
    }
    
    private func configureLottie(){
        lottieView.initLottieView(withName: "login_animation")
    }
    
}


extension LoginVC: UITextFieldDelegate{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField as? BindiableTF
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let tf = textField as? BindiableTF {
            if tf.returnKeyType == .next {
                tf.resignFirstResponder()
                tf.nextField?.becomeFirstResponder()
            } else if tf.returnKeyType == .done {
                login()
            }
        }
        return true
    }
}
