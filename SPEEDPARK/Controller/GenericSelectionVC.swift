//
//  GenericSelection.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 6/20/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

protocol GenericSelectionDelegate: class{
    func select(genericSelectionData: GenericSelectionData)
}

class GenericSelectionVC: UIViewController, StoryboardInit, CardCreator {
    
    @IBOutlet weak var lottieView: UIView!
    @IBOutlet weak var resultsTV: UITableView!
    @IBOutlet weak var selectView: UIView!
    @IBOutlet weak var selectBTN: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var viewModel: GenericSelectionVM!
    
    weak var delegate: GenericSelectionDelegate?
    private var observation : NSKeyValueObservation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        observePoso()
        configureView()
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    @IBAction func dismissVC(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func selectEntry(_ sender: UIButton) {
        if viewModel.poso.isValid(), let selectedEntry = viewModel.getSelectedEntry(){
            delegate?.select(genericSelectionData: selectedEntry)
            dismiss(animated: true, completion: nil)
        }
    }
}

extension GenericSelectionVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.selectOption(at: indexPath.row)
        DispatchQueue.main.async {
            self.resultsTV.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(viewModel.getHeight(byRow: indexPath.row))
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "genericSelectionCell") as? GenericSelectionCell else{
            return UITableViewCell()
        }
        
        let option = viewModel.getOption(byRow: indexPath.row)
        cell.backgroundColor = indexPath.row % 2 == 0 ? #colorLiteral(red: 0.968627451, green: 0.968627451, blue: 0.968627451, alpha: 1) : nil
        cell.configureView(genericSelectionData: option)
        return cell
    }
}

extension GenericSelectionVC{
    
    private func observePoso(){
        self.observation = viewModel.poso.observe(\GenericSelectionPOSO.selection, options: .new) { [weak self] poso, change in
            self?.shouldEnabledSelectionBTN(isPojoValid: poso.isValid())
        }
    }
    
    private func shouldEnabledSelectionBTN(isPojoValid: Bool){
        selectView.backgroundColor = isPojoValid ? UIColor(named: SpeedParkConstants.Color.main) : UIColor(named: SpeedParkConstants.Color.disabled)
        selectBTN.isEnabled = isPojoValid
    }
    
    private func registerCells(){
        resultsTV.register(UINib(nibName: "GenericSelectionCell", bundle: Bundle.main), forCellReuseIdentifier: "genericSelectionCell")
    }
    
    func configureView(){
        createCard(shadowView: shadowView, mainView: cardView)
        switch  viewModel.getConfiguratorType() {
        case .carMake:
            searchBar.placeholder = "Enter Car Maker"
            lottieView.initLottieView(withName: "car")
        case .carModel:
            searchBar.placeholder = "Enter Car Model"
            lottieView.initLottieView(withName: "car")
        default:
            searchBar.placeholder = "Enter Customer Name"
            lottieView.initLottieView(withName: "user")
        }
    }
}

extension GenericSelectionVC: UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        viewModel.filter(by: searchText)
        DispatchQueue.main.async {
            self.resultsTV.reloadData()
        }
    }
}

