//
//  TipsVC.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/27/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

protocol TipsSelectionDelegate: class{
    func select(tip: SPTip)
}

class TipsVC: UIViewController, StoryboardInit, CardCreator {
    
    @IBOutlet weak var totalLBL: UILabel!
    @IBOutlet weak var fivePercentBTN: UIButton!
    @IBOutlet weak var tenPercentBTN: UIButton!
    @IBOutlet weak var fifteenPercentBTN: UIButton!
    @IBOutlet weak var twentyPercentBTN: UIButton!
    @IBOutlet weak var addCustomAmountBTN: UIButton!
    @IBOutlet weak var customTipTF: UITextField!
    @IBOutlet weak var noTipBTN: UIButton!
    @IBOutlet weak var addCustomTipView: UIView!
    
    weak var delegate :TipsSelectionDelegate?
    
    var currentTotal: Double!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    private func configureView(){
        totalLBL.text = String(format: "$%.02f", currentTotal)
        fivePercentBTN.setTitle(getTitle(forTip: .five), for: .normal)
        tenPercentBTN.setTitle(getTitle(forTip: .ten), for: .normal)
        fifteenPercentBTN.setTitle(getTitle(forTip: .fifteen), for: .normal)
        twentyPercentBTN.setTitle(getTitle(forTip: .twenty), for: .normal)
        addKeyboardDismissGesture()
        customTipTF.addTarget(self, action: #selector(customTipTFValueChanged(textField:)), for: .editingChanged)
    }
    
    private func getTitle(forTip: SPTip) -> String{
        return "\(String(format: "$%.02f",forTip.getPercentage(fromValue: currentTotal))) (\(forTip.getDisplayName()))"
    }
    
    private func shouldEnableAddCustomTipBTN(enabled: Bool){
        addCustomAmountBTN.isEnabled = enabled
        addCustomTipView.backgroundColor = enabled ? UIColor(named: SpeedParkConstants.Color.main) : UIColor(named: SpeedParkConstants.Color.disabled)
    }
    
    @IBAction func addTip(_ sender: UIButton) {
        guard let title = sender.accessibilityIdentifier else{
            return
        }
        
        var selectedTip: SPTip = .none
        
        let tip = SPTip(value: title)
        if tip == .five || tip == .ten || tip == .fifteen || tip == .twenty  {
            selectedTip = tip
        }
        
        if title == "noTip"{
            selectedTip = .custom(0)
        }
        
        if title == "custom",let tipAmountText = customTipTF.text, !tipAmountText.isEmpty,  let customTipAmount = Double(tipAmountText), customTipAmount > 0 && customTipAmount < 200{
            selectedTip = .custom(customTipAmount)
        }
        
        if selectedTip != .none{
            delegate?.select(tip: selectedTip)
            dismiss(animated: true, completion: nil)
        }
    }
    
    private func addKeyboardDismissGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(TipsVC.dismissKeyboard))
        view.addGestureRecognizer(tapGesture)
    }
    
    @objc private func dismissKeyboard(){
        customTipTF.resignFirstResponder()
    }
    
    
    @objc private func customTipTFValueChanged(textField : UITextField){
        guard let tipAmountText = textField.text, let tipAmount = Double(tipAmountText), tipAmount > 0 && tipAmount <= 200 else{
            shouldEnableAddCustomTipBTN(enabled: false)
            return
        }
        shouldEnableAddCustomTipBTN(enabled: true)
    }
    
    
}

