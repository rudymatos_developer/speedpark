//
//  ProfileVC.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 5/25/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController, StoryboardInit , ViewLoadingType{

    @IBOutlet weak var loggedInDateLBL: UILabel!
    @IBOutlet weak var loggedInUserLBL: UILabel!
    @IBOutlet weak var resetParkingBTN: UIButton!
    
    var loadingScreen: LoadingScreen!
    var viewModel:ProfileVM!
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    private func configureView(){
        title = "Profile"
        navigationController?.navigationBar.prefersLargeTitles = false
        loggedInDateLBL.text = viewModel.getEnterTime()
        loggedInUserLBL.text = viewModel.getDisplayName()
        resetParkingBTN.isHidden = !viewModel.isCurrentUserAdmin()
    }
    
    @IBAction func show7DaysProjectionReport(_ sender: UIButton) {
        showLoadingScreen()
        viewModel.generateProjectionReport { [weak self , unowned viewModel] result in
            guard let self = self else { return }
            if case .success(let data) = result{
                self.removeLoadingScreen()
                viewModel?.goToProjection(availability: data)
            }
        }
    }
    
    @IBAction func resetParkingSpots(_ sender: UIButton) {
        displayConfirmationDialog(title: "Reset Parking", message: "Do you want to reset all the parking spots?") { _ in
            ParkingMaintenance().resetAllParkingSpots()
        }
    }
    
    @IBAction func logout(_ sender: UIButton) {
        displayConfirmationDialog(title: "Logout", message: "Do you really want to logout?", okTitle: "I'm sure!", isOKDesctructive: true) { [weak self] _ in
            guard let self = self else {return}
            self.showLoadingScreen()
            self.viewModel.logout{ 
                self.removeLoadingScreen()
            }
        }
    }
}
