//
//  ParkingVehicleVC.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/1/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit
import Lottie
import IQKeyboardManagerSwift


class ParkVehicleVC: UIViewController, StoryboardInit, CardCreator, ViewLoadingType {
    
    @IBOutlet weak var lottieView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var categoriesCV: UICollectionView!
    @IBOutlet weak var contentCV: UICollectionView!
    @IBOutlet weak var ticketNumberLBL: UILabel!
    @IBOutlet weak var componentsSV: UIStackView!
    @IBOutlet weak var noKeysLBL: UILabel!
    @IBOutlet weak var keysCheckedInSV: UIStackView!
    @IBOutlet weak var parkVehicleView: UIView!
    @IBOutlet weak var parkVehicleBTN: UIButton!
    var loadingScreen: LoadingScreen!
    
    private var categoriesDD : CategoriesDD!
    private var contentDD : ContentDD!
    
    var viewModel: ParkVehicleVM!
    private var observation = [NSKeyValueObservation]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        registerDDs()
        configureView()
        observePoso()
        loadParkingSpots()
    }
    
    private  func configureView(){
        ticketNumberLBL.text = viewModel.getTicketNumber()
        createCard(shadowView: shadowView, mainView: cardView)
        lottieView.initLottieView(withName: "search_location")
        shouldEnableParkVehicleButton(isPojoValid: false)
        IQKeyboardManager.shared.enableAutoToolbar = true
        noKeysLBL.isHidden = !viewModel.isBookingNoKeys()
        keysCheckedInSV.isHidden = viewModel.isBookingNoKeys()
    }
    
    @IBAction func keyCheckedInValueChanged(_ sender: UISwitch) {
        viewModel.poso.keyStatus = sender.isOn ? SPBooking.Parking.KeyStatus.keysCheckedIn : SPBooking.Parking.KeyStatus.keysNotCheckedIn
    }
    
    private func observePoso(){
        self.observation = [viewModel.poso.observe(\ParkTicketPOSO.location, options: .new) { [weak self] poso, change in
            self?.shouldEnableParkVehicleButton(isPojoValid: poso.isValid())
        },
        viewModel.poso.observe(\ParkTicketPOSO.spot, options: .new) { [weak self] poso, change in
            self?.shouldEnableParkVehicleButton(isPojoValid: poso.isValid())
        },
        viewModel.poso.observe(\ParkTicketPOSO.parkedBy, options: .new) { [weak self] poso, change in
            self?.shouldEnableParkVehicleButton(isPojoValid: poso.isValid())
        }]
    }
    
    private func shouldEnableParkVehicleButton(isPojoValid: Bool){
        parkVehicleBTN.isEnabled = isPojoValid
        parkVehicleView.backgroundColor = isPojoValid ? UIColor(named: SpeedParkConstants.Color.main) : UIColor(named: SpeedParkConstants.Color.disabled)
    }
    
    private func loadParkingSpots(){
        viewModel.loadAdditionalInformation { [weak self] _ in
            guard let self = self else {return}
            let indexPath = IndexPath(item: 1, section: 0)
            self.categoriesCV.delegate?.collectionView?(self.categoriesCV, didSelectItemAt: indexPath)
        }
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    @IBAction func dismissView(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func parkVehicle(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Park Vehicle", message: viewModel.getConfirmationTitle(), preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Yes, I'm Sure!", style: .destructive) { [weak self] _ in
            guard let self = self else {return}
            self.showLoadingScreen()
            self.viewModel.parkVehicle {
                self.removeLoadingScreen()
                self.dismiss(animated: true, completion: nil)
                IQKeyboardManager.shared.enableAutoToolbar = false
            }
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alert.addAction(okAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    private func registerCells(){
        categoriesCV.register(UINib(nibName: "CategoryCell", bundle: Bundle.main), forCellWithReuseIdentifier: "CategoryCell")
        contentCV.register(UINib(nibName: "ParkCarOptionSelector", bundle: Bundle.main), forCellWithReuseIdentifier: "ParkCarOptionSelector")
        contentCV.register(UINib(nibName: "ParkCarOther", bundle: Bundle.main), forCellWithReuseIdentifier: "ParkCarOther")
    }
    
    private func registerDDs(){
        categoriesDD = CategoriesDD(maxWidth: componentsSV.bounds.width, viewModel: viewModel)
        contentDD = ContentDD(viewModel: viewModel)
        categoriesDD.selectItemCompletion = { [weak self] indexPath in
            self?.viewModel.changeCategory(forItem: indexPath.item)
            DispatchQueue.main.async {
                self?.categoriesCV.reloadData()
                self?.contentCV.scrollToItem(at: indexPath, at: .right, animated: true)
            }
        }
        categoriesCV.delegate = categoriesDD
        categoriesCV.dataSource = categoriesDD
        
        contentCV.isPagingEnabled = true
        contentCV.delegate = contentDD
        contentCV.dataSource = contentDD
        
    }
}

