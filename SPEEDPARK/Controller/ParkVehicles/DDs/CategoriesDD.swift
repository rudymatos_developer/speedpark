//
//  CategoriesDD.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/3/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

class CategoriesDD: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    private var maxWidth: CGFloat
    private var viewModel: ParkVehicleVM
    
    init(maxWidth: CGFloat, viewModel: ParkVehicleVM){
        self.maxWidth = maxWidth
        self.viewModel = viewModel
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    var selectItemCompletion : ((IndexPath) -> Void)?
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: maxWidth / CGFloat(viewModel.categories.count) - 10, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectItemCompletion?(indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCell", for: indexPath) as? CategoryCell else {
            return UICollectionViewCell()
        }
        cell.set(category: viewModel.categories[indexPath.item])
        return cell
    }
    
}
