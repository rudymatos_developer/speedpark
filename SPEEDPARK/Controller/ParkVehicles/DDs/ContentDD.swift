//
//  ContentDD.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/3/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

class ContentDD: NSObject, UICollectionViewDataSource, UICollectionViewDelegate,UICollectionViewDelegateFlowLayout{
    
    private var viewModel: ParkVehicleVM
    
    init(viewModel: ParkVehicleVM){
        self.viewModel = viewModel
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let category = ParkVehicleCategory.getCategory(byIndex: indexPath.item) else { return UICollectionViewCell() }
        switch category.type{
        case .location, .spot, .valet:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ParkCarOptionSelector.identifier, for: indexPath) as? ParkCarOptionSelector else {
                return UICollectionViewCell()
            }
            cell.type = category.type
            cell.viewModel = viewModel
            cell.options = viewModel.getOptions(forType: category.type) ?? []
            if let spotIndex = ParkVehicleCategory.getIndexBy(type: .spot){
                switch category.type{
                case .location:
                    cell.locationSelectorCompletion = { [weak self] location in
                        self?.viewModel.cleanUpPosoSpot()
                        self?.viewModel.fillUpFilteredParkingSpots(withFilter: .all)
                        collectionView.reloadItems(at: [IndexPath(row: spotIndex, section: 0)])
                    }
                case .spot:
                    cell.selectParkingSpotSegmentCompletion = { [weak self] filter in
                        self?.viewModel.fillUpFilteredParkingSpots(withFilter: filter)
                        cell.options = self?.viewModel.getOptions(forType: category.type) ?? []
                        (collectionView.cellForItem(at: IndexPath(row: spotIndex, section: 0)) as? ParkCarOptionSelector)?.reloadData()
                    }
                default:
                    print("do nothing")
                }
            }
            cell.backgroundColor = .clear
            return cell
        case .other:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ParkCarOther", for: indexPath) as? ParkCarOther else {
                return UICollectionViewCell()
            }
            cell.poso = viewModel.poso
            return cell
        }
    }
}
