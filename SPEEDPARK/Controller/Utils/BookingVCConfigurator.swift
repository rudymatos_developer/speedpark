//
//  BookingVCConfigurator.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/30/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

class BookingVCConfigurator{
    
    typealias Dependencies = HasUser & HasDataService
    var dependencies: Dependencies?
    
    enum BookingFilter: CaseIterable, Equatable{
        
        case todayResevation
        case tomorrowResevation
        case paid
        case parked
        case noParked
        case keysCheckedIn
        case noKeys
        case keysNotCheckedIn
        case swapable
        case all
        
        func getTitle() -> String{
            switch self{
            case .todayResevation:
                return "Today's Reservations"
            case .tomorrowResevation:
                return "Tomorrow's Reservations"
            case .paid:
                return "Paid"
            case .parked:
                return "Parked"
            case .noParked:
                return "No Parked"
            case .keysCheckedIn:
                return "Keys Ckecked In"
            case .noKeys:
                return "No Keys"
            case .swapable:
                return "Swapables"
            case .keysNotCheckedIn:
                return "Keys Not Checked In"
            case .all:
                return "Show All"
            }
        }
        
        func shouldAppearOn(bookingType: BookingVCConfigurator.BookingType) -> Bool{
            switch bookingType{
            case .tickets:
                return true
            case .active:
                return self == .paid || self == .all
            case .reservations:
                return self == .todayResevation || self == .tomorrowResevation || self == .all
            case .new:
                return false
            }
        }
        
    }
    
    
    enum BookingSorter: CaseIterable{
        case ticketNumberAsc
        case ticketNumberDesc
        case customerNameAsc
        case customerNameDesc
        case reservationNumberAsc
        case reservationNumberDesc
        case enterDateAsc
        case enterDateDesc
        case parkingSpotAsc
        case parkingSpotDesc
        
        func getTitle() -> String{
            switch self{
            case .ticketNumberAsc:
                return "Ticket Number Ascending"
            case .ticketNumberDesc:
                return "Ticket Number Descending"
            case .customerNameAsc:
                return "Customer Name Ascending"
            case .customerNameDesc:
                return "Customer Name Descending"
            case .reservationNumberAsc:
                return "Reservation # Ascending"
            case .reservationNumberDesc:
                return "Reservation # Descending"
            case .parkingSpotAsc:
                return "Parking Spot Ascending"
            case .parkingSpotDesc:
                return "Parking Spot Descending"
            case .enterDateAsc:
                return "Enter Date Ascending"
            case .enterDateDesc:
                return "Enter Date Descending"
                
            }
        }
        
        func shouldAppearOn(bookingType: BookingVCConfigurator.BookingType) -> Bool{
            switch bookingType{
            case .tickets:
                return true
            case .active:
                return self == .customerNameAsc || self == .customerNameDesc || self == .reservationNumberAsc || self == .reservationNumberDesc || self == .parkingSpotAsc || self == .parkingSpotDesc
            case .reservations:
                return self == .customerNameAsc || self == .customerNameDesc || self == .reservationNumberAsc || self == .reservationNumberDesc || self == .enterDateAsc || self == .enterDateDesc
            case .new:
                return self == .ticketNumberAsc || self == .ticketNumberDesc || self == .parkingSpotAsc || self == .parkingSpotDesc
            }
        }
    }
    
    enum BookingType{
        case tickets
        case reservations
        case active
        case new
        
        func getColor() -> UIColor{
            return #colorLiteral(red: 0.231372549, green: 0.4549019608, blue: 0.7254901961, alpha: 1)
        }
    }
    
    init(dependencies: Dependencies, title: String, bookingType: BookingType, sortBy: BookingSorter? = nil, filterBy: BookingFilter? = nil){
        self.dependencies = dependencies
        self.title = title
        self.bookingType = bookingType
        self.sortBy = sortBy
        self.filterBy = filterBy
        setDataLoader()
    }
    
    var sortBy: BookingSorter?
    var filterBy: BookingFilter?
    var title: String
    var bookings : [SPBooking] = []
    var filteredBookings : [SPBooking] = []
    var dataLoader: (()->Void) = {}
    var dataLoaderCompletion: (()->Void)?
    var bookingType: BookingType
    var bookingStatus : SPBooking.Status = .booked
    
}

extension BookingVCConfigurator: SPDateFormatter{
    
    func getFilteredResultsCount() -> Int{
        return filteredBookings.count
    }
    
    func sort(with: BookingSorter){
        filterBy(value: "")
        sortBy = with
        filteredBookings.sort { (b1, b2) -> Bool in
            switch with{
            case .ticketNumberAsc:
                return b1.ticketNumber < b2.ticketNumber
            case .ticketNumberDesc:
                return b1.ticketNumber > b2.ticketNumber
            case .customerNameAsc:
                return b1.customer.name < b2.customer.name
            case .customerNameDesc:
                return b1.customer.name > b2.customer.name
            case .reservationNumberAsc:
                return b1.identifier < b2.identifier
            case .reservationNumberDesc:
                return b1.identifier > b2.identifier
            case .parkingSpotAsc:
                return b1.parking.spot < b2.parking.spot
            case .parkingSpotDesc:
                return b1.parking.spot > b2.parking.spot
            case .enterDateAsc:
                return b1.parking.enterDate < b2.parking.enterDate
            case .enterDateDesc:
                return b1.parking.enterDate > b2.parking.enterDate
            }
        }
    }
    
    func filter(with: BookingFilter){
        filterBy = with
        filteredBookings = bookings.filter{ booking -> Bool in
            switch with{
            case .all:
                return true
            case .noKeys:
                return booking.status == .parked && booking.parking.keyStatus == .noKeys
            case .keysCheckedIn:
                return booking.status == .parked && booking.parking.keyStatus == .keysCheckedIn
            case .keysNotCheckedIn:
                return booking.status == .parked && booking.parking.keyStatus == .keysNotCheckedIn
            case .parked:
                return booking.status == .parked
            case .noParked:
                return booking.status == .checkedIn
            case .paid:
                return booking.payment.paid
            case .swapable:
                return booking.isSwapable
            case .todayResevation:
                let todaysDate = Date()
                return calculate(date: todaysDate, booking: booking)
            case .tomorrowResevation:
                let tomorrowsDate = Date(timeIntervalSinceNow: 60*60*24*1)
                return calculate(date: tomorrowsDate, booking: booking)
            }
        }
        
        if with == .tomorrowResevation{
            sort(with: .enterDateAsc)
        }
        
        func calculate(date: Date, booking: SPBooking) -> Bool{
            let bookingDate = convert(string: booking.parking.enterDate, usingPattern: .speedParkLongFormat)
            let systemTodaysDateString = convert(date: date, usingPattern: .main)
            let bookingDateString = convert(date: bookingDate, usingPattern: .main)
            return systemTodaysDateString == bookingDateString
        }
    }
    
    func filterBy(value: String){
        guard value.trimmingCharacters(in: .whitespaces) != "" else {
            filteredBookings = bookings
            return
        }
        filteredBookings = bookings.filter({$0.filterBy(value: value)})
    }
    
    func setDataLoader(){

        func performSuccessAction(with bookings: [SPBooking]){
            self.bookings = bookings
            self.filteredBookings = bookings
            if let sortBy = sortBy{
                sort(with: sortBy)
            }
            if let filterBy = filterBy{
                filter(with: filterBy)
            }
            self.dataLoaderCompletion?()
        }
        
        func getBookingByStatus(){
            dependencies?.dataService.listenToAllBooking(byStatus: bookingStatus, completion: { result in
                switch result{
                case .success(let bookings):
                    performSuccessAction(with: bookings)
                case .failure(let error):
                    print(error)
                }
            })
        }
        
        func getBookingWithTicket(){
            dependencies?.dataService.listenToAllOpenTickets{ result in
                switch result{
                case .success(let bookings):
                    performSuccessAction(with: bookings)
                case .failure(let error):
                    print(error)
                }
            }
        }
        
        switch bookingType {
        case .reservations:
            bookingStatus = .booked
            dataLoader = getBookingByStatus
        case .active:
            bookingStatus = .active
            dataLoader = getBookingByStatus
        case .new:
            bookingStatus = .new
            dataLoader = getBookingByStatus
        case .tickets:
            dataLoader = getBookingWithTicket
        }
    }
    
}
