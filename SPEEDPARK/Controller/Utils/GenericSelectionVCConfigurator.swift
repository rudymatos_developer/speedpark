//
//  GenericSelectionVCConfigurator.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 6/20/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation


class GenericSelectionVCConfigurator: HasDependencies{
    
    typealias Dependencies = HasUser & HasDataService & HasCarAPIService
    var dependencies: Dependencies?
    
    private var results : [GenericSelectionData] = []
    var filteredResults : [GenericSelectionData] = []
    private var defaultValue: GenericSelectionData?
    var genericSelectionType: GenericSelectionType
    var dataLoadCompletion: ((Result<Bool, SpeedParkError>) -> Void)?
    private var selectedValue: GenericSelectionData?
    
    init(dependencies: Dependencies, genericSelectionType: GenericSelectionType, defaultValue: GenericSelectionData? = nil){
        self.dependencies = dependencies
        self.defaultValue = defaultValue
        self.genericSelectionType = genericSelectionType
    }
    
    enum GenericSelectionType: Equatable{
        case customer
        case carMake
        case carModel(String)
        
        func getIconName() -> String{
            switch self{
            case .carMake, .carModel:
                return "gs_car"
            default:
                return "gs_user"
            }
        }
        
        static func getType(by: String) -> GenericSelectionType?{
            switch by {
            case "searchCustomer":
                return .customer
            case "searchMaker":
                return .carMake
            case "searchModels":
                return .carModel("")
            default:
                return nil
            }
        }
    }
}

extension GenericSelectionVCConfigurator{
    
    func filter(by: String){
        guard by.trimmingCharacters(in: .whitespaces) != "" else {
            filteredResults = results
            return
        }
        filteredResults = results.filter({$0.contains(by)})
    }
    
    func select(value: GenericSelectionData){
        func toggleSelection(array: [GenericSelectionData]){
            if let selectedValue = selectedValue , let currentIndex = array.firstIndex(where: {$0 == selectedValue}){
                array[currentIndex].toggleSelected()
            }
            if let valueIndex = array.firstIndex(where: {$0 == value}){
                array[valueIndex].toggleSelected()
            }
        }
        toggleSelection(array: results)
        toggleSelection(array: filteredResults)
        value.toggleSelected()
        selectedValue = value
    }
}

extension GenericSelectionVCConfigurator{
    
    func loadData(){
        switch genericSelectionType {
        case .carMake:
            dependencies?.carAPIService?.getAllCarMakers(completion: { [weak self] result in
                guard let self = self else {return}
                self.process(result, ofType: .carMake)
            })
        case .carModel(let maker):
            dependencies?.carAPIService?.getAllModels(byMaker: maker){ [weak self] result in
                guard let self = self else {return}
                self.process(result, ofType: .carModel(maker))
            }
        case .customer:
            dependencies?.dataService.getAllCustomerNames(completion: { [weak self] result in
                guard let self = self else {return}
                self.process(result, ofType: .customer)
            })
        }
    }
    
    private func process(_ result: Result<[GenericSelectionData], SpeedParkError>, ofType: GenericSelectionVCConfigurator.GenericSelectionType){
        switch result{
        case .failure(let error):
            dataLoadCompletion?(.failure(error))
            return
        case .success(let requestResults):
            requestResults.forEach{$0.set(type: ofType)}
            self.results = requestResults
            validateDefaultValue(ofType: ofType)
        }
    }
    
    private func validateDefaultValue(ofType: GenericSelectionVCConfigurator.GenericSelectionType){
        if let defaultValue = defaultValue{
            if let index = results.firstIndex(where: {$0 == defaultValue}){
                results[index].selected = true
            }else{
                results.append(GenericSelectionData(displayName: defaultValue.displayName, info: defaultValue.info, additionalInfo: defaultValue.additionalInfo , selected: true, type:ofType))
            }
            selectedValue = defaultValue
        }
        results.sort(by: {$0.displayName < $1.displayName})
        filteredResults = results
        dataLoadCompletion?(.success(true))
    }
    
}
