//
//  DayAxisValueFormatter.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 2/8/20.
//  Copyright © 2020 TonyS. All rights reserved.
//

import Foundation
import Charts

class DayAxisValueFormatter: NSObject, IAxisValueFormatter{
    
    
    weak var chart: BarChartView?
    
    init(chart: BarChartView){
        self.chart = chart
    }
    
    private lazy var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        return dateFormatter
    }()
    
    
    func stringForValue(_ value: Double, axis: AxisBase?) -> String {
        let day = Int(value)
        if let currentDate = Calendar.current.date(byAdding: .day, value: day, to: Date()) {
            return dateFormatter.string(from: currentDate)
        }
        return "INVALID_DATE"
    }
    
}
