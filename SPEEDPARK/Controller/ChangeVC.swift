//
//  ChangeVC.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/23/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class ChangeVC: UIViewController, CardCreator, StoryboardInit, Animator {
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var ticketLBL: UILabel!
    @IBOutlet weak var totalLBL: UILabel!
    @IBOutlet weak var amountTF: UITextField!
    @IBOutlet weak var returnLBL: UILabel!
    @IBOutlet weak var payBTNView: UIView!
    @IBOutlet weak var payBTN: UIButton!
    
    var viewModel : ChangeVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    private func configureView(){
        createCard(shadowView: shadowView, mainView: cardView)
        ticketLBL.text = viewModel.getTicketNumber()
        totalLBL.text = viewModel.getFormattedTotal()
        returnLBL.text = viewModel.getCashToReturn()
        IQKeyboardManager.shared.enableAutoToolbar = true
        isPayBTNView(enabled: false)
    }
    
    @IBAction func pay(_ sender: UIButton) {
        displayConfirmationDialog(title: "Pay Ticket", message: viewModel.getPayMessage()) { [weak self] _ in
            IQKeyboardManager.shared.enableAutoToolbar = false
            self?.viewModel.pay()
            self?.viewModel.callPaymentCompletion()
            self?.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func dismisView(_ sender: UIButton) {
        IQKeyboardManager.shared.enableAutoToolbar = false
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func setCash(_ sender: UIButton) {
        guard let title = sender.title(for: .normal) else {
            return
        }
        setCashAmount(fromText: title)
    }
}

extension ChangeVC{
    
    private func isPayBTNView(enabled: Bool){
        payBTN.isEnabled = enabled
        payBTNView.backgroundColor = enabled ? UIColor(named: SpeedParkConstants.Color.main) : UIColor(named: SpeedParkConstants.Color.disabled)
    }
    
    private func setCashAmount(fromText text: String?){
        guard let amountString = text, !amountString.isEmpty, let amount = viewModel.getInCashValue(amountString: amountString), amount > 0, amount >= viewModel.getTotal() else{
            shake(view: returnLBL)
            isPayBTNView(enabled: false)
            returnLBL.text = "$0.00"
            return
        }
        isPayBTNView(enabled: true)
        viewModel.setInCash(amount: amount)
        viewModel.calculateReturn()
        amountTF.text = viewModel.getInCashAmountDisplayString()
        returnLBL.text = viewModel.getCashToReturn()
    }
}


extension ChangeVC: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        setCashAmount(fromText: textField.text)
    }
 
    
    
}
