//
//  TimeVC.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/18/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import UIKit
import Lottie

class TimeVC: UIViewController,StoryboardInit,Animator,CardCreator,ViewLoadingType{

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var setPickupTimeView: UIView!
    @IBOutlet weak var setPickupTimeBTN: UIButton!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var titleLBL: UILabel!
    @IBOutlet weak var minutesPickerView: UIPickerView!
    private var observation = [NSKeyValueObservation]()
    var loadingScreen: LoadingScreen!
    
    var viewModel: TimeVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        observeSelection()
        selectDefaultValue()
    }
    
    private func selectDefaultValue(){
        guard let defaultValueIndex = viewModel.getIndex(forMinutes: viewModel.defaultValue) else {
            return
        }
        minutesPickerView.selectRow(defaultValueIndex, inComponent: 0, animated: false)
        viewModel.set(minutes: viewModel.defaultValue)
    }
    
    private func observeSelection(){
        self.observation = [
            viewModel.observe(\TimeVM.selectedMinutes, options: .new) { [weak self] poso, _ in
                guard let self = self else {return}
                self.shouldEnableAssignPickUpTimeButton(isValidPickUpTime: self.viewModel.isValidPickUpTime())
            }
        ]
    }
    
    private func configureView() {
        titleLBL.text = viewModel.getInstructionsText()
        createCard(shadowView: shadowView, mainView: cardView)
        animationView.initLottieView(withName: "time_animation")
    }
    
    private func shouldEnableAssignPickUpTimeButton(isValidPickUpTime: Bool){
        setPickupTimeBTN.isEnabled = isValidPickUpTime
        setPickupTimeView.backgroundColor = isValidPickUpTime ? UIColor(named: SpeedParkConstants.Color.main) : UIColor(named: SpeedParkConstants.Color.disabled)
    }
    
    @IBAction func setPickUpTime(_ sender: UIButton) {
        guard viewModel.isValidPickUpTime() else {
            shake(view: minutesPickerView)
            return
        }
        showLoadingScreen()
        viewModel.setPickUpTime {  [weak self] result in
            guard let self = self else {return}
            DispatchQueue.main.async {
                self.removeLoadingScreen()
                switch result{
                case .success:
                    self.dismiss(animated: true, completion: nil)
                case .failure(let error):
                    self.displaySimpleAlertMessage(title: "Error Assigning PickUp Time", message: error.message)
                }
            }
        }
    }

    @IBAction func dismissVC(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}

extension TimeVC: UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.getNumberOfRows()
    }
    
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        viewModel.set(minutes: viewModel.getMinutes(byIndex: row))
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.getMinutes(byIndex: row)
    }
}
