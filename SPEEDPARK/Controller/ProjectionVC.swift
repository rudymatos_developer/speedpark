//
//  ProjectionVC.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 2/5/20.
//  Copyright © 2020 TonyS. All rights reserved.
//

import UIKit
import Charts


class ProjectionVC: UIViewController, StoryboardInit {

    var viewModel: ProjectionVM!
    
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var todaysProjectionLBL: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }

    private func configureView(){
        
        //Configuring View Controller first
        title = viewModel.title
        navigationItem.largeTitleDisplayMode = .never
        navigationController?.navigationBar.tintColor = UIColor(named: SpeedParkConstants.Color.main)
        
        //Configuring BarChartView
        barChartView.fitBars = true
        
        barChartView.maxVisibleCount = 60
        barChartView.drawValueAboveBarEnabled = false
        barChartView.drawBarShadowEnabled = false
        barChartView.chartDescription?.enabled = false
        barChartView.drawGridBackgroundEnabled = false
        barChartView.rightAxis.enabled = false

        barChartView.dragEnabled = false
        barChartView.setScaleEnabled(false)
        barChartView.pinchZoomEnabled = false
        barChartView.highlightPerTapEnabled = false
        barChartView.leftAxis.axisMinimum = 0.0
        barChartView.data = viewModel.chartData
        
        //Configuring Labels at the bottom of the chart
        let xAxis = barChartView.xAxis
        xAxis.labelPosition = .bottom
        xAxis.labelFont = .systemFont(ofSize: 10)
        xAxis.granularity = 1
        xAxis.labelCount = 7
        xAxis.labelRotationAngle = 25
        xAxis.valueFormatter = DayAxisValueFormatter(chart: barChartView)
        
        let legend = barChartView.legend
        legend.horizontalAlignment = .right
        
        todaysProjectionLBL.text = viewModel.todaysProjection
        
    }

}
