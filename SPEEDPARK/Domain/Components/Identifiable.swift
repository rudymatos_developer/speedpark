//
//  Identifiable.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/4/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

protocol Identifiable{
    static var identifier: String {get}
}

extension Identifiable{
    static var identifier: String {
        return String(describing: self)
    }
}
