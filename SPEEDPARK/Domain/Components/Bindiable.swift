//
//  Bindiable.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class Bindiable<T>{
    
    var completion: ((T)->Void)?
    
    init(_ value: T){
        self.value = value
    }
    
    var value: T{
        didSet{
            completion?(value)
        }
    }
    
    func bind(completion: @escaping ((T)->Void)){
        self.completion = completion
    }
    
    func bindAndFire(completion: @escaping ((T)->Void)){
        self.completion = completion
        self.completion?(value)
    }
    
}
