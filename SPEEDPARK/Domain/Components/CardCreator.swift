//
//  CardCreator.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

protocol CardCreator: class {
    func createCard(shadowView: UIView, mainView: UIView, cornerRadius: CGFloat, offSet: CGSize)
}

extension CardCreator {
    func createCard(shadowView: UIView, mainView: UIView, cornerRadius: CGFloat = 10,offSet: CGSize = CGSize(width: 3, height: 3)) {
        shadowView.layer.shadowColor = UIColor.black.cgColor
        shadowView.layer.shadowOffset = offSet
        shadowView.layer.shadowRadius = 4
        shadowView.layer.shadowOpacity = 0.7
        shadowView.layer.masksToBounds = false
        mainView.layer.cornerRadius = cornerRadius
        mainView.layer.masksToBounds = true
    }
}
