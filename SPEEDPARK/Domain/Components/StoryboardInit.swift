//
//  StoryboardInit.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

protocol StoryboardInit{
    static func getInstance(storyBoardName: String) -> Self
}

extension StoryboardInit where Self: UIViewController{
    
    static var identifier: String{
        return String(describing: self)
    }
    
    static func getInstance(storyBoardName: String) -> Self{
        let storyBoard = UIStoryboard(name: storyBoardName, bundle: Bundle.main)
        return storyBoard.instantiateViewController(withIdentifier: Self.identifier) as! Self
    }
}
