//
//  BindiableTF.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

class BindiableTF: UITextField{
    
    var callback: (String) -> Void = { _ in }
    @IBOutlet var nextField: UIView?
    
    func isEmpty() -> Bool {
        return text == nil || text?.trimmingCharacters(in: CharacterSet.whitespaces) == ""
    }
    
    func bind(callback: @escaping (String)->Void) {
        self.callback = callback
        addTarget(self, action: #selector(BindiableTF.setNewText(_:)), for: UIControl.Event.editingChanged)
        addTarget(self, action: #selector(BindiableTF.setNewText(_:)), for: UIControl.Event.editingDidEnd)
    }
    
    func set(text: String){
        if !text.isEmpty{
            self.text = text
            self.callback(text)
        }
    }
    
    @objc func setNewText(_ textField: UITextField) {
        if let text = textField.text, !text.isEmpty{
            self.callback(text)
        }
    }
    
}

class BindiableTV: UITextView, UITextViewDelegate{
    
    var callback: ((String) -> Void) = { _ in }
    
    func bind(callback: @escaping ((String) -> Void)){
        self.callback = callback
        delegate = self
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if let text = textView.text, !text.isEmpty{
            self.callback(text)
        }
    }
}
