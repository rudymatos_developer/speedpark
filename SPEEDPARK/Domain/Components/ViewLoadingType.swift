//
//  ViewLoadingType.swift
//  CPRMobileApp
//
//  Created by Rudy E Matos on 4/19/18.
//  Copyright © 2018 Bearded Gentleman. All rights reserved.
//

import Foundation
import UIKit

protocol ViewLoadingType: class {
    var loadingScreen : LoadingScreen! {get set}
}

extension ViewLoadingType where Self: UIViewController {
    
    private func initLoadingScreenIfNeeded(){
        if loadingScreen == nil{
            self.loadingScreen = LoadingScreen(frame: UIApplication.shared.keyWindow!.frame)
        }
    }
    
    func showLoadingScreen() {
        initLoadingScreenIfNeeded()
        UIApplication.shared.keyWindow?.addSubview(loadingScreen)
    }
    
    func removeLoadingScreen() {
        DispatchQueue.main.async {
            self.loadingScreen.removeFromSuperview()
        }
    }
}
