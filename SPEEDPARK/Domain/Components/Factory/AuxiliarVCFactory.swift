//
//  AuxiliarVCFactory.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 4/29/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

enum AuxiliarFactory{
    
    case scan(SPBooking)
    case change(ChangePOSO)
    case parkVehicle(SPBooking, ParkVehicleVM.Mode)
    case time(SPBooking)
    case pay(SPBooking)
    case ticket(SPBooking?)
    case genericSelection(GenericSelectionVCConfigurator)
    case projection
    
}

class AuxiliarVCFactory: HasDependencies{
    
    
    typealias Dependencies = HasDataService & HasUserDefaultService & HasAuthorizableService & HasUser & HasMessageService & HasSoundService & HasCarAPIService
    var dependencies: Dependencies?
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
    }
    
    func getInstance(auxiliarFactory: AuxiliarFactory) -> UIViewController{
        switch auxiliarFactory {
        case .scan(let booking):
            let scanVC = ScanVC.getInstance(storyBoardName: "Scan")
            let viewModel = ScanVM(dependencies: dependencies!, booking: booking)
            scanVC.viewModel = viewModel
            scanVC.modalPresentationStyle = .overFullScreen
            return scanVC
        case .change(let poso):
            let changeVC = ChangeVC.getInstance(storyBoardName: "Change")
            let viewModel = ChangeVM(dependencies: dependencies!, poso: poso)
            changeVC.viewModel = viewModel
            changeVC.modalPresentationStyle = .overFullScreen
            return changeVC
        case .parkVehicle(let booking, let mode):
            let parkVehicleVC = ParkVehicleVC.getInstance(storyBoardName: "ParkVehicle")
            parkVehicleVC.modalPresentationStyle = .overFullScreen
            let viewModel = ParkVehicleVM(dependencies: dependencies!, booking: booking, mode: mode)
            parkVehicleVC.viewModel = viewModel
            return parkVehicleVC
        case .time(let booking):
            let timeVC = TimeVC.getInstance(storyBoardName: "Time")
            timeVC.modalPresentationStyle = .overFullScreen
            let viewModel = TimeVM(dependencies: dependencies!, booking: booking)
            timeVC.viewModel = viewModel
            return timeVC
        case .pay(let booking):
            let payVC = PayVC.getInstance(storyBoardName: "Pay")
            let viewModel = PayVM(dependencies: dependencies!, booking: booking)
            payVC.viewModel = viewModel
            return payVC
        case .genericSelection(let configurator):
            let genericSelection = GenericSelectionVC.getInstance(storyBoardName: "GenericSelection")
            let viewModel = GenericSelectionVM(dependencies: dependencies!, configurator: configurator)
            genericSelection.viewModel = viewModel
            return genericSelection
        case .ticket(let booking):
            let ticketTVC = TicketTVC.getInstance(storyBoardName: "Ticket")
            if let booking = booking{
                ticketTVC.viewModel = TicketVM(dependencies: dependencies!, mode: .edit(booking))
            }else{
                ticketTVC.viewModel = TicketVM(dependencies: dependencies!, mode: .create)
            }
            return ticketTVC
        case .projection:
            let projectionVC = ProjectionVC.getInstance(storyBoardName: "Projection")
//            if let booking = booking{
//                ticketTVC.viewModel = TicketVM(dependencies: dependencies!, mode: .edit(booking))
//            }else{
//                ticketTVC.viewModel = TicketVM(dependencies: dependencies!, mode: .create)
//            }
            return projectionVC
        }
    }
}
