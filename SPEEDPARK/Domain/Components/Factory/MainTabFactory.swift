//
//  VCFactory.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 4/29/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

class MainTabFactory: HasDependencies{
    
    typealias Dependencies = HasDataService & HasUserDefaultService & HasAuthorizableService & HasUser & HasMessageService & HasSoundService
    var dependencies: Dependencies?
    
    init(dependencies: Dependencies){
        self.dependencies = dependencies
    }
    
    enum MainTab{
        case openTicket
        case reservation
        case new
        case active
        case profile
    }
    
    func getNVC(mainTab: MainTab) -> (nav: UINavigationController, vc: UIViewController){
        switch mainTab {
        case .openTicket:
            return (openTicketsNVC, openTicketVC)
        case .reservation:
            return (reservationsNVC, reservationVC)
        case .new:
            return (newNVC, newVC)
        case .active:
            return (activeNVC, activeVC)
        case .profile:
            return (profileNVC, profileVC)
        }
    }
    
    func push(mainTab: MainTab, delegate: UINavigationControllerDelegate){
        switch mainTab {
        case .openTicket:
            openTicketsNVC.pushViewController(openTicketVC, animated: false)
            openTicketsNVC.delegate = delegate
        case .reservation:
            reservationsNVC.pushViewController(reservationVC, animated: false)
            reservationsNVC.delegate = delegate
        case .new:
            newNVC.pushViewController(newVC, animated: false)
            newNVC.delegate = delegate
        case .active:
            activeNVC.pushViewController(activeVC, animated: false)
            activeNVC.delegate = delegate
        case .profile:
            profileNVC.pushViewController(profileVC, animated: false)
            profileNVC.delegate = delegate
        }
    }
    
    private var  profileNVC: UINavigationController = {
        let profileNVC = UINavigationController()
        profileNVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        profileNVC.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        profileNVC.navigationBar.prefersLargeTitles = true
       return profileNVC
    }()
    
    private var profileVC: ProfileVC = {
        let profileVC = ProfileVC.getInstance(storyBoardName: "Profile")
        profileVC.tabBarItem = UITabBarItem(title: "Profile", image: UIImage(named: "main_profile"), selectedImage: UIImage(named: "main_profile"))
        return profileVC
    }()
    
    private var openTicketsNVC: UINavigationController = {
        let openTicketsNVC = UINavigationController()
        openTicketsNVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        openTicketsNVC.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        openTicketsNVC.navigationBar.prefersLargeTitles = true
        return openTicketsNVC
    }()
    
    private var openTicketVC: BookingVC = {
        let openTicketVC = BookingVC.getInstance(storyBoardName: "Booking")
        openTicketVC.tabBarItem = UITabBarItem(title: "Issued", image: UIImage(named: "main_tab_issued"), selectedImage: UIImage(named: "main_tab_issued"))
        return openTicketVC
    }()
    
    private var reservationsNVC: UINavigationController = {
        let reservationsNVC = UINavigationController()
        reservationsNVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        reservationsNVC.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        reservationsNVC.navigationBar.prefersLargeTitles = true
        return reservationsNVC
    }()
    
    private var reservationVC: BookingVC = {
        let reservationVC = BookingVC.getInstance(storyBoardName: "Booking")
        reservationVC.tabBarItem = UITabBarItem(title: "Reservations", image: UIImage(named: "main_tab_reservation"), selectedImage: UIImage(named: "main_tab_reservation"))
        return reservationVC
    }()
    
    private var activeNVC: UINavigationController = {
        let activeNVC = UINavigationController()
        activeNVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        activeNVC.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        activeNVC.navigationBar.prefersLargeTitles = true
        return activeNVC
    }()
    
    private var activeVC: BookingVC = {
        let activeVC = BookingVC.getInstance(storyBoardName: "Booking")
        activeVC.tabBarItem = UITabBarItem(title: "Active", image: UIImage(named: "main_tab_active"), selectedImage: UIImage(named: "main_tab_active"))
        return activeVC
    }()
    
    private var newNVC: UINavigationController = {
        let newNVC = UINavigationController()
        newNVC.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        newNVC.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor : #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)]
        newNVC.navigationBar.prefersLargeTitles = true
        return newNVC
    }()
    
    private var newVC: BookingVC = {
        let newVC = BookingVC.getInstance(storyBoardName: "Booking")
        newVC.tabBarItem = UITabBarItem(title: "New", image: UIImage(named: "main_tab_new"), selectedImage: UIImage(named: "main_tab_new"))
        return newVC
    }()
    
}

extension MainTabFactory{
    
    func initReservationsVC() {
        let configurator = BookingVCConfigurator(dependencies: dependencies!, title: "Reservations", bookingType: .reservations, sortBy: nil, filterBy: .todayResevation)
        let viewModel = BookingVM(dependencies: dependencies!,configurator: configurator)
        reservationVC.viewModel = viewModel
        reservationVC.title = configurator.title
    }
    
    func initActiveVC() {
        let configurator = BookingVCConfigurator(dependencies: dependencies!, title: "Active", bookingType: .active)
        let viewModel = BookingVM(dependencies: dependencies!,configurator: configurator)
        activeVC.viewModel = viewModel
        activeVC.title = configurator.title
    }
    
    func initNewVC() {
        let configurator = BookingVCConfigurator(dependencies: dependencies!, title: "New", bookingType: .new)
        let viewModel = BookingVM(dependencies: dependencies!,configurator: configurator)
        newVC.viewModel = viewModel
        newVC.title = configurator.title
    }
    
    func initOpenTicketsVC() {
        let configurator = BookingVCConfigurator(dependencies: dependencies!, title: "Issued", bookingType: .tickets, sortBy: .ticketNumberDesc)
        let viewModel = BookingVM(dependencies: dependencies!,configurator: configurator)
        openTicketVC.viewModel = viewModel
        openTicketVC.title = configurator.title
    }
    
    
}
