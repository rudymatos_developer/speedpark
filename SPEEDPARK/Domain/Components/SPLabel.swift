//
//  SPLabel.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/29/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable
class SPLabel: UILabel{
    
    @IBInspectable
    var required: Bool = false
    
    var completion : (() -> Void)?
    
    func bind(completion: @escaping (() -> Void)){
        self.completion = completion
    }
    
    func set(text: String){
        guard text.trimmingCharacters(in: .whitespaces) != "" else{
            self.textColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
            self.text = required ? "REQUIRED" : "OPTIONAL"
            return
        }
        self.textColor = .black
        self.text = text
        completion?()
    }
    
}
