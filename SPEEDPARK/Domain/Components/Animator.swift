//
//  Animator.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

protocol Animator: class{
    func shake(view: UIView)
}

extension Animator{
    func shake(view: UIView){
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.07
        animation.repeatCount = 4
        animation.autoreverses = true
        animation.fromValue = CGPoint(x: view.center.x, y: view.center.y)
        animation.toValue = CGPoint(x: view.center.x + 10, y: view.center.y)
        view.layer.add(animation, forKey: "position")
    }
}

