//
//  SpeedParkError.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

struct SpeedParkError: Error{
    
    var message: String
    var type: ErrorType

    enum ErrorType{
        case loginError
        case logoutError
        case errorGettingBookingInfo
        case errorGettingLocations
        case errorGettingParkSpots
        case errorSendingSMS
        case errorGettingUsers
        case errorGettingCarMakers
        case errorGettingCarModels
    }

}

