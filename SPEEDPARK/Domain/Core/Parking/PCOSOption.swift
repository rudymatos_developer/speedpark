//
//  PCOSOption.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/4/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class PCOSOption{
    
    var title: String
    var icon: String = ""
    var selected: Bool
    var type: PCOSType
    var object: Any?
    
    init(title: String, selected: Bool, type: PCOSType, object: Any?){
        self.title = title
        self.selected = selected
        self.type = type
        self.icon = getIcon()
        self.object = object
    }
    
    private func getIcon() -> String{
        switch type {
        case .location:
            return "parking_car_location"
        case .spot:
            return "parking_car_spot"
        case .valet:
            return "parking_car_valet"
        case .other:
            return ""
        }
    }
}


