//
//  ParkingVehicleCategory.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/4/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class ParkVehicleCategory{
    
    var name: String
    var selected: Bool
    var type: PCOSType
    
    private init(name: String, selected: Bool, type: PCOSType){
        self.name = name
        self.selected = selected
        self.type = type
    }
    
    
    static func getIndexBy(type: PCOSType) -> Int?{
        return getCategories().firstIndex(where: {$0.type == type})
    }
    
    static func getCategory(byIndex: Int) -> ParkVehicleCategory?{
        let categories = getCategories()
        guard byIndex <= categories.count - 1 else {return nil}
        return categories[byIndex]
    }
    
    static func getCategories() -> [ParkVehicleCategory]{
        let locationSection = ParkVehicleCategory(name: "Location", selected: true, type: .location)
        let spotSection = ParkVehicleCategory(name: "Spot", selected: false, type:.spot)
        let parkedBySection = ParkVehicleCategory(name: "Parked By", selected: false, type: .valet)
        let otherSection = ParkVehicleCategory(name: "Notes", selected: false, type: .other)
        return [locationSection,spotSection, parkedBySection, otherSection]
    }
}
