//
//  PCOSType.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/4/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

enum PCOSType{
    case location
    case spot
    case valet
    case other
}
