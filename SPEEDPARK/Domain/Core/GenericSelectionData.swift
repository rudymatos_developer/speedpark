//
//  GenericSelectionData.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 7/14/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class GenericSelectionData: NSObject{
    
    var displayName: String
    var info: String
    var additionalInfo: String
    var selected: Bool
    var type: GenericSelectionVCConfigurator.GenericSelectionType
    
    init(displayName: String, info: String, additionalInfo: String, selected: Bool, type: GenericSelectionVCConfigurator.GenericSelectionType){
        self.displayName = displayName
        self.info = info
        self.additionalInfo = additionalInfo
        self.selected = selected
        self.type = type
    }
    
    override var description: String{
        return "\(displayName) - \(info) - \(additionalInfo) - \(selected) - \(type)"
    }
    
    func toggleSelected(){
        selected.toggle()
    }
    
    func contains(_ string: String) -> Bool{
        let uppercased = string.uppercased()
        return displayName.uppercased().contains(uppercased) || info.uppercased().contains(uppercased) || additionalInfo.uppercased().contains(uppercased)
    }
    
    func set(type: GenericSelectionVCConfigurator.GenericSelectionType){
        self.type = type
    }
    
    static func ==(lhs: GenericSelectionData, rhs: GenericSelectionData) -> Bool{
        return lhs.displayName == rhs.displayName && lhs.info == rhs.info && lhs.additionalInfo == rhs.additionalInfo && lhs.selected == rhs.selected && lhs.type == rhs.type
        
    }
    
    
}
