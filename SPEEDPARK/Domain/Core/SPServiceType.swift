//
//  SPServiceType.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/21/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

enum SPServiceType: String,  CaseIterable{

    case noSelection = "Select One..."
    case speedPark = "SpeedPark"
    case ecoPark = "EcoPark"
    case selfPark = "SelfPark"
    case spotHero = "SpotHero"
    case oversized = "Oversized"

    
    init(displayName: String){
        switch displayName{
        case "Speed Park":
            self = .speedPark
        case "Eco Park":
            self = .ecoPark
        case "Self Park":
            self = .selfPark
        case "Spot Hero":
            self = .spotHero
        case "Oversized":
            self = .oversized
        default:
            self = .noSelection
        }
    }
    
    func getTitle() -> String{
        switch self {
        case .speedPark:
            return "Speed Park"
        case .ecoPark:
            return "Eco Park"
        case .selfPark:
            return "Self Park"
        case .spotHero:
            return "Spot Hero"
        default:
            return self.rawValue
        }
    }
    
    func getMyIndex() -> Int?{
        return AllCases().firstIndex(of: self)
    }
    
    func getRate() -> Double{
        switch self {
        case .speedPark:
            return 14
        case .ecoPark:
            return 12
        case .selfPark:
            return 10
        case .spotHero, .oversized:
            return 18
        case .noSelection:
            return 0
        }
    }
    
    func getDisplayRate() -> String{
        return String(format: "$%.02f", self.getRate())
    }
    
}

