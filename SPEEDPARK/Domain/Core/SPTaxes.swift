//
//  SPTaxes.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/26/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

enum SPTaxes: Equatable{
   
    case none
    case zero
    case ten
    case eleven
    case twelve
    case thirteen
    case custom(Double)
    
    init(value: String){
        switch value{
        case "0%":
            self = .zero
        case "10%":
            self = .ten
        case "11%":
            self = .eleven
        case "12%":
            self = .twelve
        case "13%":
            self = .thirteen
        default:
            self = .none
        }
    }
    
    func getPercentage(fromValue: Double) -> Double{
        switch self {
        case .none, .zero:
            return 0
        case .ten:
            return fromValue * 0.10
        case .eleven:
            return fromValue * 0.11
        case .twelve:
            return fromValue * 0.12
        case .thirteen:
            return fromValue * 0.13
        case .custom(let amount):
            return fromValue * amount
        }
    }
    
    
    func getDisplayName() -> String{
        switch self {
        case .none:
            return ""
        case .zero:
            return "0%"
        case .ten:
            return "10%"
        case .eleven:
            return "11%"
        case .twelve:
            return "12%"
        case .thirteen:
            return "13%"
        case .custom(let amount):
            return "\(amount)%"
        }
    }
    
}
