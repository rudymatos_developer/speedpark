//
//  File.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

enum SpeedParkConstants{
    
    enum Fields{
        enum Booking{
            static let bookingNo = "bookingNo"
            static let bookingStatus = "bookingStatus"
            static let notes = "bookingNotes"
            static let isSwapable = "isSwapable"
            static let ticketNumber = "ticketNo"
            static let checkedInOrParkedStatus = "checkedInOrParked"
            static let scanDate = "scanDate"
            static let activeDate = "activeDate"
        }
        
        enum Payment{
            static let paid = "paid"
            static let prePaid = "prePaid"
            static let serviceType = "serviceType"
            static let taxRate = "taxRate"
            static let dailyRate = "dailyRate"
            static let paidDate = "paidDate"
            static let prePaidDate = "prePaidDate"
        }
        
        enum Customer{
            static let firstName = "firstName"
            static let lastName = "lastName"
            static let emailAddress = "emailAddress"
            static let waitingTime = "waitingTime"
            static let waitingTimeDate = "waitingTimeDate"
            static let phone = "customerPhone"
            static let incomingPhoneNumber = "incomingPhoneNumber"
        }
        
        enum Parking{
            static let location = "location"
            static let siteLocation = "siteLocation"
            static let parkingSpot = "parkingSpot"
            static let parkingSpotId = "parkingSpotId"
            static let valet = "valet"
            static let valetId = "valetId"
            static let parkedDate = "parkedDate"
            static let keyStatus = "keyStatus"
            static let enterDate = "enterDate"
            static let enterTime = "enterTime"
            static let exitDate = "exitDate"
            static let exitTime = "exitTime"
        }
        
        enum Flight{
            static let airline = "airline"
            static let arrivingFrom = "arrivingFrom"
            static let flightNumber = "flightNo"
            static let arrivingDate = "exitDate"
            
        }
        
        enum Vehicle{
            static let maker = "carMakes"
            static let model = "carModels"
            static let year = "carYears"
            static let plate = "plateNo"
            static let color = "vehicleColor"
            static let photos = "photos"
        }
        
        enum Location{
            static let name = "name"
            static let displayName = "displayName"
            static let image = "image"
            static let company = "company"
            static let isDefault = "isDefault"
            static let order = "order"
            
            enum ParkingSpot{
                static let child = "child"
                static let parent = "parent"
                static let company = "company"
                static let isAvailable = "isAvailable"
                static let location = "location"
                static let isReusable = "isReusable"
                static let bookingId = "bookingId"
                static let name = "name"
                static let returningDate = "returningDate"
            }
            
        }
        
        enum User{
            static let email = "email"
            static let loggedIn = "loggedIn"
            static let company = "company"
            static let isAdmin = "isAdmin"
            static let displayName = "displayName"
            static let preferredLocationDocumentId = "preferredLocationDocumentId"
        }
    }
    
    enum MessagingURLs{
        static let sendSMSURL = "https://speedpark-sms-notification.herokuapp.com/sms/checkin"
        static let rejectBookingSMSURL = "https://speedpark-sms-notification.herokuapp.com/sms/bookingRejection"
        static let approximateTime = "https://speedpark-sms-notification.herokuapp.com/sms/approximateTime"
        static let sendSMSURLLocal = "http://Rudys-MacBook-Pro.local:3000/sms/checkin"
    }
    
    enum Stripe{
        static let publicKey = "pk_test_bAdbLk6F20FpJSNQoW4cr7az00dvR9wl14"
    }
    
    enum Color{
        static let main = "palette_main_blue_color"
        static let disabled = "palette_disabled_color"
    }
    
    enum Collections{
        static let bookings = "bookings"
        static let locations = "locations"
        static let parkingSpot = "parkingSpots"
        static let users = "users"
        static let global = "global"
    }
    
}
