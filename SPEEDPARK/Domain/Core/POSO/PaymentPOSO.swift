//
//  PaymentPOSO.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/21/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class PaymentPOSO: NSObject{
    @objc dynamic var serviceType: String = SPServiceType.noSelection.getTitle()
    @objc dynamic var tipPercentage: String = ""
    var currentTotalWithNoTips: Double = 0.0
    
    var tip: SPTip = .none {
        didSet{
            tipPercentage = tip.getDisplayName()
        }
    }

    func isValid() -> Bool{
        return serviceType != SPServiceType.noSelection.getTitle() && tip != .none
    }
}
