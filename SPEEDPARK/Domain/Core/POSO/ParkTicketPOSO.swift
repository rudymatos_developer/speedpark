//
//  ParkTicketPOSO.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/4/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class ParkTicketPOSO: NSObject{
    
    @objc dynamic var location: Location?
    @objc dynamic var spot: Location.ParkingSpot?
    @objc dynamic var parkedBy: SPUser?
    var keyStatus: SPBooking.Parking.KeyStatus = .keysNotCheckedIn
    var internalComments: String = ""
    
    func isValid() -> Bool{
        guard location != nil, spot != nil, parkedBy != nil else {
            return false
        }
        return true
    }
    
    override init(){
        super.init()
    }
    
    override var description: String {
        return "\(String(describing: location)) \(String(describing: spot)) \(String(describing: parkedBy)) \(keyStatus.rawValue) \(internalComments)"
    }
    
}

