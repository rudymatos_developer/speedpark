//
//  GenericSelectionPOSO.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 6/21/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

class GenericSelectionPOSO: NSObject{
    
    @objc dynamic var selection: GenericSelectionData?
    
    override init() {
        super.init()
    }
    
    func isValid() -> Bool{
        guard selection != nil else{
            return false
        }
        return true
    }
    
    override var description: String{
        return selection?.description ?? "Invalid Selection"
    }
    
}
