//
//  SpeedParkUser.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import Firebase

@objc class SPUser: NSObject{
    var email : String
    var company: String
    var displayName: String
    var locations: [Location]?
    var loggedInDate: Date?
    var isAdmin = false
    var defaultLocation : Location?
    
    init?(email: String, data: [String:Any]){
        guard let company = data[SpeedParkConstants.Fields.User.company ] as? String, let displayName = data[SpeedParkConstants.Fields.User.displayName ] as? String  else{ return nil}
        self.loggedInDate = (data["login-date"] as? Timestamp)?.dateValue()
        self.isAdmin = data[SpeedParkConstants.Fields.User.isAdmin] as? Bool ?? false
        self.company = company
        self.displayName = displayName
        self.email = email
    }
}
