//
//  Location.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/3/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import Firebase

struct ParkingSpotAvailability{
    var date: Date
    var available: [Location.ParkingSpot]
    var unavailable: [Location.ParkingSpot]
}

@objc class Location: NSObject{
    
    var documentId: String
    var name: String
    var displayName: String
    var imageURL: URL?
    var company : String
    var order: Int
    var isDefault: Bool
    
    var parkingSpots : [ParkingSpot]?
    
    init?(documentId: String, data: [String:Any]){
        guard let name = data[SpeedParkConstants.Fields.Location.name] as? String ,
            let displayName = data[SpeedParkConstants.Fields.Location.displayName] as? String,
            let company = data[SpeedParkConstants.Fields.Location.company] as? String,
            let order = data[SpeedParkConstants.Fields.Location.order] as? Int else {return nil}
        
        self.documentId = documentId
        self.name = name
        self.displayName = displayName
        self.company = company
        self.order = order
        self.isDefault = data[SpeedParkConstants.Fields.Location.isDefault] as? Bool ?? false
        if let imageString = data[SpeedParkConstants.Fields.Location.image] as? String{
            self.imageURL = URL(string:  imageString)
        }
    }
    
    
    @objc class ParkingSpot: NSObject{
        
        var documentId: String
        var bookingId: String?
        var child: ParkingSpot?
        var parent: ParkingSpot?
        var company: String
        var isAvailable: Bool
        var isReusable: Bool
        var location: String
        var name: String
        var returningDate: Date?
        
        override var description: String{
            return "Document Id: \(documentId) - Name: \(name) - Parent: \(String(describing: parent?.documentId)) - Child: \(String(describing: child?.documentId)) - isAvailable: \(isAvailable) - returningDate: \(String(describing: returningDate)) \n"
        }
        
        init(withDocumentId: String){
            self.documentId = withDocumentId
            self.company = ""
            self.isAvailable = false
            self.isReusable = false
            self.location = ""
            self.name = ""
        }
        
        init(documentId: String, name: String, location: String){
            self.location = location
            self.documentId = documentId
            self.name = name
            self.isAvailable = true
            self.company = "speedpark_llc"
            self.isReusable = false
        }
        
        init?(documentId: String, data: [String:Any]){
            guard let name = data[SpeedParkConstants.Fields.Location.ParkingSpot.name] as? String ,
            let company = data[SpeedParkConstants.Fields.Location.ParkingSpot.company] as? String,
            let isAvailable = data[SpeedParkConstants.Fields.Location.ParkingSpot.isAvailable] as? Bool,
            let location = data[SpeedParkConstants.Fields.Location.ParkingSpot.location] as? String else {return nil}
            self.documentId = documentId
            self.name = name
            self.company = company
            self.isAvailable = isAvailable
            self.isReusable = data[SpeedParkConstants.Fields.Location.ParkingSpot.isReusable] as? Bool ?? false
            self.bookingId = data[SpeedParkConstants.Fields.Location.ParkingSpot.bookingId] as? String
            if let childDocumentId = data[SpeedParkConstants.Fields.Location.ParkingSpot.child] as? String{
                self.child = ParkingSpot(withDocumentId: childDocumentId)
            }
            if let parentDocumentId = data[SpeedParkConstants.Fields.Location.ParkingSpot.parent] as? String{
                self.parent = ParkingSpot(withDocumentId: parentDocumentId)
            }
            self.location = location
            self.returningDate = (data[SpeedParkConstants.Fields.Location.ParkingSpot.returningDate] as? Timestamp)?.dateValue()
        }

        enum Filter: Int{
            case all = 0
            case available = 1
        }
        
        func getStatus(byBookingReturningDate bookingReturningDate: Date? = nil) -> Status{
            if let parent = parent, !parent.isAvailable, let parentSpotReturningDate = parent.returningDate, let bookingReturningDate = bookingReturningDate{
                if isAvailable{
                    return parentSpotReturningDate > bookingReturningDate ? .available : .swapable
                }
                return .unavailable
            }else{
                return isAvailable ? .available : .unavailable
            }
        }
        
        func parse() -> [String: Any]{
            return ["child":child?.documentId ?? "", "company":"speedpark_llc","isAvailable":true,"location":"\(location)","name":"\(name)","parent":parent?.documentId ?? "","returningDate":"", "isReusable": isReusable]
        }
        
        enum Status: String{
            case available = "Available"
            case unavailable = "Unavailable"
            case swapable = "Swapable"
            
            func getColor() -> UIColor?{
                switch self {
                case .available:
                    return UIColor.init(named: SpeedParkConstants.Color.main)
                case .unavailable:
                    return UIColor.init(named: "palette_red")
                case .swapable:
                    return UIColor.init(named: "main_yellow")
                }
            }
        }
        
    }
    
    
}
