//
//  SPTips.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/23/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

enum SPTip: Equatable{
    case none
    case five
    case ten
    case fifteen
    case twenty
    case custom(Double)

    
    init(value: String){
        switch value{
        case "5%":
            self = .five
        case "10%":
            self = .ten
        case "15%":
            self = .fifteen
        case "20%":
            self = .twenty
        default:
            self = .none
        }
    }
    
    func isPercentage() -> Bool{
        return self == .five || self == .ten || self == .fifteen || self == .twenty
    }
    
    func getPercentage(fromValue: Double) -> Double{
        switch self {
        case .none:
            return 0
        case .five:
            return fromValue * 0.05
        case .ten:
            return fromValue * 0.10
        case .fifteen:
            return fromValue * 0.15
        case .twenty:
            return fromValue * 0.20
        case .custom(let amount):
            return amount
        }
    }
    
    func getDisplayName() -> String{
        switch self {
        case .none:
            return ""
        case .five:
            return "5%"
        case .ten:
            return "10%"
        case .fifteen:
            return "15%"
        case .twenty:
            return "20%"
        case .custom(let amount):
            return String(format: "$%.02f", amount)
        }
    }
    
}

