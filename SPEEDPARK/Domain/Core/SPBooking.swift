//
//  SPBooking.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit
import Firebase

class SPBooking: CustomStringConvertible, SPDateFormatter{
    
    var firebaseDocumentId: String?
    var identifier: Int
    var ticketNumber: String
    var status: Status
    var flight: Flight
    var customer: Customer
    var vehicle : Vehicle
    var payment: Payment
    var parking: Parking
    var notes: String
    var scanDate: Date?
    var isSwapable: Bool = false
    var activeDate: Date?
    
    var description: String{
        return "\(String(describing: firebaseDocumentId)) \(identifier) \(ticketNumber) \(isSwapable)"
    }

    var exitDate: Date? {
        return convert(string: parking.exitDate, usingPattern: .speedParkLongFormat)
    }
    
    
    func getBookingPayableStartDate() -> Date?{
        if isThereAdditionalCharges{
            guard let exitDate = exitDate else{
                return nil
            }
            return exitDate
        }
        return scanDate
    }
    
    func getBookingPayableEndDate(mode: PayVM.Mode) -> Date? {
        guard let endDate = convert(string: parking.exitDate, usingPattern: .speedParkLongFormat) else{
            return nil
        }
        if let activeDate = activeDate, activeDate > endDate{
            return activeDate
        }
        if mode == .normal{
            if let totalDays = Calendar.current.dateComponents([.day], from: Date(), to: endDate).day{
                if totalDays >= 1{
                    return Date()
                }
            }
        }
        return endDate
    }
    
    var isThereAdditionalCharges : Bool {
        guard payment.prePaid,let activeDate = activeDate,  let exitDate = exitDate, let totalMinutes = Calendar.current.dateComponents([.minute], from: exitDate, to: activeDate).minute else{
            return false
        }
        print("⏰ total minutes difference: \(totalMinutes)")
        return totalMinutes >= 60
    }
    
    func filterBy(value: String) -> Bool{
        let value = value.uppercased()
        return "\(ticketNumber)".uppercased().contains(value) || customer.name.uppercased().contains(value) || vehicle.description.uppercased().contains(value) || "\(identifier)".uppercased().contains(value) || parking.spot.contains(value)
    }
    
    init(firebaseDocumentId: String?, data: [String:Any]){
        self.firebaseDocumentId = firebaseDocumentId
        self.identifier = Int(data[SpeedParkConstants.Fields.Booking.bookingNo] as? String ?? "") ?? 0
        self.ticketNumber = data[SpeedParkConstants.Fields.Booking.ticketNumber] as? String ?? ""
        let bookingStatus = Status(rawValue: data[SpeedParkConstants.Fields.Booking.bookingStatus] as? String ?? "") ?? .done
        self.notes = data[SpeedParkConstants.Fields.Booking.notes] as? String ?? ""
        self.isSwapable = data[SpeedParkConstants.Fields.Booking.isSwapable] as? Bool ?? false
        self.activeDate = (data[SpeedParkConstants.Fields.Booking.activeDate] as? Timestamp)?.dateValue() ?? nil
        self.status = bookingStatus
        self.flight = Flight(data: data)
        self.customer = Customer(data: data)
        self.parking = Parking(data: data)
        self.payment = Payment(data: data)
        self.vehicle = Vehicle(data: data)
        self.scanDate = (data[SpeedParkConstants.Fields.Booking.scanDate] as? Timestamp)?.dateValue() ?? nil
    }
    
    class Payment{
        var paid: Bool = false
        var paidDate: Date?
        var prePaid: Bool = false
        var prePaidDate: Date?
        var serviceType: SPServiceType
        var taxRate: Double
        var dailyRate: Double
        
        init(data: [String: Any]){
            self.paid = data[SpeedParkConstants.Fields.Payment.paid] as? Bool ?? false
            self.prePaid = data[SpeedParkConstants.Fields.Payment.prePaid] as? Bool ?? false
            self.taxRate = data[SpeedParkConstants.Fields.Payment.taxRate] as? Double ?? 0.0
            let serviceTypeString = data[SpeedParkConstants.Fields.Payment.serviceType] as? String ?? ""
            self.serviceType = .noSelection
            if let serviceType = SPServiceType(rawValue: serviceTypeString){
                self.serviceType = serviceType
            }
            self.dailyRate = data[SpeedParkConstants.Fields.Payment.dailyRate] as? Double ?? 0.0
            self.paidDate = (data[SpeedParkConstants.Fields.Payment.paidDate] as? Timestamp)?.dateValue() ?? nil
            self.prePaidDate = (data[SpeedParkConstants.Fields.Payment.prePaidDate] as? Timestamp)?.dateValue() ?? nil
        }
        
        func getServiceTypeDisplayName() -> String{
            return serviceType.getTitle()
        }
        
    }
    
    class Customer{
        var firstName: String
        var lastName : String
        var name : String{
            return "\(firstName) \(lastName)"
        }
        var email : String
        var phone : String
        var waitingTime: Int
        var waitingTimeDate : Date?
        var incomingPhoneNumber: String?
        
        init(data: [String:Any]){
            self.firstName = data[SpeedParkConstants.Fields.Customer.firstName] as? String ?? ""
            self.lastName = data[SpeedParkConstants.Fields.Customer.lastName] as? String ?? ""
            self.waitingTime = data[SpeedParkConstants.Fields.Customer.waitingTime] as? Int ?? 0
            self.email = data[SpeedParkConstants.Fields.Customer.emailAddress] as? String ?? ""
            self.phone = data[SpeedParkConstants.Fields.Customer.phone] as? String ?? ""
            self.incomingPhoneNumber = data[SpeedParkConstants.Fields.Customer.incomingPhoneNumber] as? String ?? ""
            self.waitingTimeDate = (data[SpeedParkConstants.Fields.Customer.waitingTimeDate] as? Timestamp)?.dateValue() ?? nil
        }
    }
    
    class Flight:SPDateFormatter{
        var airline: String
        var flightNumber: String
        var arrivingFrom: String
        var arrivingDate: Date?
        init(data: [String:Any]){
            self.airline = data[SpeedParkConstants.Fields.Flight.airline] as? String ?? ""
            self.flightNumber = data[SpeedParkConstants.Fields.Flight.flightNumber] as? String ?? ""
            self.arrivingDate = nil
            self.arrivingFrom = data[SpeedParkConstants.Fields.Flight.arrivingFrom] as? String ?? ""
            if let exitDateString = data[SpeedParkConstants.Fields.Flight.arrivingDate] as? String, let exitDate = convert(string: exitDateString, usingPattern: .main) {
                self.arrivingDate = exitDate
            }
        }
    }
    
    class Vehicle: CustomStringConvertible{
        var maker: String
        var model: String
        var year: String
        var photos: [String]
        var color: String
        var plate: String
        
        init(data: [String:Any]){
            self.maker = data[SpeedParkConstants.Fields.Vehicle.maker] as? String ?? ""
            self.model = data[SpeedParkConstants.Fields.Vehicle.model] as? String ?? ""
            self.year = data[SpeedParkConstants.Fields.Vehicle.year] as? String ?? ""
            self.photos = data[SpeedParkConstants.Fields.Vehicle.photos] as? [String] ?? []
            self.color = data[SpeedParkConstants.Fields.Vehicle.color] as? String ?? ""
            self.plate = data[SpeedParkConstants.Fields.Vehicle.plate] as? String ?? ""
        }
        
        var description: String{
            return "\(color) \(maker) \(model) \(year) \(plate)".uppercased().trimmingCharacters(in: .whitespaces)
        }
    }
    
    class Parking: SPDateFormatter{
        
        enum KeyStatus: String{
            case noKeys = "No Keys"
            case keysNotCheckedIn = "keysNotCheckedIn"
            case keysCheckedIn = "keysCheckIn"
            case notSpecified = ""
        }
        
        var location: String
        var siteLocation: String
        var spot: String
        var spotId: String
        var valet: String
        var keyStatus : KeyStatus
        var enterDate: String
        var exitDate: String
        
        var enterDate12HoursFormat: String{
            let date = convert(string: enterDate, usingPattern: .speedParkLongFormat)
            return convert(date: date, usingPattern: .speedParkLongFormat12HoursDisplay)
        }
        
        init(data: [String:Any]){
            self.location = data[SpeedParkConstants.Fields.Parking.location] as? String ?? ""
            self.siteLocation = data[SpeedParkConstants.Fields.Parking.siteLocation] as? String ?? ""
            self.spot = data[SpeedParkConstants.Fields.Parking.parkingSpot] as? String ?? ""
            self.spotId = data[SpeedParkConstants.Fields.Parking.parkingSpotId] as? String ?? ""
            self.valet = data[SpeedParkConstants.Fields.Parking.valet] as? String ?? ""
            self.keyStatus = .notSpecified
            if let keyStatusString = data[SpeedParkConstants.Fields.Parking.keyStatus] as? String, let keyStatus = KeyStatus(rawValue: keyStatusString){
                self.keyStatus = keyStatus
            }
            self.enterDate = ""
            self.exitDate = ""
            
            if let enterDate = data[SpeedParkConstants.Fields.Parking.enterDate] as? String, let enterTime =  data[SpeedParkConstants.Fields.Parking.enterTime] as? String{
                let enterFullDate = "\(enterDate) \(enterTime)"
                self.enterDate = enterFullDate
            }
            if let exitDate =  data[SpeedParkConstants.Fields.Parking.exitDate] as? String, let exitTime =  data[SpeedParkConstants.Fields.Parking.exitTime] as? String{
                let exitFullDate = "\(exitDate) \(exitTime)"
                self.exitDate  = exitFullDate
            }
        }
    }
    
    func parse() -> [String: Any]{
        var dict : [String:Any] =  [
            SpeedParkConstants.Fields.Customer.firstName : customer.firstName,
            SpeedParkConstants.Fields.Customer.lastName : customer.lastName,
            SpeedParkConstants.Fields.Customer.phone : customer.phone,
            SpeedParkConstants.Fields.Customer.emailAddress : customer.email,
            SpeedParkConstants.Fields.Vehicle.maker : vehicle.maker,
            SpeedParkConstants.Fields.Vehicle.model : vehicle.model,
            SpeedParkConstants.Fields.Vehicle.color : vehicle.color,
            SpeedParkConstants.Fields.Vehicle.year : vehicle.year,
            SpeedParkConstants.Fields.Vehicle.plate : vehicle.plate,
            SpeedParkConstants.Fields.Flight.airline : flight.airline,
            SpeedParkConstants.Fields.Flight.flightNumber : flight.flightNumber,
            SpeedParkConstants.Fields.Flight.arrivingFrom : flight.arrivingFrom,
            SpeedParkConstants.Fields.Booking.notes : notes
        ]

        #warning("change this implementation asap")
        let values = parking.exitDate.split(separator: " ")
        if values.count == 2{
            dict[SpeedParkConstants.Fields.Parking.exitDate] =  values[0]
            dict[SpeedParkConstants.Fields.Parking.exitTime] =  values[1]
        }
        return dict
    }
    
    func isValid() -> Bool{
        guard !customer.name.isEmpty, !customer.email.isEmpty, !customer.phone.isEmpty, !vehicle.maker.isEmpty, !vehicle.model.isEmpty, !parking.exitDate.isEmpty else{
            return false
        }
        return true
    }
    
    enum Status: String{
        case booked = "Booked"
        case done = "Done"
        case parked = "Parked"
        case new = "New"
        case active = "Active"
        case checkedIn = "Check-In"
        case noShow  = "noShow"
        
        func getBTNActionTitle() -> String{
            switch self {
            case .booked:
                return "Scan"
            case .checkedIn:
                return "Park"
            case .parked:
                return "Pull"
            case .active:
                return "Done"
            case .new:
                return "Accept"
            default:
                return ""
            }
        }
        
        func getBorderColor() -> UIColor{
            if self == .checkedIn{
                return #colorLiteral(red: 0.2745098174, green: 0.4862745106, blue: 0.1411764771, alpha: 1)
            }
            return #colorLiteral(red: 0.06274510175, green: 0, blue: 0.1921568662, alpha: 1)
        }
        
        func getBTNActionColor() -> UIColor{
            if self == .checkedIn{
                return #colorLiteral(red: 0.01091912389, green: 0.8790634274, blue: 0.005333657376, alpha: 1)
            }
            return #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)
        }
    }
    
    
}

extension SPBooking: Equatable{
    static func == (lhs: SPBooking, rhs: SPBooking) -> Bool {
        return lhs.firebaseDocumentId == rhs.firebaseDocumentId && lhs.ticketNumber == rhs.ticketNumber && lhs.identifier == rhs.identifier
    }
}
