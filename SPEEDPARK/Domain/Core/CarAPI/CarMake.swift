// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let carMake = try? newJSONDecoder().decode(CarMake.self, from: jsonData)

import Foundation

// MARK: - CarMake
class CarMake: Codable {
    let makes: [Make]
    
    enum CodingKeys: String, CodingKey {
        case makes = "Makes"
    }
    
    init(makes: [Make]) {
        self.makes = makes
    }
}

// MARK: - Make
class Make: Codable {
    let makeID, makeDisplay, makeIsCommon, makeCountry: String
    
    enum CodingKeys: String, CodingKey {
        case makeID = "make_id"
        case makeDisplay = "make_display"
        case makeIsCommon = "make_is_common"
        case makeCountry = "make_country"
    }
    
    init(makeID: String, makeDisplay: String, makeIsCommon: String, makeCountry: String) {
        self.makeID = makeID
        self.makeDisplay = makeDisplay
        self.makeIsCommon = makeIsCommon
        self.makeCountry = makeCountry
    }
}
