// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse the JSON, add this file to your project and do:
//
//   let carModel = try? newJSONDecoder().decode(CarModel.self, from: jsonData)

import Foundation

// MARK: - CarModel
class CarModel: Codable {
    let models: [Model]
    
    enum CodingKeys: String, CodingKey {
        case models = "Models"
    }
    
    init(models: [Model]) {
        self.models = models
    }
}

// MARK: - Model
class Model: Codable {
    let modelName: String
    let modelMakeID: String
    
    enum CodingKeys: String, CodingKey {
        case modelName = "model_name"
        case modelMakeID = "model_make_id"
    }
    
    init(modelName: String, modelMakeID: String) {
        self.modelName = modelName
        self.modelMakeID = modelMakeID
    }
}
