//
//  TicketCoordinator.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

class TicketCoordinator: NSObject, Coordinator, HasDependencies, HasRootViewController{
    
    var parent: Coordinator?
    var childCoordinators: [Coordinator] = []
    typealias Dependencies = HasDataService & HasUserDefaultService & HasAuthorizableService & HasUser & HasMessageService & HasSoundService & HasCarAPIService
    
    var dependencies: Dependencies?
    
    var rootViewController : UINavigationController
    private var booking: SPBooking?
    private var auxiliarFactory : AuxiliarVCFactory!
    private var ticketVC : TicketTVC!
    
    private var hasABooking : Bool{
        return booking != nil
    }
    
    init(dependencies: Dependencies, rootViewController: UINavigationController){
        self.dependencies = dependencies
        self.rootViewController = rootViewController
        self.auxiliarFactory = AuxiliarVCFactory(dependencies: dependencies)
        super.init()
        dependencies.carAPIService = CarAPIService()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    convenience init(dependencies: Dependencies, rootViewController: UINavigationController, booking: SPBooking){
        self.init(dependencies: dependencies, rootViewController: rootViewController)
        self.booking = booking
    }
    
    func start() {
        rootViewController.navigationBar.tintColor = .white
        navigate(to: hasABooking ? .editTicket(booking!) : .createTicket)
    }
    
    func stop() {
    }
    
    enum Destination{
        case createTicket
        case editTicket(SPBooking)
        case parkVehicle
        case genericSelection(GenericSelectionVCConfigurator)
    }
    
    deinit {
        dependencies?.carAPIService = nil
        print("💀 Removing \(self.classForCoder.description())")
    }
}

extension TicketCoordinator: Navigator{
    func navigate(to: TicketCoordinator.Destination) {
        switch to {
        case .createTicket:
            displayTicketTV(mode: .create)
        case .editTicket(let ticket):
            displayTicketTV(mode: .edit(ticket))
        case .parkVehicle:
            displayParkVehicle()
        case .genericSelection(let configurator):
            displayGenericSelection(with: configurator)
        }
    }
}

extension TicketCoordinator{
    
    private func displayGenericSelection(with configurator: GenericSelectionVCConfigurator){
        guard let genericSelectionVC = auxiliarFactory.getInstance(auxiliarFactory: .genericSelection(configurator)) as? GenericSelectionVC else{
            return
        }
        genericSelectionVC.delegate = ticketVC
        present(viewController: genericSelectionVC, presentationStyle: .modally(true))
    }
    
    private func displayParkVehicle(){
        guard let booking = booking, let parking = auxiliarFactory.getInstance(auxiliarFactory: .parkVehicle(booking,.repark)) as? ParkVehicleVC  else {
            return
        }

        parking.viewModel.parkCompletion = { [weak self] booking in
            guard let self = self else {return}
            self.ticketVC.viewModel.booking = booking
            self.ticketVC.assignParkingInformation()
        }
        
        present(viewController: parking, presentationStyle: .present)
    }
    
    private func displayTicketTV(mode: TicketVM.Mode){
        ticketVC = auxiliarFactory.getInstance(auxiliarFactory: .ticket(booking)) as? TicketTVC
        ticketVC.viewModel.reparkVehicleCompletion = { [weak self] in
            self?.navigate(to: .parkVehicle)
        }
        ticketVC.viewModel.genericSelectionCompletion = {[weak self] configurator in
            self?.navigate(to: .genericSelection(configurator))
        }
        present(viewController: ticketVC, presentationStyle: .push(true))
    }
}
