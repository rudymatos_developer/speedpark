//
//  AppCoordinator.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseMessaging
import IQKeyboardManagerSwift

class AppCoordinator: NSObject, Coordinator, HasRootViewController{
    
    var parent: Coordinator?
    var childCoordinators: [Coordinator]
    var rootViewController = UINavigationController()
    private var window : UIWindow
    
    private var appDependencies: AppDependencies?
    
    init(window: UIWindow){
        self.window = window
        childCoordinators = []
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()
        configureInitialView()
    }
    
    func start() {
        registerApplicationAPIs()
        registerAppDependencies()
        registerForNotifications()
        loadUserInformation()
    }
    
    func stop() {
        rootViewController = UINavigationController()
        window.rootViewController = rootViewController
        removeChild(coordinator: &mainCoordinator)
        childCoordinators = []
        self.navigate(to: .login)
    }
    
    private var emptyVC : UIViewController = {
        let viewController = UIViewController()
        viewController.view.backgroundColor = #colorLiteral(red: 0.9759238362, green: 0.9766622186, blue: 0.9760381579, alpha: 1)
        return viewController
    }()
    
    private func configureInitialView(){
        let loadingScreen = LoadingScreen(frame: UIApplication.shared.keyWindow!.frame)
        let viewController = emptyVC
        viewController.view.addSubview(loadingScreen)
        rootViewController.navigationBar.isHidden = true
        rootViewController.pushViewController(viewController, animated: false)
    }
    
    internal var mainCoordinator: MainCoordinator!
    
    enum Destination{
        case login
        case main
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
    
}

extension AppCoordinator{
    
    private func registerApplicationAPIs(){
        FirebaseApp.configure()
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        Messaging.messaging().delegate = self
    }
    
    private func registerAppDependencies(){

        let maintenance = ParkingMaintenance()
        maintenance.updateNotifications()
        
        let userDefaultService = UserDefaultService()
        let dataService = DataService()
        let messageService = MessageService()
        appDependencies = AppDependencies(userDefaultService: userDefaultService, dataService: dataService, messageService: messageService)
        let authorizableService  = AuthorizableService(dependencies: appDependencies!)
        let soundService = SoundService(dependencies: appDependencies!)
        appDependencies?.authorizableService = authorizableService
        appDependencies?.soundService = soundService
    }
    
    private func loadUserInformation(){
        guard let email = appDependencies?.userDefaultService.getCurrentLoggedInUser(), appDependencies?.authorizableService?.isUserLoggedIn() ?? false else{
            navigate(to: .login)
            return
        }
        appDependencies?.dataService.getUserInfo(email: email, completion: { [weak self] result in
            self?.rootViewController.popToRootViewController(animated: false)
            switch result{
            case .success(let user):
                self?.appDependencies?.user = user
                self?.navigate(to: .main)
            default:
                self?.navigate(to: .login)
            }
        })
    }
}


extension AppCoordinator: Navigator{
    func navigate(to: AppCoordinator.Destination) {
        switch to {
        case .login:
            presentLoginVC()
        case .main:
            initMainCoordinator()
        }
    }
}


extension AppCoordinator{
    
    private func presentLoginVC(){
        let loginViewController = LoginVC.getInstance(storyBoardName: "Login")
        let viewModel = LoginVM(dependencies: appDependencies!)
        loginViewController.viewModel = viewModel
        viewModel.loginCompletion = { [weak self] result in
            switch result {
            case .success:
                self?.navigate(to: .main)
            default:
                print("There was an error. Coordinator won't do much")
            }
        }
        rootViewController.navigationBar.isHidden = true
        present(viewController: loginViewController, presentationStyle: .crossDisolved)
    }
    
    private func initMainCoordinator(){
        let tabBarVC = UITabBarController()
        rootViewController = UINavigationController()
        mainCoordinator = MainCoordinator(dependencies: appDependencies!, rootViewController: tabBarVC)
        mainCoordinator.start()
        mainCoordinator.parent = self
        addChild(coordinator: mainCoordinator)
        window.rootViewController = tabBarVC
    }
    
}

extension AppCoordinator: MessagingDelegate{
    
    private func registerForNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted, _) in
            guard granted else {return}
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { (settings) in
                guard settings.authorizationStatus == .authorized else {return}
                DispatchQueue.main.async {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            })
        }
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("device fcmtoken: \(fcmToken)")
        appDependencies?.userDefaultService.set(fcmToken)
    }
    
    func handleReceivedNotification(userInfo: [AnyHashable: Any]){
        
        print(#function)
        print(userInfo)

        enum NotificationType: String{
            case removeAllAlarms = "removeAllAlarms"
            case newBooking = "newBooking"
            case keysCheckedIn = "keysCheckedIn"
        }

        guard let type = userInfo["type"] as? String,  let notificationType = NotificationType(rawValue: type) else{
            return
        }

        print(notificationType.rawValue)
        switch notificationType{
        case .removeAllAlarms:
            appDependencies?.soundService?.stopCurrentSound()
        case .keysCheckedIn:
            appDependencies?.soundService?.loadAndPlay(sound: .alarm5, shouldOverrideSound: true)
        case .newBooking:
            appDependencies?.soundService?.loadAndPlay(sound: .alarm2, shouldOverrideSound: true)
        }
    }
    
}

