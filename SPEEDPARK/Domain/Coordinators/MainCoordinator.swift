//
//  MainCoordinator.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator: NSObject, Coordinator, HasRootViewController, HasDependencies{

    weak var parent: Coordinator?
    var childCoordinators: [Coordinator] = []
    typealias Dependencies = HasDataService & HasUserDefaultService & HasAuthorizableService & HasUser & HasMessageService & HasSoundService & HasCarAPIService
    var dependencies: Dependencies?
    
    var rootViewController : UITabBarController
    
    private let mainTabFactory : MainTabFactory!
    private let auxiliarVCFactory : AuxiliarVCFactory!
    private var ticketCoordinator: TicketCoordinator?
    private var projectionCoordinator: ProjectionCoordinator?
    
    init(dependencies: Dependencies, rootViewController: UITabBarController){
        self.dependencies = dependencies
        self.rootViewController = rootViewController
        self.auxiliarVCFactory = AuxiliarVCFactory(dependencies: dependencies)
        self.mainTabFactory = MainTabFactory(dependencies: dependencies)
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    func start() {
        configureTabBar()
        initializeViewsCoordinators()
    }
    
    func stop() {
    }
    
    enum Destination{
        case assignTicketNumber(SPBooking)
        case acceptBooking(SPBooking)
        case parkVehile(SPBooking)
        case viewTicket(SPBooking, UINavigationController)
        case payTicket(BookingVCConfigurator, SPBooking)
        case change(BookingVCConfigurator, ChangePOSO)
        case createNewReservation
        case presentProjection([ParkingSpotAvailability])
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
}

extension MainCoordinator: Navigator{
    func navigate(to: MainCoordinator.Destination) {
        switch to {
        case .assignTicketNumber(let booking):
            displayScanVC(withBooking: booking)
        case .parkVehile(let booking):
            displayParkVehicleVC(withBooking: booking)
        case .acceptBooking(let booking):
            displayTimeVC(withBooking: booking)
        case .viewTicket(let booking, let navigationController):
            viewBookingDetails(booking: booking, rootViewController: navigationController)
        case .createNewReservation:
            createNewReservation()
        case .payTicket(let configurator, let booking):
            displayPayVC(withConfigurator: configurator, booking: booking)
        case .change(let configurator, let poso):
            displayChangeVC(configurator: configurator, poso:poso)
        case .presentProjection(let availability):
            displayProjection(withProjection: availability)
        }
    }
}

extension MainCoordinator{
    
    private func displayProjection(withProjection projection: [ParkingSpotAvailability]){
        guard let dependencies = dependencies else { return }
        projectionCoordinator = ProjectionCoordinator(dependencies: dependencies, rootViewController:  mainTabFactory.getNVC(mainTab: .profile).nav, availability: projection)
        projectionCoordinator?.start()
        addChild(coordinator: projectionCoordinator!)
    }
    
    private func displayScanVC(withBooking booking: SPBooking){
        let scanVC = auxiliarVCFactory.getInstance(auxiliarFactory: .scan(booking))
        mainTabFactory.getNVC(mainTab: .reservation).vc.present(scanVC, animated: true, completion: nil)
    }
    
    private func displayChangeVC(configurator: BookingVCConfigurator, poso: ChangePOSO){
        guard let changeVC = auxiliarVCFactory.getInstance(auxiliarFactory: .change(poso)) as? ChangeVC else{
            return
        }
        
        changeVC.viewModel.paymentCompletion = { [weak self] in
            if configurator.bookingType == .active{
                self?.mainTabFactory.getNVC(mainTab: .active).nav.popToRootViewController(animated: true)
            }else if configurator.bookingType == .tickets{
                self?.mainTabFactory.getNVC(mainTab: .openTicket).nav.popToRootViewController(animated: true)
            }
        }
        mainTabFactory.getNVC(mainTab: .active).vc.present(changeVC, animated: true, completion: nil)
    }
    
    private func displayParkVehicleVC(withBooking booking: SPBooking){
        let parkVehicleVC = auxiliarVCFactory.getInstance(auxiliarFactory: .parkVehicle(booking,.normal))
        mainTabFactory.getNVC(mainTab: .openTicket).vc.present(parkVehicleVC, animated: true, completion: nil)
    }
    
    private func displayTimeVC(withBooking booking: SPBooking){
        let timeVC = auxiliarVCFactory.getInstance(auxiliarFactory: .time(booking))
        rootViewController.present(timeVC, animated: true, completion: nil)
    }
    
    private func displayPayVC(withConfigurator configurator: BookingVCConfigurator,  booking: SPBooking){
        guard let payVC = auxiliarVCFactory.getInstance(auxiliarFactory: .pay(booking)) as? PayVC else{
            return
        }
        payVC.viewModel.confirmPaymentCompletion = { [weak self] poso in
            self?.navigate(to: .change(configurator, poso))
        }
        if configurator.bookingType == .active{
            payVC.viewModel.mode = .normal
            mainTabFactory.getNVC(mainTab: .active).nav.pushViewController(payVC, animated: true)
        }else if configurator.bookingType == .tickets{
            payVC.viewModel.mode = .prepaid
            mainTabFactory.getNVC(mainTab: .openTicket).nav.pushViewController(payVC, animated: true)
        }
    }
    
    private func createNewReservation(){
        ticketCoordinator = TicketCoordinator(dependencies: dependencies!, rootViewController: mainTabFactory.getNVC(mainTab: .reservation).nav)
        ticketCoordinator?.start()
        addChild(coordinator: ticketCoordinator!)
    }
    
    private func viewBookingDetails(booking: SPBooking, rootViewController: UINavigationController){
        ticketCoordinator = TicketCoordinator(dependencies: dependencies!, rootViewController: rootViewController, booking: booking)
        ticketCoordinator?.start()
        addChild(coordinator: ticketCoordinator!)
    }
    
}

extension MainCoordinator{
    
    private func configureTabBar(){
        rootViewController.tabBar.tintColor = #colorLiteral(red: 0.2078431373, green: 0.5764705882, blue: 0.7921568627, alpha: 1)
        rootViewController.tabBar.isTranslucent = false
        rootViewController.tabBar.unselectedItemTintColor = #colorLiteral(red: 0.4862297177, green: 0.4863031507, blue: 0.4862136841, alpha: 1)
    }
    
    private func initReservationsVC() {
        guard let reservationVC = mainTabFactory.getNVC(mainTab: .reservation).vc as? BookingVC else { return }
        mainTabFactory.initReservationsVC()
        reservationVC.viewModel.assignTicketNumber = { [weak self] booking in
            self?.navigate(to: .assignTicketNumber(booking))
        }
        reservationVC.viewModel.newReservation = {[weak self] in
            self?.navigate(to: .createNewReservation)
        }
        
        reservationVC.viewModel.bookingDetails = {[weak self] booking in
            guard let self = self else {return}
            self.navigate(to: .viewTicket(booking,self.mainTabFactory.getNVC(mainTab: .reservation).nav))
        }
        mainTabFactory.push(mainTab: .reservation, delegate: self)
    }
    
    private func initActiveVC() {
        guard let activeVC = mainTabFactory.getNVC(mainTab: .active).vc as? BookingVC else { return }
        mainTabFactory.initActiveVC()
        activeVC.viewModel.payTicket = { [weak self] configurator, booking in
            self?.navigate(to: .payTicket(configurator, booking))
        }
                mainTabFactory.push(mainTab: .active, delegate: self)
    }
    
    private func initNewVC() {
        guard let newVC = mainTabFactory.getNVC(mainTab: .new).vc as? BookingVC else { return }
        mainTabFactory.initNewVC()
        newVC.viewModel.acceptBooking  = { [weak self] booking in
            self?.navigate(to: .acceptBooking(booking))
        }
        mainTabFactory.push(mainTab: .new, delegate: self)
    }
    
    private func initProfile(){
        guard let profileVC = mainTabFactory.getNVC(mainTab: .profile).vc as? ProfileVC else { return }
        let viewModel = ProfileVM(dependencies: dependencies!)
        viewModel.logoutCompletion = { [weak self] in
            guard let self = self else {return}
            self.parent?.stop()
        }
        viewModel.presentProjectionCompletion = { [weak self] parkingSpotAvailability in
            guard let self = self else {return}
            self.navigate(to: .presentProjection(parkingSpotAvailability))
        }
        profileVC.viewModel = viewModel
        mainTabFactory.push(mainTab: .profile, delegate: self)
    }
    
    private func initOpenTicketsVC() {
        guard let openTicketVC = mainTabFactory.getNVC(mainTab: .openTicket).vc as? BookingVC else { return }
        mainTabFactory.initOpenTicketsVC()
        openTicketVC.viewModel.bookingDetails = {[weak self] booking in
            guard let self = self else {return}
            self.navigate(to: .viewTicket(booking, self.mainTabFactory.getNVC(mainTab: .openTicket).nav))
        }

        openTicketVC.viewModel.parkVehicle = { [weak self] booking in
            self?.navigate(to: .parkVehile(booking))
        }
        
        openTicketVC.viewModel.moveToActive = { [weak self] booking in
              self?.navigate(to: .acceptBooking(booking))
        }
        
        openTicketVC.viewModel.payTicket = { [weak self] configurator, booking in
            self?.navigate(to: .payTicket(configurator, booking))
        }
        mainTabFactory.push(mainTab: .openTicket, delegate: self)
    }
    
    private func initializeViewsCoordinators(){
        
        initOpenTicketsVC()
        initReservationsVC()
        initActiveVC()
        initNewVC()
        initProfile()

        rootViewController.setViewControllers([
            mainTabFactory.getNVC(mainTab: .openTicket).nav,
            mainTabFactory.getNVC(mainTab: .reservation).nav,
            mainTabFactory.getNVC(mainTab: .new).nav,
            mainTabFactory.getNVC(mainTab: .active).nav,
            mainTabFactory.getNVC(mainTab: .profile).nav], animated: false)
        rootViewController.viewControllers?.forEach({let _ = ($0 as? UINavigationController)?.viewControllers.first?.view})
    }
}


extension MainCoordinator: UINavigationControllerDelegate{
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        guard let viewController = navigationController.transitionCoordinator?.viewController(forKey: .from), !navigationController.viewControllers.contains(viewController) else { return }
        if viewController is TicketTVC{
            removeChild(coordinator: &ticketCoordinator)
        }else if viewController is ProjectionVC{
            removeChild(coordinator: &projectionCoordinator)
        }
    }
}
