//
//  Coordinator.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 3/28/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

protocol Navigator: class{
    associatedtype Destination
    func navigate(to: Destination)
}

protocol HasRootViewController{
    associatedtype ViewController
    var rootViewController: ViewController {get set}
}

protocol Coordinator: class{
    var parent: Coordinator? {get set}
    var childCoordinators: [Coordinator] {get set}
    func addChild(coordinator: Coordinator)
    func removeChild<T:Coordinator>(coordinator: inout T?)
    func start()
    func stop()
}

enum PresentationStyle{
    case push(Bool)
    case present
    case modally(Bool)
    case modallyWithPush(Bool)
    case dismiss
    case crossDisolved
}

extension Coordinator{
    
    func addChild(coordinator: Coordinator){
        childCoordinators.append(coordinator)
    }
    
    func removeChild<T:Coordinator>(coordinator: inout T?){
        print("🗑 Removing \((coordinator as? NSObject)?.classForCoder.description() ?? "INVALID-NAME") from childs")
        coordinator?.stop()
        removeChild(coordinator: coordinator)
        coordinator = nil
    }
    
    func removeChild(coordinator: Coordinator?){
        guard let coordinator = coordinator else {return}
        childCoordinators = childCoordinators.filter({$0 !== coordinator})
    }
    
}

extension HasRootViewController where ViewController == UINavigationController{
    func present(viewController : UIViewController, presentationStyle: PresentationStyle){
        rootViewController.modalPresentationStyle = .none
        switch presentationStyle {
        case .push(let animated):
            rootViewController.pushViewController(viewController, animated: animated)
        case .modally(let fullScreen):
            viewController.modalPresentationStyle = fullScreen ? .overFullScreen : .overCurrentContext
            rootViewController.present(viewController, animated: true, completion: nil)
        case .present:
            rootViewController.present(viewController, animated: true, completion: nil)
        case .modallyWithPush(let fullScreen):
            let transition = CATransition()
            transition.duration = 0.2
            transition.type = .moveIn
            transition.subtype = CATransitionSubtype.fromTop
            viewController.modalPresentationStyle = fullScreen ? .overFullScreen : .overCurrentContext
            rootViewController.view.layer.add(transition, forKey: nil)
            rootViewController.pushViewController(viewController, animated: false)
        case .crossDisolved:
            let transition = CATransition()
            transition.duration = 0.2
            transition.type = .fade
            transition.subtype = CATransitionSubtype.fromTop
            rootViewController.view.layer.add(transition, forKey: nil)
            rootViewController.pushViewController(viewController, animated: false)
        case .dismiss:
            rootViewController.popViewController(animated: true)
        }
    }
}

