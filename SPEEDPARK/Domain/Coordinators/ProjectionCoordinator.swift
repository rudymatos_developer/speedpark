//
//  ProfileCoordinator.swift
//  SPEEDPARK
//
//  Created by Rudy Matos on 2/5/20.
//  Copyright © 2020 TonyS. All rights reserved.
//

import Foundation


import Foundation
import UIKit

class ProjectionCoordinator: NSObject, Coordinator, HasDependencies, HasRootViewController{
    
    var parent: Coordinator?
    var childCoordinators: [Coordinator] = []
    typealias Dependencies = HasDataService & HasUserDefaultService & HasAuthorizableService & HasUser & HasCarAPIService & HasMessageService & HasSoundService
    
    var dependencies: Dependencies?
    var rootViewController : UINavigationController
    private let availability: [ParkingSpotAvailability]
    private let auxiliarVCFactory: AuxiliarVCFactory!
    
    init(dependencies: Dependencies, rootViewController: UINavigationController,availability: [ParkingSpotAvailability]){
        self.dependencies = dependencies
        self.rootViewController = rootViewController
        self.availability = availability
        self.auxiliarVCFactory = AuxiliarVCFactory(dependencies: dependencies)
        super.init()
        print("🏀 initializing \(self.classForCoder.description())")
    }
    
    func start() {
        rootViewController.navigationBar.tintColor = .white
        navigate(to: .showGraph(availability))
    }
    
    func stop() {
    }
    
    enum Destination{
        case showGraph([ParkingSpotAvailability])
        case showGraphDetails([ParkingSpotAvailability])
    }
    
    deinit {
        print("💀 Removing \(self.classForCoder.description())")
    }
}

extension ProjectionCoordinator: Navigator{
    func navigate(to: ProjectionCoordinator.Destination) {
        switch to {
        case .showGraph(let data):
            showMainGraphProjection(availability: data)
        case .showGraphDetails(let data):
            showGraphProjectionDetail(availability: data)
        }
    }
}

extension ProjectionCoordinator{
    private func showMainGraphProjection(availability: [ParkingSpotAvailability]){
        guard let projectionVC = auxiliarVCFactory.getInstance(auxiliarFactory: .projection) as? ProjectionVC else { return }
        let viewModel = ProjectionVM(dependencies: dependencies!, availability: availability)
        projectionVC.viewModel = viewModel
        present(viewController: projectionVC, presentationStyle: .push(true))
    }
    
    private func showGraphProjectionDetail(availability: [ParkingSpotAvailability]){
    }
}
