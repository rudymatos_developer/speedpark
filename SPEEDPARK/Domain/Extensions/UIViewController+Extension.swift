//
//  UIViewController+Extension.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/10/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    
    func addSectionUIConfig(view: UIView) {
        view.layer.borderColor = UIColor(named: "DisabledColor")?.cgColor
        view.layer.borderWidth = 0.3
    }
    
    func applyCircleCornerRadius(toView: UIView) {
        toView.layer.cornerRadius = toView.frame.width / 2
    }
    
    func applyUIChangesToTextView(textView: UITextView, withColor: UIColor = UIColor.black) {
        textView.layer.borderColor = withColor.cgColor
        textView.layer.cornerRadius = 4
        textView.layer.borderWidth = 1
    }
    
    func addBorder(toView: UIView, color: UIColor = UIColor.gray, width: CGFloat) {
        toView.layer.borderWidth = width
        toView.layer.borderColor = color.cgColor
    }
    
    func applyCornerRadius(toView: UIView, cornerRadius: CGFloat) {
        toView.layer.cornerRadius = cornerRadius
        toView.layer.masksToBounds = true
    }
    
    func getViewController(fromStoryboardName: String, vcName: String) -> UIViewController? {
        let storyBoard = UIStoryboard(name: fromStoryboardName, bundle: nil)
        let vc = storyBoard.instantiateViewController(withIdentifier: vcName)
        return vc
    }
    
    func displaySimpleAlertMessage(title: String, message: String) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertView.addAction(okAction)
        self.present(alertView, animated: true, completion: nil)
    }
    
    func displayConfirmationDialog(title: String, message: String, okTitle: String = "Ok", isOKDesctructive: Bool = false, okCompletion: ((UIAlertAction) -> Void)?) {
        let alertView = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: isOKDesctructive ? .destructive : .default, handler: okCompletion)
        let cancelAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        alertView.addAction(cancelAction)
        alertView.addAction(okAction)
        self.present(alertView, animated: true, completion: nil)
    }
}

