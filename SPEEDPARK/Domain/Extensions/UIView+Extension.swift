//
//  UIView+Extension.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/21/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation
import UIKit
import Lottie

extension UIView{
    func initLottieView(withName: String, loopAnimation: Bool = true){
        let animation = AnimationView(name: withName)
        animation.frame = self.bounds
        self.addSubview(animation)
        animation.loopMode = loopAnimation ? .loop : .playOnce
        animation.play()
    }
}
