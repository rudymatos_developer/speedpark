//
//  DateFormatter.swift
//  SPEEDPARK
//
//  Created by Rudy E Matos on 4/10/19.
//  Copyright © 2019 TonyS. All rights reserved.
//

import Foundation

enum DatePattern: String{
    case main = "MM/dd/yyyy"
    case short = "yyyy-MM-dd"
    case normal = "yyyy-MM-dd HH:mm:ss"
    case long = "yyyy-MM-dd'T'hh:mm:ssZ"
    case time = "HH:mm:ss a"
    case shortTime  = "HH:mm"
    case full
    case amadeus = "yyyy-MM-dd'T'HH:mm"
    case section = "LLLL yyyy"
    case justYear = "yyyy"
    case monthAndDay = "MMMM d"
    
    case speedParkLongFormat = "MM/dd/yyyy HH:mm"
    case speedParkLongFormatDisplay = "dd MMMM yyyy HH:mm"
    case speedParkLongFormat12HoursDisplay = "dd MMMM yyyy hh:mm a"
    
}

protocol SPDateFormatter {
    func convert(date: Date?, usingPattern: DatePattern) -> String
    func convert(string: String?, usingPattern: DatePattern) -> Date?
}

extension SPDateFormatter {
    
    func convert(date: Date?, usingPattern: DatePattern = .short) -> String {
        if let date = date {
            if usingPattern == .full{
                let prefix = convert(date: date, usingPattern: .monthAndDay)
                let ordinarySuffix = daySuffix(from: date)
                let suffix = convert(date: date, usingPattern: .justYear)
                return "\(prefix)\(ordinarySuffix) \(suffix) at \(convert(date: date, usingPattern: .time))"
            }else{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = usingPattern.rawValue
                return dateFormatter.string(from: date)
            }
        }
        return ""
    }
    
    func convert(string: String?, usingPattern: DatePattern = .short) -> Date? {
        if let string = string {
            if usingPattern == .full{
                return nil
            }else{
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = usingPattern.rawValue
                return dateFormatter.date(from: string)
            }
        }
        return nil
    }
    
    private func daySuffix(from date: Date) -> String {
        let calendar = Calendar.current
        let dayOfMonth = calendar.component(.day, from: date)
        switch dayOfMonth {
        case 1, 21, 31: return "st"
        case 2, 22: return "nd"
        case 3, 23: return "rd"
        default: return "th"
        }
    }
}
